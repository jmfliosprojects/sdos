# CONNECTION

Technical test.


## Overview ##

Library for development app.


## Installation

### CocoaPods

To create pod dependencies:

````ruby
pod install
````


## Version ##

-   **_v.1.0.0_**: Initial version.


## Build Requirements ##

-   _Xcode 10.3_ or later.
-   _iOS 12.4_ SDK or later.
-   Run ```pod install`` to create pod dependencies.


## Frameworks Requirements ##

### Targets ###

### Pods ###

-   **Alamofire (4.9.0)_**
-   **ReachabilitySwift (4.3.1)_**
-   **SwiftyJSON (5.0.0)_**


## Release Notes ##

-   **_v.1.0.0_**: Initial version.


**Copyright (C) 2019 SDOS All rights reserved.**

