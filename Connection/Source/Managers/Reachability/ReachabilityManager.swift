//
//  ReachabilityManager.swift
//  Connection
//
//  Created by Juan Miguel Fernández Lerenaon 04/09/2019.
//  Copyright © 2019 JUAN MIGUEL. All rights reserved.
//

import Foundation
import Reachability

/// Protocol for reachability.
public protocol ReachabilityDelegate: NSObjectProtocol {
    
    /// Reachability status change handler.
    /// - parameters:
    ///   - reachability: Reachability.
    /// - throws:
    func reachabilityStatusChangeHandler(reachability: Reachability)
}

/// Manager of reachability.
public class ReachabilityManager: NSObject {
    
    /// Shared instance.
    public static let sharedInstance = ReachabilityManager()
    /// Delegate for reachability.
    public var delegate: ReachabilityDelegate? = nil
    /// Is reachability
    public var isReachability: Bool {
        get {return _isReachability}
    }
    
    private var _useClosures: Bool = false
    private var reachability: Reachability?
    private var _isReachability: Bool = false
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Public methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Init reachability monitor.
    /// - throws:
    public func startRechabilityMonitor() {
        let reachability: Reachability
        reachability = Reachability.init()!
        self.reachability = reachability
        if (_useClosures) {
            reachability.whenReachable = { reachability in
                self.notifyReachability(reachability: reachability)
            }
            reachability.whenUnreachable = { reachability in
                self.notifyReachability(reachability: reachability)
            }
        } else {
            self.notifyReachability(reachability: reachability)
        }
        do {
            try reachability.startNotifier()
        } catch {
            print("unable to start notifier")
            return
        }
    }
    
    /// Is connected.
    /// - throws:
    /// - return: If is connected
    public func isConnected() -> Bool {
        var isConnected = false
        isConnected = (self.reachability?.connection != .none) 
        return isConnected
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Observer of notification
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Notify reachability.
    /// - parameters:
    ///   - note: Notification.
    /// - throws:
    @objc public func reachabilityChanged(note: NSNotification) {
        let reachability = note.object as! Reachability
        DispatchQueue.main.async( execute: {
            self.delegate?.reachabilityStatusChangeHandler(reachability: reachability)
        })
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Private methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Notify reachability.
    /// - parameters:
    ///   - reachability: Reachability.
    /// - throws:
    private func notifyReachability(reachability:Reachability) {
        if reachability.connection != .none {
            self._isReachability = true
        } else {
            self._isReachability = false
        }
        NotificationCenter.default.addObserver(self, selector: #selector(ReachabilityManager.reachabilityChanged(note:)), name: Notification.Name.reachabilityChanged, object: reachability)
    }
    
    /// Deinit.
    /// - throws:
    deinit {
        reachability?.stopNotifier()
        if (!_useClosures) {
            NotificationCenter.default.removeObserver(self, name: Notification.Name.reachabilityChanged, object: nil)
        }
    }
    
}

