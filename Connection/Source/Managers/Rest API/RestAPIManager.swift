//
//  RestAPIManager.swift
//  Connection
//
//  Created by Juan Miguel Fernández Lerenaon 04/09/2019.
//  Copyright © 2019 JUAN MIGUEL. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

/// Constants for manager.
public struct RestAPIMethod {
    
    public static let POST = "Post"
    public static let GET = "Get"
    public static let DELETE = "Delete"
    public static let PUT = "Put"
    
}

/// Constants for manager.
struct RestAPIKey {
    
    static let SYNCED_AT = "Synced-At"
    
}

/// Constants for manager.
public struct RestAPIError {
    
    public static let EXCEPTION_CONNECTION_DOMAIN = "ExceptionConnectionDomain"
    public static let CODE_8999_INVALID_URL = 8999
    public static let CODE_8998_PARAMETER_ENCODING_FAILED = 8998
    public static let CODE_8997_MULTIPART_ENCODING_FAILED = 8997
    public static let CODE_8996_DOWNLOADED_FILE_COULD_NOT_BE_READ = 8996
    public static let CODE_8995_MISSING_CONTENT_TYPE = 8995
    public static let CODE_8994_UNACCEPTABLE_CONTENT_TYPE = 8994
    public static let CODE_8993_UNACCEPTABLE_STATUS_CODE = 8993
    public static let CODE_8992_RESPONSE_SERIALIZATION_FAILED_CODE = 8992
    public static let CODE_8990_UNDEFINED = 8990
    public static let CODE_8991_URL_ERROR_CODE = 8991
    public static let CODE_400_BAD_REQUEST = 400
    public static let CODE_401_UNAUTHORIZED = 401
    public static let CODE_403_FORBIDDEN = 403
    public static let CODE_404_NOT_FOUND = 404
    public static let CODE_500_INTERNAL_ERROR = 500
    public static let CODE_503_NOT_AVAILABLE = 503
    
}

/// Manager of Rest API.
public class RestAPIManager: NSObject {
    
    /// Shared instance.
    public static let sharedInstance = RestAPIManager()
    /// Completion handler for request in background.
    public typealias RequestCompletionHandler = (_ isReachability: Bool, _ success: Bool, _ json: JSON, _ apiError: NSError?, _ syncedAt: NSNumber?) -> Void    
    // Time out.
    public static let TIME_OUT: Double = 300
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Public methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Permform request.
    /// - parameters:
    ///   - httpMethod: HTTP method.
    ///   - domain: Domain.
    ///   - service: Service.
    ///   - method: Method.
    ///   - headers: Headers.
    ///   - parameters: Parameters.
    ///   - completionHandlerWithSyncAt: Completion handler With syncAt.
    /// - throws:
    public static func performRequest(httpMethod: String,
                                      domain: String,
                                      service: String,
                                      method: String,
                                      headers: [String : String],
                                      parameters: [String : Any],
                                      completionHandler: @escaping RequestCompletionHandler) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let url: String = domain + service + method
        var methodRequet = HTTPMethod.post
        var encoding: ParameterEncoding = JSONEncoding.default
        if httpMethod == RestAPIMethod.GET {
            methodRequet = HTTPMethod.get
            encoding = URLEncoding.queryString
        } else if httpMethod == RestAPIMethod.DELETE {
            methodRequet = HTTPMethod.delete
            encoding = URLEncoding.queryString
        } else if httpMethod == RestAPIMethod.PUT {
            methodRequet = HTTPMethod.put
            encoding = JSONEncoding.default
        }
        let isConnected: Bool = ReachabilityManager.sharedInstance.isConnected()
        if !isConnected {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            var message: String
            var apiError: NSError?
            message = "No internet connection"
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] =  message as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = message as AnyObject?
            apiError = NSError(domain: RestAPIError.EXCEPTION_CONNECTION_DOMAIN, code: RestAPIError.CODE_8998_PARAMETER_ENCODING_FAILED, userInfo: dict)
            return completionHandler(false, false, [], apiError, 0)
        }
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = RestAPIManager.TIME_OUT
        manager.request(url,
                        method: methodRequet,
                        parameters: parameters,
                        encoding: encoding,
                        headers: headers).responseJSON {
                            (response:DataResponse<Any>) in
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                            var statusCode = response.response?.statusCode
                            var message: String
                            var apiError: NSError?
                            var syncedAt: NSNumber?
                            if let error = response.result.error as? AFError {
                                statusCode = error._code
                                switch error {
                                case .invalidURL(let url):
                                    message = "Invalid URL: \(url) - \(error.localizedDescription)"
                                    var dict = [String: AnyObject]()
                                    dict[NSLocalizedDescriptionKey] =  message as AnyObject?
                                    dict[NSLocalizedFailureReasonErrorKey] = message as AnyObject?
                                    apiError = NSError(domain: RestAPIError.EXCEPTION_CONNECTION_DOMAIN, code: RestAPIError.CODE_8999_INVALID_URL, userInfo: dict)
                                case .parameterEncodingFailed(let reason):
                                    message = "Parameter encoding failed: \(error.localizedDescription)" + " " + "Failure Reason: \(reason)"
                                    var dict = [String: AnyObject]()
                                    dict[NSLocalizedDescriptionKey] =  message as AnyObject?
                                    dict[NSLocalizedFailureReasonErrorKey] = message as AnyObject?
                                    apiError = NSError(domain: RestAPIError.EXCEPTION_CONNECTION_DOMAIN, code: RestAPIError.CODE_8998_PARAMETER_ENCODING_FAILED, userInfo: dict)
                                case .multipartEncodingFailed(let reason):
                                    message = "Multipart encoding failed: \(error.localizedDescription)" + " " + "Failure Reason: \(reason)"
                                    var dict = [String: AnyObject]()
                                    dict[NSLocalizedDescriptionKey] =  message as AnyObject?
                                    dict[NSLocalizedFailureReasonErrorKey] = message as AnyObject?
                                    apiError = NSError(domain: RestAPIError.EXCEPTION_CONNECTION_DOMAIN, code: RestAPIError.CODE_8997_MULTIPART_ENCODING_FAILED, userInfo: dict)
                                case .responseValidationFailed(let reason):
                                    switch reason {
                                    case .dataFileNil, .dataFileReadFailed:
                                        message = "Downloaded file could not be read"
                                        var dict = [String: AnyObject]()
                                        dict[NSLocalizedDescriptionKey] =  message as AnyObject?
                                        dict[NSLocalizedFailureReasonErrorKey] = message as AnyObject?
                                        dict[NSUnderlyingErrorKey] = error as NSError
                                        apiError = NSError(domain: RestAPIError.EXCEPTION_CONNECTION_DOMAIN, code: RestAPIError.CODE_8996_DOWNLOADED_FILE_COULD_NOT_BE_READ, userInfo: dict)
                                    case .missingContentType(let acceptableContentTypes):
                                        message = "Content Type Missing: \(acceptableContentTypes)"
                                        var dict = [String: AnyObject]()
                                        dict[NSLocalizedDescriptionKey] =  message as AnyObject?
                                        dict[NSLocalizedFailureReasonErrorKey] = message as AnyObject?
                                        dict[NSUnderlyingErrorKey] = error as NSError
                                        apiError = NSError(domain: RestAPIError.EXCEPTION_CONNECTION_DOMAIN, code: RestAPIError.CODE_8995_MISSING_CONTENT_TYPE, userInfo: dict)
                                    case .unacceptableContentType(let acceptableContentTypes, let responseContentType):
                                        message = "Response content type: \(responseContentType) was unacceptable: \(acceptableContentTypes)"
                                        var dict = [String: AnyObject]()
                                        dict[NSLocalizedDescriptionKey] =  message as AnyObject?
                                        dict[NSLocalizedFailureReasonErrorKey] = message as AnyObject?
                                        dict[NSUnderlyingErrorKey] = error as NSError
                                        apiError = NSError(domain: RestAPIError.EXCEPTION_CONNECTION_DOMAIN, code: RestAPIError.CODE_8994_UNACCEPTABLE_CONTENT_TYPE, userInfo: dict)
                                    case .unacceptableStatusCode(let code):
                                        message = "Response status code was unacceptable: \(code)"
                                        var dict = [String: AnyObject]()
                                        dict[NSLocalizedDescriptionKey] =  message as AnyObject?
                                        dict[NSLocalizedFailureReasonErrorKey] = message as AnyObject?
                                        dict[NSUnderlyingErrorKey] = error as NSError
                                        apiError = NSError(domain: RestAPIError.EXCEPTION_CONNECTION_DOMAIN, code: RestAPIError.CODE_8993_UNACCEPTABLE_STATUS_CODE, userInfo: dict)
                                        statusCode = code
                                    }
                                case .responseSerializationFailed(let reason):
                                    message = "Response serialization failed: \(error.localizedDescription)" + " " + "Failure Reason: \(reason)"
                                    var dict = [String: AnyObject]()
                                    dict[NSLocalizedDescriptionKey] =  message as AnyObject?
                                    dict[NSLocalizedFailureReasonErrorKey] = message as AnyObject?
                                    dict[NSUnderlyingErrorKey] = error as NSError
                                    apiError = NSError(domain: RestAPIError.EXCEPTION_CONNECTION_DOMAIN, code: RestAPIError.CODE_8992_RESPONSE_SERIALIZATION_FAILED_CODE, userInfo: dict)
                                }
                            } else if let error = response.result.error as? URLError {
                                message = "URLError occurred: \(error)"
                                var dict = [String: AnyObject]()
                                dict[NSLocalizedDescriptionKey] =  message as AnyObject?
                                dict[NSLocalizedFailureReasonErrorKey] = message as AnyObject?
                                dict[NSUnderlyingErrorKey] = error as NSError
                                apiError = NSError(domain: RestAPIError.EXCEPTION_CONNECTION_DOMAIN, code: RestAPIError.CODE_8991_URL_ERROR_CODE, userInfo: dict)
                            } else {
                                if let httpStatusCode = response.response?.statusCode {
                                    switch(httpStatusCode) {
                                    case 200:
                                        if let headersResponse: [AnyHashable : Any] = response.response?.allHeaderFields {
                                            if let syncedAtValue = headersResponse[RestAPIKey.SYNCED_AT] {
                                                if let syncedAtString = (syncedAtValue as? String) {
                                                    if let syncedAtInteger = Int(syncedAtString) {
                                                        syncedAt = NSNumber(value:syncedAtInteger)
                                                    }
                                                }
                                            }
                                        }
                                        break
                                    case 400:
                                        message = "The request had bad syntax or was inherently impossible to be satisfied."
                                        var dict = [String: AnyObject]()
                                        dict[NSLocalizedDescriptionKey] =  message as AnyObject?
                                        dict[NSLocalizedFailureReasonErrorKey] = message as AnyObject?
                                        apiError = NSError(domain: RestAPIError.EXCEPTION_CONNECTION_DOMAIN, code: RestAPIError.CODE_400_BAD_REQUEST, userInfo: dict)
                                        break
                                    case 401:
                                        message = "The parameter to this message gives a specification of authorization schemes which are acceptable."
                                        var dict = [String: AnyObject]()
                                        dict[NSLocalizedDescriptionKey] = message as AnyObject?
                                        dict[NSLocalizedFailureReasonErrorKey] = message as AnyObject?
                                        apiError = NSError(domain: RestAPIError.EXCEPTION_CONNECTION_DOMAIN, code: RestAPIError.CODE_401_UNAUTHORIZED, userInfo: dict)
                                        break
                                    case 403:
                                        message = "The request is for something forbidden. Authorization will not help."
                                        var dict = [String: AnyObject]()
                                        dict[NSLocalizedDescriptionKey] =  message as AnyObject?
                                        dict[NSLocalizedFailureReasonErrorKey] = message as AnyObject?
                                        apiError = NSError(domain: RestAPIError.EXCEPTION_CONNECTION_DOMAIN, code: RestAPIError.CODE_403_FORBIDDEN, userInfo: dict)
                                        break
                                    case 404:
                                        message = "The server has not found anything matching the URI given."
                                        var dict = [String: AnyObject]()
                                        dict[NSLocalizedDescriptionKey] =  message as AnyObject?
                                        dict[NSLocalizedFailureReasonErrorKey] = message as AnyObject?
                                        apiError = NSError(domain: RestAPIError.EXCEPTION_CONNECTION_DOMAIN, code: RestAPIError.CODE_404_NOT_FOUND, userInfo: dict)
                                        break
                                    case 500:
                                        message = "Internal server error"
                                        var dict = [String: AnyObject]()
                                        dict[NSLocalizedDescriptionKey] =  message as AnyObject?
                                        dict[NSLocalizedFailureReasonErrorKey] = message as AnyObject?
                                        apiError = NSError(domain: RestAPIError.EXCEPTION_CONNECTION_DOMAIN, code: RestAPIError.CODE_500_INTERNAL_ERROR, userInfo: dict)
                                    case 503:
                                        message = "Non available server"
                                        var dict = [String: AnyObject]()
                                        dict[NSLocalizedDescriptionKey] =  message as AnyObject?
                                        dict[NSLocalizedFailureReasonErrorKey] = message as AnyObject?
                                        apiError = NSError(domain: RestAPIError.EXCEPTION_CONNECTION_DOMAIN, code: RestAPIError.CODE_503_NOT_AVAILABLE, userInfo: dict)
                                        break
                                    default:
                                        message = "Error with status code \(httpStatusCode)"
                                        var dict = [String: AnyObject]()
                                        dict[NSLocalizedDescriptionKey] =  message as AnyObject?
                                        dict[NSLocalizedFailureReasonErrorKey] = message as AnyObject?
                                        apiError = NSError(domain: RestAPIError.EXCEPTION_CONNECTION_DOMAIN, code: RestAPIError.CODE_8990_UNDEFINED, userInfo: dict)
                                        break
                                    }
                                } else {
                                    message = (response.result.error?.localizedDescription)!
                                    var dict = [String: AnyObject]()
                                    dict[NSLocalizedDescriptionKey] =  message as AnyObject?
                                    dict[NSLocalizedFailureReasonErrorKey] = message as AnyObject?
                                    apiError = NSError(domain: RestAPIError.EXCEPTION_CONNECTION_DOMAIN, code: RestAPIError.CODE_8990_UNDEFINED, userInfo: dict)
                                }
                            }
                            print(response.response?.url ?? "")
                            print(url)
                            print(statusCode ?? "")
                            print(response.result)
                            switch(response.result) {
                            case .success(_):
                                if let data = response.result.value {
                                    print(data)
                                    let json = JSON(data)
                                    completionHandler(true, true, json, apiError, syncedAt)
                                }
                                break
                            case .failure(_):
                                message = (response.result.error?.localizedDescription)!
                                var dict = [String: AnyObject]()
                                dict[NSLocalizedDescriptionKey] =  message as AnyObject?
                                dict[NSLocalizedFailureReasonErrorKey] = message as AnyObject?
                                apiError = NSError(domain: RestAPIError.EXCEPTION_CONNECTION_DOMAIN, code: RestAPIError.CODE_8990_UNDEFINED, userInfo: dict)
                                completionHandler(true, false, [], apiError, syncedAt)
                                break
                            }
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Private methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
}


