//
//  TranslucentNavigationController.swift
//  SkeletonSwift
//
//  Created by Juan Miguel Fernández on 07/09/2019.
//  Copyright © 2019 Juan Miguel Fernández Lerena. All rights reserved.
//

import UIKit

/// Controller of translucent navigation.
class TranslucentNavigationController: UINavigationController {
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        let image: UIImage = UIImage()
        self.navigationBar.setBackgroundImage(image, for: UIBarMetrics.default)
        self.navigationBar.shadowImage = image
        self.navigationBar.isTranslucent = true
    }
    
    override var prefersStatusBarHidden: Bool {
        get {
            return false
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
