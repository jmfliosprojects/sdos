//
//  BaseTabTableViewController.swift
//  SkeletonSwift
//
//  Created by Juan Miguel Fernández on 06/09/2019.
//  Copyright © 2019 Juan Miguel Fernández Lerena. All rights reserved.
//

import Foundation
import UIKit

/// Controller of base table view.
class BaseTabTableViewController: BaseTableViewController {
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Navigation
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    override func prepare(for segue: UIStoryboardSegue,
                          sender: Any?) {
        
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Public methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Lifecycle
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Managing the View
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    override public func viewDidLoad() {
        super.viewDidLoad()
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Responding to View Events
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override public func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
    }
    
    override public func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillTransition(to size: CGSize,
                                     with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to:size, with: coordinator)
        
        coordinator.animate(alongsideTransition: { (UIViewControllerTransitionCoordinatorContext) -> Void in
            
            let orient = UIApplication.shared.statusBarOrientation
            
            switch orient {
            case .portrait:
                
                break
            case .portraitUpsideDown:
                
                break
            case .landscapeLeft:
                
                break
            case .landscapeRight:
                
                break
            default:
                
                break
            }
            
        }, completion: { (UIViewControllerTransitionCoordinatorContext) -> Void in
            
        })
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override var prefersStatusBarHidden: Bool {
        get {
            return false
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.default
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Delegate methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: BaseAnyControllerDelegate
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Update preferred font adjustment.
    /// - throws:
    override func updatePreferredFontAdjustment() {
        super.updatePreferredFontAdjustment()
    }
    
    /// Perform reload remote.
    /// - throws:
    override func performReloadRemote() {
        super.performReloadRemote()
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Private methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Actions
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Observer of notification
    ////////////////////////////////////////////////////////////////////////////////////////////
    
}

