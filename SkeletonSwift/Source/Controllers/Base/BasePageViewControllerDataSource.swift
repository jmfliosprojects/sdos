//
//  BasePageViewControllerDataSource.swift
//  SkeletonSwift
//
//  Created by Juan Miguel Fernández on 07/09/2019.
//  Copyright © 2019 Juan Miguel Fernández Lerena. All rights reserved.
//

import UIKit

@objc public protocol BasePageViewControllerDataSource {
    
    @objc func ss_pageViewController(pageViewController: BasePageViewController,
                                     viewControllerLeftOfViewController viewController: UIViewController) -> UIViewController?
    
    @objc func ss_pageViewController(pageViewController: BasePageViewController,
                                     viewControllerRightOfViewController viewController: UIViewController) -> UIViewController?
    
}

@objc public protocol BasePageViewControllerDelegate {
    
    @objc optional func ss_pageViewController(pageViewController: BasePageViewController,
                                              willStartScrollingFrom startingViewController: UIViewController,
                                              destinationViewController:UIViewController)
    
    @objc optional func ss_pageViewController(pageViewController: BasePageViewController,
                                              isScrollingFrom startingViewController: UIViewController,
                                              destinationViewController:UIViewController,
                                              progress: CGFloat)
    
    @objc optional func ss_pageViewController(pageViewController: BasePageViewController,
                                              didFinishScrollingFrom startingViewController: UIViewController?,
                                              destinationViewController:UIViewController, transitionSuccessful: Bool)
    
}

public enum BasePageViewControllerNavigationDirection : Int {
    
    case Forward
    case Reverse
    
}

public class BasePageViewController: UIViewController, UIScrollViewDelegate {
    
    public weak var dataSource: BasePageViewControllerDataSource?
    public weak var delegate: BasePageViewControllerDelegate?
    
    private let scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.isPagingEnabled = true
        scrollView.scrollsToTop = false
        scrollView.autoresizingMask = [.flexibleTopMargin, .flexibleRightMargin, .flexibleBottomMargin, .flexibleLeftMargin]
        scrollView.bounces = true
        scrollView.alwaysBounceHorizontal = true
        scrollView.translatesAutoresizingMaskIntoConstraints = true
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
        return scrollView
    }()
    
    private var leftViewController: UIViewController?
    private(set) var selectedViewController: UIViewController?
    private var rightViewController: UIViewController?
    
    private(set) var scrolling = false
    private(set) var navigationDirection: BasePageViewControllerNavigationDirection?
    
    private var adjustingContentOffset = false
    private var loadNewAdjoiningViewControllersOnFinish = false
    private var didFinishScrollingCompletionHandler: ((_ transitionSuccessful: Bool) -> Void)?
    private var transitionAnimated = false
    
    override public var shouldAutomaticallyForwardAppearanceMethods: Bool {
        return false
    }
    
    public func selected() -> UIViewController? {
        return selectedViewController
    }
    
    public func selectViewController(viewController: UIViewController,
                                     direction: BasePageViewControllerNavigationDirection,
                                     animated: Bool,
                                     completion: ((_ transitionSuccessful: Bool) -> Void)?) {
        if (direction == .Forward) {
            self.rightViewController = viewController
            self.layoutViews()
            self.loadNewAdjoiningViewControllersOnFinish = true
            self.scrollForwardAnimated(animated: animated, completion: completion)
        } else if (direction == .Reverse) {
            self.leftViewController = viewController
            self.layoutViews()
            self.loadNewAdjoiningViewControllersOnFinish = true
            self.scrollReverseAnimated(animated: animated, completion: completion)
        }
    }
    
    public func scrollForwardAnimated(animated: Bool,
                                      completion: ((_ transitionSuccessful: Bool) -> Void)?) {
        if (self.rightViewController != nil) {
            // Cancel current animation and move
            if self.scrolling {
                self.scrollView.setContentOffset(CGPoint(x: self.view.bounds.width * 2, y: 0), animated: false)
            }
            self.didFinishScrollingCompletionHandler = completion
            self.transitionAnimated = animated
            self.scrollView.setContentOffset(CGPoint(x: self.view.bounds.width * 2, y: 0), animated: animated)
        }
    }
    
    public func scrollReverseAnimated(animated: Bool,
                                      completion: ((_ transitionSuccessful: Bool) -> Void)?) {
        if (self.leftViewController != nil) {
            // Cancel current animation and move
            if self.scrolling {
                self.scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
            }
            self.didFinishScrollingCompletionHandler = completion
            self.transitionAnimated = animated
            self.scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: animated)
        }
    }
    
    
    // MARK: - View Controller Overrides
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.scrollView.delegate = self
        self.view.addSubview(scrollView)
    }
    
    override public func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.scrollView.frame = self.view.bounds
        self.scrollView.contentSize = CGSize(width: self.view.bounds.width * 3, height: self.view.bounds.height)
        self.layoutViews()
    }
    
    private func loadViewControllers(selectedViewController: UIViewController) {
        // Scrolled forward
        if (selectedViewController == self.rightViewController) {
            // Shift view controllers forward
            self.leftViewController = self.selectedViewController
            self.selectedViewController = self.rightViewController
            self.selectedViewController!.endAppearanceTransition()
            self.removeChildIfNeeded(viewController: self.leftViewController)
            self.leftViewController?.endAppearanceTransition()
            self.delegate?.ss_pageViewController?(pageViewController: self, didFinishScrollingFrom: self.leftViewController, destinationViewController: self.selectedViewController!, transitionSuccessful: true)
            self.didFinishScrollingCompletionHandler?(true)
            self.didFinishScrollingCompletionHandler = nil
            // Load new left view controller if required
            if self.loadNewAdjoiningViewControllersOnFinish {
                self.loadLeftViewControllerForSelectedViewController(selectedViewController: selectedViewController)
                self.loadNewAdjoiningViewControllersOnFinish = false
            }
            // Load new right view controller
            self.loadRightViewControllerForSelectedViewController(selectedViewController: selectedViewController)
            // Scrolled reverse
        } else if (selectedViewController == self.leftViewController) {
            // Shift view controllers reverse
            self.rightViewController = self.selectedViewController
            self.selectedViewController = self.leftViewController
            self.selectedViewController!.endAppearanceTransition()
            self.removeChildIfNeeded(viewController: self.rightViewController)
            self.rightViewController?.endAppearanceTransition()
            self.delegate?.ss_pageViewController?(pageViewController: self, didFinishScrollingFrom: self.rightViewController!, destinationViewController: self.selectedViewController!, transitionSuccessful: true)
            self.didFinishScrollingCompletionHandler?(true)
            self.didFinishScrollingCompletionHandler = nil
            // Load new right view controller if required
            if self.loadNewAdjoiningViewControllersOnFinish {
                self.loadRightViewControllerForSelectedViewController(selectedViewController: selectedViewController)
                self.loadNewAdjoiningViewControllersOnFinish = false
            }
            // Load new left view controller
            self.loadLeftViewControllerForSelectedViewController(selectedViewController: selectedViewController)
            // Scrolled but ended up where started
        } else if (selectedViewController == self.selectedViewController) {
            self.selectedViewController!.beginAppearanceTransition(true, animated: self.transitionAnimated)
            if (self.navigationDirection == .Forward) {
                self.rightViewController!.beginAppearanceTransition(false, animated: self.transitionAnimated)
            } else if (self.navigationDirection == .Reverse) {
                self.leftViewController!.beginAppearanceTransition(false, animated: self.transitionAnimated)
            }
            self.selectedViewController!.endAppearanceTransition()
            // Remove hidden view controllers
            self.removeChildIfNeeded(viewController: self.leftViewController)
            self.removeChildIfNeeded(viewController: self.rightViewController)
            if (self.navigationDirection == .Forward) {
                self.rightViewController!.endAppearanceTransition()
                self.delegate?.ss_pageViewController?(pageViewController: self, didFinishScrollingFrom: self.selectedViewController!, destinationViewController: self.rightViewController!, transitionSuccessful: false)
            } else if (self.navigationDirection == .Reverse) {
                self.leftViewController!.endAppearanceTransition()
                self.delegate?.ss_pageViewController?(pageViewController: self, didFinishScrollingFrom: self.selectedViewController!, destinationViewController: self.leftViewController!, transitionSuccessful: false)
            }
            self.didFinishScrollingCompletionHandler?(false)
            self.didFinishScrollingCompletionHandler = nil
            if self.loadNewAdjoiningViewControllersOnFinish {
                if (self.navigationDirection == .Forward) {
                    self.loadRightViewControllerForSelectedViewController(selectedViewController: selectedViewController)
                } else if (self.navigationDirection == .Reverse) {
                    self.loadLeftViewControllerForSelectedViewController(selectedViewController: selectedViewController)
                }
            }
        }
        self.navigationDirection = nil
        self.scrolling = false
    }
    
    private func loadLeftViewControllerForSelectedViewController(selectedViewController:UIViewController) {
        // Retreive the new left controller from the data source if available, otherwise set as nil
        if let leftViewController = self.dataSource?.ss_pageViewController(pageViewController: self, viewControllerLeftOfViewController: selectedViewController) {
            self.leftViewController = leftViewController
        } else {
            self.leftViewController = nil
        }
    }
    
    private func loadRightViewControllerForSelectedViewController(selectedViewController:UIViewController) {
        // Retreive the new right controller from the data source if available, otherwise set as nil
        if let rightViewController = self.dataSource?.ss_pageViewController(pageViewController: self, viewControllerRightOfViewController: selectedViewController) {
            self.rightViewController = rightViewController
        } else {
            self.rightViewController = nil
        }
    }
    
    
    // MARK: - View Management
    
    private func addChildIfNeeded(viewController: UIViewController) {
        self.scrollView.addSubview(viewController.view)
        self.addChild(viewController)
        viewController.didMove(toParent: self)
    }
    
    private func removeChildIfNeeded(viewController: UIViewController?) {
        viewController?.view.removeFromSuperview()
        viewController?.didMove(toParent: nil)
        viewController?.removeFromParent()
    }
    
    func layoutViews() {
        let viewWidth = self.view.bounds.width
        let viewHeight = self.view.bounds.height
        var leftInset:CGFloat = 0
        var rightInset:CGFloat = 0
        if (self.leftViewController == nil) {
            leftInset = -viewWidth
        }
        if (self.rightViewController == nil) {
            rightInset = -viewWidth
        }
        self.adjustingContentOffset = true
        self.scrollView.contentOffset = CGPoint(x: viewWidth, y: 0)
        self.scrollView.contentInset = UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: rightInset)
        self.adjustingContentOffset = false
        self.leftViewController?.view.frame = CGRect(x: 0, y: 0, width: viewWidth, height: viewHeight)
        self.selectedViewController?.view.frame = CGRect(x: viewWidth, y: 0, width: viewWidth, height: viewHeight)
        self.rightViewController?.view.frame = CGRect(x: viewWidth * 2, y: 0, width: viewWidth, height: viewHeight)
    }
    
    private func willScrollFromViewController(startingViewController: UIViewController?, destinationViewController: UIViewController) {
        if (startingViewController != nil) {
            self.delegate?.ss_pageViewController?(pageViewController: self, willStartScrollingFrom: startingViewController!, destinationViewController: destinationViewController)
        }
        
        destinationViewController.beginAppearanceTransition(true, animated: self.transitionAnimated)
        startingViewController?.beginAppearanceTransition(false, animated: self.transitionAnimated)
        self.addChildIfNeeded(viewController: destinationViewController)
    }
    
    private func didFinishScrollingToViewController(viewController: UIViewController) {
        self.loadViewControllers(selectedViewController: viewController)
        self.layoutViews()
    }
    
    public func scrollForwardViewDidScroll() {
        self.willScrollFromViewController(startingViewController: self.selectedViewController, destinationViewController: self.rightViewController!)
        if (self.selectedViewController != nil) {
            self.delegate?.ss_pageViewController?(pageViewController: self, isScrollingFrom: self.selectedViewController!, destinationViewController: self.rightViewController!, progress: 1)
        }
        self.didFinishScrollingToViewController(viewController: self.rightViewController!)
        
    }
    
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if !adjustingContentOffset {
            let viewWidth = self.view.bounds.width
            let progress = (scrollView.contentOffset.x - viewWidth) / viewWidth
            // Scrolling forward / right
            if (progress > 0) {
                if (self.rightViewController != nil) {
                    if !scrolling { // call willScroll once
                        self.willScrollFromViewController(startingViewController: self.selectedViewController, destinationViewController: self.rightViewController!)
                        self.scrolling = true
                    }
                    if self.navigationDirection == .Reverse { // check if direction changed
                        self.didFinishScrollingToViewController(viewController: self.selectedViewController!)
                        self.willScrollFromViewController(startingViewController: self.selectedViewController, destinationViewController: self.rightViewController!)
                    }
                    self.navigationDirection = .Forward
                    if (self.selectedViewController != nil) {
                        self.delegate?.ss_pageViewController?(pageViewController: self, isScrollingFrom: self.selectedViewController!, destinationViewController: self.rightViewController!, progress: progress)
                    }
                }
                // Scrolling reverse / left
            } else if (progress < 0) {
                if (self.leftViewController != nil) {
                    if !scrolling { // call willScroll once
                        self.willScrollFromViewController(startingViewController: self.selectedViewController, destinationViewController: self.leftViewController!)
                        self.scrolling = true
                    }
                    if self.navigationDirection == .Forward { // check if direction changed
                        self.didFinishScrollingToViewController(viewController: self.selectedViewController!)
                        self.willScrollFromViewController(startingViewController: self.selectedViewController, destinationViewController: self.leftViewController!)
                    }
                    self.navigationDirection = .Reverse
                    if (self.selectedViewController != nil) {
                        self.delegate?.ss_pageViewController?(pageViewController: self, isScrollingFrom: self.selectedViewController!, destinationViewController: self.leftViewController!, progress: progress)
                    }
                }
                // At zero
            } else {
                if (self.navigationDirection == .Forward) {
                    self.delegate?.ss_pageViewController?(pageViewController: self, isScrollingFrom: self.selectedViewController!, destinationViewController: self.rightViewController!, progress: progress)
                } else if (self.navigationDirection == .Reverse) {
                    self.delegate?.ss_pageViewController?(pageViewController: self, isScrollingFrom: self.selectedViewController!, destinationViewController: self.leftViewController!, progress: progress)
                }
            }
            // Thresholds to update view layouts call delegates
            if (progress >= 1 && self.rightViewController != nil) {
                self.didFinishScrollingToViewController(viewController: self.rightViewController!)
            } else if (progress <= -1  && self.leftViewController != nil) {
                self.didFinishScrollingToViewController(viewController: self.leftViewController!)
            } else if (progress == 0  && self.selectedViewController != nil) {
                self.didFinishScrollingToViewController(viewController: self.selectedViewController!)
            }
        }
        
    }
    
    public func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.transitionAnimated = true
    }
    
    public func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        // setContentOffset is called to center the selected view after bounces
        // This prevents yucky behavior at the beginning and end of the page collection by making sure setContentOffset is called only if...
        if  (self.leftViewController != nil && self.rightViewController != nil) || // It isn't at the beginning or end of the page collection
            (self.rightViewController != nil && self.leftViewController == nil && scrollView.contentOffset.x > abs(scrollView.contentInset.left)) || // If it's at the beginning of the collection, the decelleration can't be triggered by scrolling away from, than torwards the inset
            (self.leftViewController != nil && self.rightViewController == nil && scrollView.contentOffset.x < abs(scrollView.contentInset.right)) { // Same as the last condition, but at the end of the collection
            scrollView.setContentOffset(CGPoint(x: self.view.bounds.width, y: 0), animated: true)
        }
    }
    
}
