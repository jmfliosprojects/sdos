//
//  BaseAnyControllerDelegate.swift
//  SkeletonSwift
//
//  Created by Juan Miguel Fernández on 06/09/2019.
//  Copyright © 2019 Juan Miguel Fernández Lerena. All rights reserved.
//

import Foundation

/// Protocol for view controller.
public protocol BaseAnyControllerDelegate: NSObjectProtocol {
    
    /// Configure reachability.
    /// - throws:
    func configureReachablity()
    
    /// Configure dynamic type.
    /// - throws:
    func configureDynamicType()
    
    /// Show indicator with not reachable.
    /// - throws:
    func showIndicatorWithNotReachable()
    
    /// Show indicator with is reachable.
    /// - throws:
    func showIndicatorWithIsReachable()
    
    /// Update preferred font adjustment.
    /// - throws:
    func updatePreferredFontAdjustment()
    
    /// Perform reload remote.
    /// - throws:
    func performReloadRemote()
    
}



