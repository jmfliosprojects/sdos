//
//  BaseViewController.swift
//  SkeletonSwift
//
//  Created by Juan Miguel Fernández on 06/09/2019.
//  Copyright © 2019 Juan Miguel Fernández Lerena. All rights reserved.
//

import Foundation
import UIKit
import Connection
import Reachability

/// Controller of base view.
public class BaseViewController: UIViewController {
    
    /// Delegate for controler.
    public var delegate: BaseAnyControllerDelegate? = nil
    
    /// Manager of reachability.
    public var reachabilityManager: ReachabilityManager = ReachabilityManager.sharedInstance
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Navigation
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    override public func prepare(for segue: UIStoryboardSegue,
                                 sender: Any?) {
        
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Public methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Lifecycle
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Managing the View
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.configureReachablity()
        self.configureDynamicType()
        self.updatePreferredFontAdjustment()
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Responding to View Events
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override public func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
    }
    
    override public func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override public func viewWillTransition(to size: CGSize,
                                            with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to:size, with: coordinator)
        
        coordinator.animate(alongsideTransition: { (UIViewControllerTransitionCoordinatorContext) -> Void in
            
            let orient = UIApplication.shared.statusBarOrientation
            
            switch orient {
            case .portrait:
                
                break
            case .portraitUpsideDown:
                
                break
            case .landscapeLeft:
                
                break
            case .landscapeRight:
                
                break
            default:
                
                break
            }
            
        }, completion: { (UIViewControllerTransitionCoordinatorContext) -> Void in
            
        })
    }
    
    override public func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override public var prefersStatusBarHidden: Bool {
        get {
            return false
        }
    }
    
    override public var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.default
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Delegate methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Private methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
}

////////////////////////////////////////////////////////////////////////////////////////////
// MARK: BaseAnyControllerDelegate
////////////////////////////////////////////////////////////////////////////////////////////

extension BaseViewController: BaseAnyControllerDelegate {
    
    /// Configure reachability.
    /// - throws:
    @objc public func configureReachablity() {
        reachabilityManager.delegate = self
    }
    
    /// Configure dynamic type.
    /// - throws:
    @objc public func configureDynamicType() {
        self.delegate = self
        NotificationCenter.default.addObserver(forName: UIContentSizeCategory.didChangeNotification, object: nil, queue: nil) { (notification) in
            self.delegate?.updatePreferredFontAdjustment()
        }
    }
    
    /// Show indicator with not reachable.
    /// - throws:
    @objc public func showIndicatorWithNotReachable() {
        
    }
    
    /// Show indicator with is reachable.
    /// - throws:
    @objc public func showIndicatorWithIsReachable() {
    }
    
    /// Perform reload remote.
    /// - throws:
    @objc public func performReloadRemote() {
        
    }
    
    /// Update preferred font adjustment.
    /// - throws:
    @objc public func updatePreferredFontAdjustment() {
    }
    
}

////////////////////////////////////////////////////////////////////////////////////////////
// MARK: ReachabilityDelegate
////////////////////////////////////////////////////////////////////////////////////////////

extension BaseViewController: ReachabilityDelegate {
    
    /// Reachability status change hander.
    /// - parameters:
    ///   - reachability: Reachability.
    /// - throws:
    public func reachabilityStatusChangeHandler(reachability: Reachability) {
        if reachability.connection != .none {
            self.showIndicatorWithIsReachable()
        } else {
            self.showIndicatorWithNotReachable()
        }
    }
    
}
