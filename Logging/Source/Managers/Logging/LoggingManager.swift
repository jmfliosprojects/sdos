//
//  LoggingManager.swift
//  Logging
//
//  Created by Juan Miguel Fernández Lerenaon 04/09/2019.
//  Copyright © 2019 JUAN MIGUEL. All rights reserved.
//

import Foundation
import SwiftyBeaver

/// Constants for manager.
public struct LoggingConstants {
    
    /// Literal for log.
    public static let SEPARATOR = " - "
    /// Key for development environment.
    public static let DEVELOPMENT = "Development"
    /// Key for distribution environment.
    public static let DISTRIBUTION = "Distribution"
    
}

/// Manager of logging.
public class LoggingManager: NSObject {
    
    /// Shared instance.
    static let sharedInstance = LoggingManager()
    /// Log.
    static let log = SwiftyBeaver.self
    /// Console destination.
    static var console: ConsoleDestination!
    /// File destination.
    static var file: FileDestination!
    /// Platform destination.
    static var platform: SBPlatformDestination!
    
    /// Init.
    private override init() {
        
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Public methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Setting logging.
    ///   - appID: App ID.
    ///   - appSecret: App Secret.
    ///   - encryptionKey: Encryption Key.
    ///   - mode: Mode: [development | distribution].
    /// - throws:
    public static func settingLogging(appID: String,
                                      appSecret: String,
                                      encryptionKey: String,
                                      environment: String) {
        // Level
        var level = SwiftyBeaver.Level.debug
        if environment == LoggingConstants.DEVELOPMENT {
            level = SwiftyBeaver.Level.verbose
        }
        // Add log destinations.
        LoggingManager.console = ConsoleDestination()  // log to Xcode Console
        LoggingManager.console?.format = "$Dyyyy-MM-dd HH:mm:ss.SSS$d $C$L$c: $M"  // full datetime, colored log level and message
        LoggingManager.file = FileDestination()  // log to default swiftybeaver.log file
        LoggingManager.file.minLevel = level // Level info
        LoggingManager.platform = SBPlatformDestination(appID: appID, appSecret: appSecret, encryptionKey: encryptionKey) // to cloud
        // Add the destinations to SwiftyBeaver
        LoggingManager.log.addDestination(LoggingManager.platform)
        LoggingManager.log.addDestination(LoggingManager.console)
        LoggingManager.log.addDestination(LoggingManager.file)
    }
    
    /// Setting analytics user name.
    ///   - userName: User name.
    /// - throws:
    public static func settingAnalyticsUserName(userName: String) {
        LoggingManager.platform.analyticsUserName = userName
    }
    
    /// Verbose.
    /// - parameters:
    ///   - message: Message.
    /// - throws:
    public static func verbose(_ message: String) {
        LoggingManager.log.verbose(message)  // prior 1, VERBOSE in silver
    }
    
    /// Debug.
    /// - parameters:
    ///   - message: Message.
    /// - throws:
    public static func debug(_ message: String) {
        LoggingManager.log.debug(message)  // prior 2, DEBUG in blue
    }
    
    /// Info.
    /// - parameters:
    ///   - message: Message.
    /// - throws:
    public static func info(_ message: String) {
        LoggingManager.log.info(message)   // prior 3, INFO in green
    }
    
    /// Warning.
    /// - parameters:
    ///   - message: Message.
    /// - throws:
    public static func warning(_ message: String) {
        LoggingManager.log.warning(message)  // prior 4, WARNING in yellow
    }
    
    /// Error.
    /// - parameters:
    ///   - message: Message.
    /// - throws:
    public static func error(_ message: String) {
        LoggingManager.log.error(message)  // prior 5, ERROR in red
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Private methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
}


