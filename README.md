# SDOS

Technical test.


## Overview ##

**_Compability_**: Requires _iOS 12.4_ or later. Compatible with _iPhone_, _iPad_, and _iPod touch_.

**_Languages_**: _English_ and _Spanish_.

**_Settings_**: Integrates a custom configuration in _Settings_. Possibility to modify api from _Settings_.

**_Accesibility_**: Supports _Dynamic Type_ will adjust to your preferred reading size below.

**_Respond to Universal Links_**: Supports pass the url to the handle deep link call.

**_Real time analytics and monitoring of crashes_**: Supports real time analytics and monitoring of crashes.


## Installation

### CocoaPods

To create pod dependencies:

````ruby
pod install
````


## Application Architecture ##

The **_sdos_** project includes iOS app targets, iOS, and frameworks containing shared code.


## Version ##

-   **_v.1.0.2_**: Finished.


## Build Requirements ##

-   _Xcode 10.3_ or later.
-   _iOS 12.4_ SDK or later.


## Written in Swift ##

This project is provided in _Swift 5.0_.

###  Swift Compiler - Custom Flags ###

Target:  **_sdos_**

_Other Swift Flags_:

For initial data load:  `-DINITIAL_LOAD`
For data load: `-DSERVER`


## Frameworks Requirements ##

### Targets ###

-   **_Skeleton (1.0.0)_**
-  **_Logging (1.0.0)_**

### Pods ###

-   **_Crashlytics (3.13.4)_**
-   **_Fabric (1.10.2)_**
-   **_AlamofireImage (3.5.2)_**

## Release Notes ##

-   **_v.1.0.0_**: Initial version.
-   **_v.1.0.1_**: Fixes bugs.
-   **_v.1.0.2_**: Finished.


**Copyright (C) 2019 SDOS All rights reserved.**

