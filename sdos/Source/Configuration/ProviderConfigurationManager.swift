//
//  DataManager.swift
//  sdos
//
//  Created by Juan Miguel Fernández on 05/09/2019.
//  Copyright © 2019 Juan Miguel Fernández Lerena. All rights reserved.
//

import Foundation

/// Constants for manager.
struct ProviderConfigurationConstants {
    
    static let CONFIGURATION_TYPE = "plist"
    static let PROVIDER = "Provider"
    
}

/// Manager of provider configuration.
class ProviderConfigurationManager: NSObject {
    
    /// Shared instance.
    static let sharedInstance = ProviderConfigurationManager()
    
    private static var _path: String = ""
    private static var _plistXML: NSData = NSData()
    private static var _provider: String = ""
    
    /// Init.
    private override init() {
        
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Public methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Setting Configuration.
    /// - throws: ConfigurationError.ErrorSerialization
    static func settingConfigurationWithBundleName() throws {
        let targetName = Bundle.main.infoDictionary?["TargetName"] as! String
        let configurationName = Bundle.main.infoDictionary?["NSAppProviderConfigurationName"] as! String
        self.settingConfiguration(bundleName: targetName, configurationName: configurationName, configurationType: ProviderConfigurationConstants.CONFIGURATION_TYPE)
        ProviderConfigurationManager._provider = try ProviderConfigurationManager.property(key: ProviderConfigurationConstants.PROVIDER) as? String ?? ""
    }
    
    /// Obtain Provider.
    /// - returns: Provider.
    static func providerVendor() -> String {
        return ProviderConfigurationManager._provider
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Private methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Setting Configuration.
    /// - parameters:
    ///   - bundleName: Bundle name.
    ///   - configurationName: Configuration name.
    ///   - configurationType: Configuration type.
    /// - throws:
    private static func settingConfiguration(bundleName: String,
                                             configurationName: String,
                                             configurationType: String) {
        let bundleName = Bundle.main.infoDictionary?["CFBundleName"] as! String
        if (bundleName as String == bundleName) {
            _path = (Bundle.main.path(forResource: configurationName, ofType: configurationType))!
        }
        _plistXML = FileManager.default.contents(atPath: _path) as NSData? ?? NSData()
    }
    
    /// Obtain value of key from plist.
    /// - parameters:
    ///   - key: Key.
    /// - throws: ConfigurationError.ErrorSerialization
    /// - returns: Server.
    private static func property(key: String) throws -> AnyObject? {
        var propertyList: NSDictionary!
        let failureReason = "There was an error recovering property from the plist."
        do {
            try propertyList = PropertyListSerialization.propertyList(from: _plistXML as Data, options: PropertyListSerialization.MutabilityOptions.mutableContainersAndLeaves, format: nil) as? NSDictionary
        } catch {
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = NSLocalizedString("Error while recovering file properties", comment: "") as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?
            dict[NSUnderlyingErrorKey] = error as NSError
            let propertyRecoveredError = NSError(domain: ErrorApp.EXCEPTION_CONFIGURATION_DOMAIN, code: ErrorApp.CODE_9999_CONFIGURATION, userInfo: dict)
            throw propertyRecoveredError
        }
        return propertyList.object(forKey: key) as AnyObject?
    }
    
}
