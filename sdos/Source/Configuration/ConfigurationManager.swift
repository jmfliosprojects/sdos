//
//  ConfigurationManager.swift
//  sdos
//
//  Created by Juan Miguel Fernández on 05/09/2019.
//  Copyright © 2019 Juan Miguel Fernández Lerena. All rights reserved.
//

import Foundation

/// Constants for manager.
struct ConfigurationConstants {
    
    static let CONFIGURATION_TYPE = "plist"
    static let SWIFTYBEAVER_ENVIRONMENT_DEBUG = "SwiftyBeaver Environment debug"
    static let SWIFTYBEAVER_APP_ID = "SwiftyBeaver App ID"
    static let SWIFTYBEAVER_APP_SECRET = "SwiftyBeaver App Secret"
    static let SWIFTYBEAVER_ENCRYPTION_KEY = "SwiftyBeaver Encryption Key"
    static let API_URL = "API URL"
    static let API_KEY = "API KEY"
    
}

/// Constants for manager.
struct ConfigurationUserDefaultsForSettingsConstants {

    static let APP_INSTALLATION = "NSAppInstallation"
    static let APP_DEBUG = "NSAppDebug"
    static let APP_VERSION = "NSAppVersion"
    static let APP_ENTITY = "NSAppEntity"
    static let APP_SERVER = "NSAppServer"
    static let APP_ENTERPRISE = "NSAppEnterprise"
    static let UUID_DEVICE = "NSAppUUIDDevice"
    static let APP_INSTALATION_DATE = "NSAppInstalationDate"
    
}

/// Manager of configuration.
class ConfigurationManager: NSObject {
    
    /// Shared instance.
    static let sharedInstance = ConfigurationManager()
    
    private static var _path: String = ""
    private static var _plistXML: NSData = NSData()
    private static var _versionApp: String = ""
    private static var _swiftyBeaverEnvironmentDebug: String = ""
    private static var _swiftyBeaverAppID: String = ""
    private static var _swiftyBeaverAppSecret: String = ""
    private static var _swiftyBeaverEncryptionKey: String = ""
    private static var _apiURL: String = ""
    private static var _apiKey: String = ""
    
    /// Init.
    private override init() {
        
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Public methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Setting Configuration.
    /// - throws: ConfigurationError.ErrorSerialization
    static func settingConfigurationWithBundleName() throws {
        let targetName = Bundle.main.infoDictionary?["TargetName"] as! String
        let configurationName = Bundle.main.infoDictionary?["NSAppConfigurationName"] as! String
        self.settingConfiguration(bundleName: targetName, configurationName: configurationName, configurationType: ConfigurationConstants.CONFIGURATION_TYPE)
        ConfigurationManager._versionApp = (Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String) + " (" + (Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String) + ")"
        ConfigurationManager._swiftyBeaverEnvironmentDebug = try ConfigurationManager.property(key: ConfigurationConstants.SWIFTYBEAVER_ENVIRONMENT_DEBUG) as? String ?? ""
        ConfigurationManager._swiftyBeaverAppID = try ConfigurationManager.property(key: ConfigurationConstants.SWIFTYBEAVER_APP_ID) as? String ?? ""
        ConfigurationManager._swiftyBeaverAppSecret = try ConfigurationManager.property(key: ConfigurationConstants.SWIFTYBEAVER_APP_SECRET)as? String ?? ""
        ConfigurationManager._swiftyBeaverEncryptionKey = try ConfigurationManager.property(key: ConfigurationConstants.SWIFTYBEAVER_ENCRYPTION_KEY) as? String ?? ""
        ConfigurationManager._apiURL = try ConfigurationManager.property(key: ConfigurationConstants.API_URL) as? String ?? ""
        ConfigurationManager._apiKey = try ConfigurationManager.property(key: ConfigurationConstants.API_KEY) as? String ?? ""
    }
    
    /// Obtain version.
    /// - returns: Version.
    static func versionApp() -> String {
        return ConfigurationManager._versionApp
    }
    
    /// Obtain SwiftyBeaverEnvironmentDebug.
    /// - returns: SwiftyBeaverEnvironmentDebug.
    static func swiftyBeaverEnvironmentDebug() -> String {
        return ConfigurationManager._swiftyBeaverEnvironmentDebug
    }
    
    /// Obtain SwiftyBeaverAppID.
    /// - returns: SwiftyBeaverAppID.
    static func swiftyBeaverAppID() -> String {
        return ConfigurationManager._swiftyBeaverAppID
    }
    
    /// Obtain SwiftyBeaverAppSecret.
    /// - returns: SwiftyBeaverAppSecret.
    static func swiftyBeaverAppSecret() -> String {
        return ConfigurationManager._swiftyBeaverAppSecret
    }
    
    /// Obtain SwiftyBeaverEncryptionKey.
    /// - returns: SwiftyBeaverEncryptionKey.
    static func swiftyBeaverEncryptionKey() -> String {
        return ConfigurationManager._swiftyBeaverEncryptionKey
    }
    
    /// Obtain Api URL.
    /// - returns: Api URL.
    static func apiURL() -> String {
        return ConfigurationManager._apiURL
    }
    
    /// Obtain Api Key.
    /// - returns: Api Key.
    static func apiKey() -> String {
        return ConfigurationManager._apiKey
    }
    
    /// Save NSAppVersion.
    /// - parameters:
    ///   - string: NSAppVersion.
    /// - throws:
    static func appUserDefaultsForSettingsVersion(string: String) {
        UserDefaults.standard.set(string, forKey: ConfigurationUserDefaultsForSettingsConstants.APP_VERSION)
    }
    
    /// Save NSAppServer.
    /// - parameters:
    ///   - string: NSAppServer.
    /// - throws:
    static func appUserDefaultsForSettingsServer(string: String) {
        UserDefaults.standard.set(string, forKey: ConfigurationUserDefaultsForSettingsConstants.APP_SERVER)
    }
    
    /// Get NSAppServer.
    /// - throws:
    /// - returns: NSAppEntity.
    static func appUserDefaultsForSettingsServer() -> String {
        let result = UserDefaults.standard.object(forKey: ConfigurationUserDefaultsForSettingsConstants.APP_SERVER) as? String ?? ""
        return result
    }
    
    /// Save NSAppEntity.
    /// - parameters:
    ///   - string: NSAppEntity.
    /// - throws:
    static func appUserDefaultsForSettingsEntity(string: String) {
        UserDefaults.standard.set(string, forKey: ConfigurationUserDefaultsForSettingsConstants.APP_ENTITY)
    }
    
    /// Get NSAppEntity.
    /// - throws:
    /// - returns: NSAppEntity.
    static func appUserDefaultsForSettingsEntity() -> String {
        let result = UserDefaults.standard.object(forKey: ConfigurationUserDefaultsForSettingsConstants.APP_ENTITY) as? String ?? ""
        return result
    }
    
    /// Save NSAppEnterprise.
    /// - parameters:
    ///   - string: NSAppEnterprise.
    /// - throws:
    static func appUserDefaultsForSettingsEnterprise(string: String) {
        UserDefaults.standard.set(string, forKey: ConfigurationUserDefaultsForSettingsConstants.APP_ENTERPRISE)
    }
    
    /// Init UUID Device.
    /// - throws:
    static func initUUIDDevice() {
        let uuid = NSUUID().uuidString
        ConfigurationManager.appUserDefaultsForSettingsUUIDDevice(string: uuid)
    }
    
    /// Init Instalation date.
    /// - throws:
    static func initInstalationDate() {
        ConfigurationManager.appUserDefaultsForSettingsInstalationDate(date: Int64(NSDate().timeIntervalSince1970))
    }
    
    /// Identifier of UUID Device.
    /// - throws:
    /// - returns: Identifier of UUID Device.
    static func appUserDefaultsForSettingsUUIDDevice() -> String? {
        let result = UserDefaults.standard.object(forKey: ConfigurationUserDefaultsForSettingsConstants.UUID_DEVICE) as? String
        return result
    }
    
    /// Save identifier of UUID Device.
    /// - parameters:
    ///   - string: Identifier of UUID Device.
    /// - throws:
    static func appUserDefaultsForSettingsUUIDDevice(string: String) {
        UserDefaults.standard.set(string, forKey: ConfigurationUserDefaultsForSettingsConstants.UUID_DEVICE)
    }
    
    /// Instalation date.
    /// - throws:
    /// - returns: Instalation date.
    static func appUserDefaultsForSettingsInstalationDate() -> Int64? {
        let result = UserDefaults.standard.object(forKey: ConfigurationUserDefaultsForSettingsConstants.APP_INSTALATION_DATE) as? Int64
        return result
    }
    
    /// Save Instalation date.
    /// - parameters:
    ///   - date: Instalation date.
    /// - throws:
    static func appUserDefaultsForSettingsInstalationDate(date: Int64) {
        UserDefaults.standard.set(date, forKey: ConfigurationUserDefaultsForSettingsConstants.APP_INSTALATION_DATE)
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Private methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Setting Configuration.
    /// - parameters:
    ///   - bundleName: Bundle name.
    ///   - configurationName: Configuration name.
    ///   - configurationType: Configuration type.
    /// - throws:
    private static func settingConfiguration(bundleName: String,
                                             configurationName: String,
                                             configurationType: String) {
        let bundleName = Bundle.main.infoDictionary?["CFBundleName"] as! String
        if (bundleName as String == bundleName) {
            _path = (Bundle.main.path(forResource: configurationName, ofType: configurationType))!
        }
        _plistXML = FileManager.default.contents(atPath: _path) as NSData? ?? NSData()
    }
    
    /// Obtain value of key from plist.
    /// - parameters:
    ///   - key: Key.
    /// - throws: ConfigurationError.ErrorSerialization
    /// - returns: Server.
    private static func property(key: String) throws -> AnyObject? {
        var propertyList: NSDictionary!
        let failureReason = "There was an error recovering property from the plist."
        do {
            try propertyList = PropertyListSerialization.propertyList(from: _plistXML as Data, options: PropertyListSerialization.MutabilityOptions.mutableContainersAndLeaves, format: nil) as? NSDictionary
        } catch {
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = NSLocalizedString("Error while recovering file properties", comment: "") as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?
            dict[NSUnderlyingErrorKey] = error as NSError
            let propertyRecoveredError = NSError(domain: ErrorApp.EXCEPTION_CONFIGURATION_DOMAIN, code: ErrorApp.CODE_9999_CONFIGURATION, userInfo: dict)
            throw propertyRecoveredError
        }
        return propertyList.object(forKey: key) as AnyObject?
    }
    
}
