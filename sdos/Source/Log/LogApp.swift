//
//  LogApp.swift
//  sdos
//
//  Created by Juan Miguel Fernández on 05/09/2019.
//  Copyright © 2019 Juan Miguel Fernández Lerena. All rights reserved.
//

import Foundation
import Logging

/// Manager of Log.
class LogApp: NSObject {
    
    /// Shared instance.
    static let sharedInstance = LogApp()
    
    /// Init.
    private override init() {
        
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Public methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Setting logging.
    ///   - appID: App ID.
    ///   - appSecret: App Secret.
    ///   - encryptionKey: Encryption Key.
    /// - throws:
    public static func settingLogging(appID: String,
                                      appSecret: String,
                                      encryptionKey: String) {
        LoggingManager.settingLogging(appID: ConfigurationManager.swiftyBeaverAppID(), appSecret: ConfigurationManager.swiftyBeaverAppSecret(), encryptionKey: ConfigurationManager.swiftyBeaverEncryptionKey(), environment: ConfigurationManager.swiftyBeaverEnvironmentDebug())
    }
    
    /// Setting analytics user name.
    ///   - userName: User name.
    /// - throws:
    public static func settingAnalytics(userName: String) {
        LoggingManager.settingAnalyticsUserName(userName: userName)
    }
    
    /// Verbose.
    /// - parameters:
    ///   - message: Message.
    /// - throws:
    static func verbose(_ message: String) {
        if UserManager.isDebugMode() {
            LoggingManager.verbose(message)  // prio 1, VERBOSE in silver
        }
    }
    
    /// Debug.
    /// - parameters:
    ///   - message: Message.
    /// - throws:
    static func debug(_ message: String) {
        if UserManager.isDebugMode() {
            LoggingManager.debug(message)  // prio 2, DEBUG in blue
        }
    }
    
    /// Info.
    /// - parameters:
    ///   - message: Message.
    /// - throws:
    static func info(_ message: String) {
        if UserManager.isDebugMode() {
            LoggingManager.info(message)   // prio 3, INFO in green
        }
    }
    
    /// Warning.
    /// - parameters:
    ///   - message: Message.
    /// - throws:
    static func warning(_ message: String) {
        if UserManager.isDebugMode() {
            LoggingManager.warning(message)  // prio 4, WARNING in yellow
        }
    }
    
    /// Error.
    /// - parameters:
    ///   - message: Message.
    /// - throws:
    static func error(_ message: String) {
        if UserManager.isDebugMode() {
            LoggingManager.error(message)  // prio 5, ERROR in red
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Private methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
}

