//
//  SearchService.swift
//  sdos
//
//  Created by Juan Miguel Fernández on 06/09/2019.
//  Copyright © 2019 Juan Miguel Fernández Lerena. All rights reserved.
//

import Foundation
import Logging
import SwiftyJSON

/// Constants for parsing.
struct SearchKey {
    
    /// Keys.
    static let identifier = "id"
    static let text = "text"
    static let created = "created"
    
}

/// Service for Search.
class SearchService: NSObject {
    
    /// Shared instance.
    static let sharedInstance = SearchService()
    
    /// Init.
    private override init() {
        
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Public methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Perform new search.
    /// - parameters:
    ///     - text: Text.
    ///     - createdDate: Date.
    /// - throws:
    static func newSearch(text: String, createdDate: Date) {
        do {
            let dictRemote = NSMutableDictionary()
            dictRemote.setValue(createdDate.timeIntervalSince1970, forKey: SearchKey.identifier)
            dictRemote.setValue(text, forKey: SearchKey.text)
            dictRemote.setValue(createdDate.timeIntervalSince1970, forKey: SearchKey.created)
            let dict = NSMutableDictionary()
            dict.setValue(dictRemote.value(forKey: SearchKey.identifier) as? NSNumber, forKey: SearchAttribute.identifier)
            dict.setValue(dictRemote.value(forKey: SearchKey.text) as? String, forKey: SearchAttribute.text)
            dict.setValue(dictRemote.value(forKey: SearchKey.created) as? String, forKey: SearchAttribute.created)
            let _: Search? = SearchDAO.newObject(dict: dict)
            try SearchDAO.saveContext()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: SearchNotificationConstants.PERFORM_INSERT_SEARCH_DATA), object: nil)
        } catch let coreDataError as NSError  {
            LogApp.error(String(describing: SearchManager.self) + LoggingConstants.SEPARATOR + "Perform new search: \(coreDataError) \(coreDataError.localizedDescription)")
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = NSLocalizedString("Processing data", comment: "") as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = coreDataError.localizedFailureReason as AnyObject?
            dict[NSUnderlyingErrorKey] = coreDataError
            let error = NSError(domain: ErrorApp.EXCEPTION_BUSINESS_DOMAIN, code: ErrorApp.CODE_198_PROCESSING_DATA, userInfo: dict)
            let message = ErrorAppManager.messageErrorApp(appError: error, json: JSON.null)
            LogApp.error(String(describing: UserManager.self) + LoggingConstants.SEPARATOR + (message ?? "Error perform user data."))
        }
    }
    
    /// Delete all.
    /// - throws: Error.
    static func deleteAll() throws {
        try SearchDAO.deleteAllObjects()
    }
    
    /// Obtain all.
    /// - throws: Error.
    /// - returns: All objects.
    static func findAll() throws -> NSArray {
        return try SearchDAO.findAllObjects()
    }
    
    /// Find with identifier.
    /// - parameters:
    ///     - identifier: Identifier.
    /// - throws: Error.
    /// - returns: All objects.
    static func findAll(identifier: Int) throws -> NSArray {
        return try SearchDAO.findObject(identifier: identifier)
    }
    
    /// Find with identifier.
    /// - parameters:
    ///     - text: Text.
    /// - throws: Error.
    /// - returns: All objects.
    static func findAll(text: String) throws -> NSArray {
        return try SearchDAO.findObject(text: text)
    }
    
    /// Delete with identifier.
    /// - parameters:
    ///     - identifier: Identifier.
    /// - throws: Error.
    static func delete(identifier: Int) throws  {
        try SearchDAO.deleteObject(identifier: identifier)
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Private methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
}



