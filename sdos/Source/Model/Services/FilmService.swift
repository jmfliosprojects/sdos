//
//  FilmService.swift
//  sdos
//
//  Created by Juan Miguel Fernández on 08/09/2019.
//  Copyright © 2019 Juan Miguel Fernández Lerena. All rights reserved.
//

import Foundation
import Logging
import SwiftyJSON

/// Constants for parsing.
struct FilmKey {
    
    /// Keys.
    static let imdbID = "imdbID"
    static let image = "Poster"
    static let title = "Title"
    static let date = "Year"
    static let duration = "Runtime"
    static let genders = "Genre"
    static let synopsis = "Plot"
    static let web = "Website"
    
}

/// Service for Search.
class FilmService: NSObject {
    
    /// Shared instance.
    static let sharedInstance = FilmService()
    
    /// Init.
    private override init() {
        
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Public methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Save new object.
    /// - parameters:
    ///   - dataJSON: Data JSON.
    ///   - syncedAt: SyncedAt.
    /// - throws: Error
    static func saveArray(dataJSON: JSON?, syncedAt: NSNumber?) throws {
        if let empty = dataJSON?.isEmpty {
            if !empty {
                for (_, subJson) in dataJSON! {
                    let json = subJson.rawString() ?? ""
                    var object: Film?
                    let dict = NSMutableDictionary()
                    dict.setValue(subJson[FilmKey.imdbID].stringValue, forKey: FilmAttribute.imdbID)
                    dict.setValue(subJson[FilmKey.title].stringValue, forKey: FilmAttribute.title)
                    dict.setValue(subJson[FilmKey.date].stringValue, forKey: FilmAttribute.date)
                    dict.setValue(subJson[FilmKey.duration].stringValue, forKey: FilmAttribute.duration)
                    dict.setValue(subJson[FilmKey.genders].stringValue, forKey: FilmAttribute.genders)
                    dict.setValue(subJson[FilmKey.synopsis].stringValue, forKey: FilmAttribute.synopsis)
                    dict.setValue(subJson[FilmKey.web].stringValue, forKey: FilmAttribute.web)
                    dict.setValue(subJson[FilmKey.image].stringValue, forKey: FilmAttribute.image)
                    dict.setValue(NSDate().timeIntervalSince1970, forKey: FilmAttribute.identifier)
                    var arrayObjects: NSArray?
                    if subJson[FilmKey.title] != JSON.null {
                        arrayObjects = try FilmDAO.findObject(imdbID: subJson[FilmKey.imdbID].stringValue)
                    }
                    if arrayObjects?.count == 0 {
                        object = FilmDAO.newObject(dict: dict, json: json)
                        try FilmDAO.saveContext()
                    } else {
                        object = arrayObjects?.firstObject as? Film
                        let jsonObject = object?.json
                        if jsonObject != json {
                            object = FilmDAO.updateObject(dict: dict, object: object, json: json)
                            try FilmDAO.saveContext()
                        }
                    }
                }
            }
        }
    }
    
    /// Save new object.
    /// - parameters:
    ///   - dataJSON: Data JSON.
    ///   - syncedAt: SyncedAt.
    /// - throws: Error
    static func saveObject(dataJSON: JSON?, syncedAt: NSNumber?) throws {
        if let empty = dataJSON?.isEmpty {
            if !empty {
                let json = dataJSON?.rawString() ?? ""
                var object: Film?
                let dict = NSMutableDictionary()
                dict.setValue(dataJSON?[FilmKey.imdbID].stringValue, forKey: FilmAttribute.imdbID)
                dict.setValue(dataJSON?[FilmKey.title].stringValue, forKey: FilmAttribute.title)
                dict.setValue(dataJSON?[FilmKey.date].stringValue, forKey: FilmAttribute.date)
                dict.setValue(dataJSON?[FilmKey.duration].stringValue, forKey: FilmAttribute.duration)
                dict.setValue(dataJSON?[FilmKey.genders].stringValue, forKey: FilmAttribute.genders)
                dict.setValue(dataJSON?[FilmKey.synopsis].stringValue, forKey: FilmAttribute.synopsis)
                dict.setValue(dataJSON?[FilmKey.web].stringValue, forKey: FilmAttribute.web)
                dict.setValue(dataJSON?[FilmKey.image].stringValue, forKey: FilmAttribute.image)
                var arrayObjects: NSArray?
                if dataJSON?[FilmKey.imdbID] != JSON.null {
                    arrayObjects = try FilmDAO.findObject(imdbID: dataJSON?[FilmKey.imdbID].stringValue ?? "")
                }
                if arrayObjects?.count == 0 {
                    object = FilmDAO.newObject(dict: dict, json: json)
                    try FilmDAO.saveContext()
                } else {
                    object = arrayObjects?.firstObject as? Film
                    let jsonObject = object?.json
                    if jsonObject != json {
                        object = arrayObjects?.firstObject as? Film
                        object = FilmDAO.updateObject(dict: dict, object: object, json: json)
                        try FilmDAO.saveContext()
                    }
                }
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: ResultNotificationConstants.PERFORM_RESULT_DATA), object: nil)
            }
        }
    }
    
    /// Delete all.
    /// - throws: Error.
    static func deleteAll() throws {
        try FilmDAO.deleteAllObjects()
    }
    
    /// Obtain all.
    /// - throws: Error.
    /// - returns: All objects.
    static func findAll() throws -> NSArray {
        return try FilmDAO.findAllObjects()
    }
    
    /// Find with ID.
    /// - parameters:
    ///     - imdbID: imdbID.
    /// - throws: Error.
    /// - returns: All objects.
    static func findAll(imdbID: String) throws -> NSArray {
        return try FilmDAO.findObject(imdbID: imdbID)
    }
    
    /// Find with title.
    /// - parameters:
    ///     - title: title.
    /// - throws: Error.
    /// - returns: All objects.
    static func findAll(title: String) throws -> NSArray {
        return try FilmDAO.findObject(title: title)
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Private methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
}

