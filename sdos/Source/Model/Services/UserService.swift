//
//  UserService.swift
//  sdos
//
//  Created by Juan Miguel Fernández on 05/09/2019.
//  Copyright © 2019 Juan Miguel Fernández Lerena. All rights reserved.
//

import Foundation
import Logging
import SwiftyJSON

/// Constants for parsing.
struct UserKey {
    
    /// Keys.
    static let identifier = "id"
    static let name = "full_name"
    static let job = "current_job"
    static let emailAddress = "email"
    static let phoneNumber = "phone"
    
}

/// Service for User.
class UserService: NSObject {
    
    /// Shared instance.
    static let sharedInstance = UserService()
    
    /// Init.
    private override init() {
        
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Public methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Perform user data.
    /// - throws:
    static func preloadUserData() {
        #if INITIAL_LOAD
        let 💤 = 3
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(💤), execute: {
            do {
                let dictRemote = NSMutableDictionary()
                dictRemote.setValue(1, forKey: UserKey.identifier)
                dictRemote.setValue("Juan Miguel Fernández Lerena", forKey: UserKey.name)
                dictRemote.setValue("iOS Developer", forKey: UserKey.job)
                dictRemote.setValue("jm.fernandezlerena@gmail.com", forKey: UserKey.emailAddress)
                dictRemote.setValue("680 166 441", forKey: UserKey.phoneNumber)
                let dict = NSMutableDictionary()
                dict.setValue(dictRemote.value(forKey: UserKey.identifier) as? NSNumber, forKey: UserAttribute.identifier)
                dict.setValue(dictRemote.value(forKey: UserKey.name) as? String, forKey: UserAttribute.name)
                dict.setValue(dictRemote.value(forKey: UserKey.job) as? String, forKey: UserAttribute.job)
                dict.setValue(dictRemote.value(forKey: UserKey.emailAddress) as? String, forKey: UserAttribute.emailAddress)
                dict.setValue(dictRemote.value(forKey: UserKey.phoneNumber) as? String, forKey: UserAttribute.phoneNumber)
                let _: User? = UserDAO.newObject(dict: dict)
                try UserDAO.saveContext()
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: UserNotificationConstants.PERFORM_USER_DATA), object: nil)
            } catch let coreDataError as NSError  {
                LogApp.error(String(describing: UserManager.self) + LoggingConstants.SEPARATOR + "Perform user data: \(coreDataError) \(coreDataError.localizedDescription)")
                var dict = [String: AnyObject]()
                dict[NSLocalizedDescriptionKey] = NSLocalizedString("Processing data", comment: "") as AnyObject?
                dict[NSLocalizedFailureReasonErrorKey] = coreDataError.localizedFailureReason as AnyObject?
                dict[NSUnderlyingErrorKey] = coreDataError
                let error = NSError(domain: ErrorApp.EXCEPTION_BUSINESS_DOMAIN, code: ErrorApp.CODE_198_PROCESSING_DATA, userInfo: dict)
                let message = ErrorAppManager.messageErrorApp(appError: error, json: JSON.null)
                LogApp.error(String(describing: UserManager.self) + LoggingConstants.SEPARATOR + (message ?? "Error perform user data."))
            }
            // print(NSHomeDirectory())
        })
        #elseif SERVER
        #endif
    }
    
    /// Delete all.
    /// - throws: Error.
    static func deleteAll() throws {
        try UserDAO.deleteAllObjects()
    }
    
    /// Obtain all.
    /// - throws: Error.
    /// - returns: All objects.
    static func findAll() throws -> NSArray {
        return try UserDAO.findAllObjects()
    }
    
    /// Find with identifier.
    /// - parameters:
    ///     - identifier: Identifier.
    /// - throws: Error.
    /// - returns: All objects.
    static func findAll(identifier: Int) throws -> NSArray {
        return try UserDAO.findObject(identifier: identifier)
    }
    
    /// Delete with identifier.
    /// - parameters:
    ///     - identifier: Identifier.
    /// - throws: Error.
    static func delete(identifier: Int) throws  {
        try UserDAO.deleteObject(identifier: identifier)
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Private methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
}


