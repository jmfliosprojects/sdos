//
//  Film+CoreDataProperties.swift
//  sdos
//
//  Created by Juan Miguel Fernández on 08/09/2019.
//  Copyright © 2019 Juan Miguel Fernández Lerena. All rights reserved.
//
//

import Foundation
import CoreData


extension Film {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Film> {
        return NSFetchRequest<Film>(entityName: "Film")
    }

    @NSManaged public var imdbID: String?
    @NSManaged public var title: String?
    @NSManaged public var date: String?
    @NSManaged public var genders: String?
    @NSManaged public var duration: String?
    @NSManaged public var sypnosis: String?
    @NSManaged public var web: String?
    @NSManaged public var image: String?
    @NSManaged public var identifier: Int64
    @NSManaged public var json: String?

}
