//
//  Search+CoreDataClass.swift
//  sdos
//
//  Created by Juan Miguel Fernández on 06/09/2019.
//  Copyright © 2019 Juan Miguel Fernández Lerena. All rights reserved.
//
//

import Foundation
import CoreData


public class Search: NSManagedObject {

    @objc var committeeNameInitial: String {
        get {
            self.willAccessValue(forKey: "committeeNameInitial")
            let initial: String = NSLocalizedString("Latest Searches", comment: "")
            self.didAccessValue(forKey: "committeeNameInitial")
            return initial
        }
    }
    
}
