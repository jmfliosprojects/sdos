//
//  Search+CoreDataProperties.swift
//  sdos
//
//  Created by Juan Miguel Fernández on 06/09/2019.
//  Copyright © 2019 Juan Miguel Fernández Lerena. All rights reserved.
//
//

import Foundation
import CoreData


extension Search {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Search> {
        return NSFetchRequest<Search>(entityName: "Search")
    }

    @NSManaged public var identifier: Int32
    @NSManaged public var text: String?
    @NSManaged public var created: NSDate?
    @NSManaged public var user: User?

}
