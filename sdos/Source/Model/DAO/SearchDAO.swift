//
//  SearchDAO.swift
//  sdos
//
//  Created by Juan Miguel Fernández on 06/09/2019.
//  Copyright © 2019 Juan Miguel Fernández Lerena. All rights reserved.
//

import Foundation
import CoreData
import Logging

/// Constants for DAO.
struct SearchAttribute {
    
    /// Attributes.
    static let identifier = "identifier"
    static let text = "text"
    static let created = "created"
    /// Relationships.
    static let identifierUSser = "user.identifier"
    
}

/// Constants for DAO.
struct SearchEntity {
    
    /// Entity.
    static let name = "Search"
    
}

/// DAO for Search.
class SearchDAO: NSObject {
    
    /// Shared instance.
    static let sharedInstance = SearchDAO()
    
    /// Init.
    private override init() {
        
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Public methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Create new object.
    /// - parameters:
    ///   - dict: Dict.
    /// - throws:
    /// - returns: Object.
    static func newObject(dict: NSDictionary) -> Search? {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedObjectContext = appDelegate.managedObjectContext
        var object: Search?
        object = NSEntityDescription.insertNewObject(forEntityName: SearchEntity.name, into: managedObjectContext) as? Search
        if let identifier = dict.value(forKey: SearchAttribute.identifier) as? NSNumber {
            object?.identifier = identifier.int32Value
        }
        if let text = dict.value(forKey: SearchAttribute.text) as? String {
            object?.text = text
        }
        if let created = dict.value(forKey: SearchAttribute.created) as? NSNumber {
            object?.created = NSDate(timeIntervalSince1970: TimeInterval(created.intValue))
        }
        return object
    }
    
    /// Update new object.
    /// - parameters:
    ///   - dict: Dict.
    ///   - object: Search.
    /// - throws:
    /// - returns: Object.
    static func updateObject(dict: NSDictionary,
                             object: Search?) -> Search? {
        if let identifier = dict.value(forKey: SearchAttribute.identifier) as? NSNumber {
            object?.identifier = identifier.int32Value
        }
        if let text = dict.value(forKey: SearchAttribute.text) as? String {
            object?.text = text
        }
        if let created = dict.value(forKey: SearchAttribute.created) as? NSNumber {
            object?.created = NSDate(timeIntervalSince1970: TimeInterval(created.intValue))
        }
        return object
    }
    
    /// Save context.
    /// - throws: Error.
    @objc static func saveContext() throws {
        do {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let managedObjectContext = appDelegate.managedObjectContext
            try managedObjectContext.save()
        } catch let error as NSError  {
            LogApp.error(String(describing: SearchDAO.self) + LoggingConstants.SEPARATOR + "Fetch failed: \(error) \(error.localizedDescription)")
            let error = NSError(domain: ErrorApp.EXCEPTION_COREDATA_SAVE_DOMAIN, code: ErrorApp.CODE_997_COREDATA_SAVE, userInfo: error.userInfo)
            throw error
        }
    }
    
    /// Delete all objects.
    /// - throws: Error.
    @objc static func deleteAllObjects() throws {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedObjectContext = appDelegate.managedObjectContext
        let fetchRequest = NSFetchRequest<Search>(entityName: SearchEntity.name)
        fetchRequest.returnsObjectsAsFaults = false
        do {
            let fetchedData = try managedObjectContext.fetch(fetchRequest)
            for managedObject in fetchedData {
                let managedObjectData:NSManagedObject = managedObject as NSManagedObject
                managedObjectContext.delete(managedObjectData)
            }
        } catch let error as NSError {
            LogApp.error(String(describing: SearchDAO.self) + LoggingConstants.SEPARATOR + "Fetch failed: \(error) \(error.localizedDescription)")
            let error = NSError(domain: ErrorApp.EXCEPTION_COREDATA_DELETE_ALL_DOMAIN, code: ErrorApp.CODE_996_COREDATA_DELETE_ALL, userInfo: error.userInfo)
            throw error
        }
    }
    
    /// All objects.
    /// - throws: Fetch failed
    /// - returns: All objects.
    @objc static func findAllObjects() throws -> NSArray {
        var result: NSArray! = NSArray()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedObjectContext = appDelegate.managedObjectContext
        let entityDescription = NSEntityDescription.entity(forEntityName: SearchEntity.name, in: managedObjectContext)
        let sortDescriptor1 = NSSortDescriptor.init(key: SearchAttribute.identifier, ascending: true)
        let fetchRequest = NSFetchRequest<Search>(entityName: SearchEntity.name)
        fetchRequest.entity = entityDescription
        fetchRequest.sortDescriptors = [sortDescriptor1]
        fetchRequest.includesPropertyValues = true
        do {
            let fetchedData = try managedObjectContext.fetch(fetchRequest)
            result = fetchedData as NSArray
        } catch let error as NSError {
            LogApp.error(String(describing: SearchDAO.self) + LoggingConstants.SEPARATOR + "Fetch failed: \(error) \(error.localizedDescription)")
            let error = NSError(domain: ErrorApp.EXCEPTION_COREDATA_FIND_ALL_DOMAIN, code: ErrorApp.CODE_995_COREDATA_FIND_ALL, userInfo: error.userInfo)
            throw error
        }
        return result
    }
    
    /// Find object.
    /// - parameters:
    ///   - identifier: Identifier.
    /// - throws:
    /// - returns: Array with object.
    @objc static func findObject(identifier: Int) throws -> NSArray {
        var result: NSArray = NSArray()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedObjectContext = appDelegate.managedObjectContext
        let predicate1 = NSPredicate(format: SearchAttribute.identifier + " == %d", identifier)
        let compound =  NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [predicate1])
        let entityDescription = NSEntityDescription.entity(forEntityName: SearchEntity.name, in: managedObjectContext)
        let sortDescriptor1 = NSSortDescriptor.init(key: SearchAttribute.identifier, ascending: true)
        let fetchRequest = NSFetchRequest<Search>(entityName: SearchEntity.name)
        fetchRequest.entity = entityDescription
        fetchRequest.predicate = compound
        fetchRequest.sortDescriptors = [sortDescriptor1]
        fetchRequest.includesPropertyValues = true
        do {
            let fetchedData = try managedObjectContext.fetch(fetchRequest)
            result = fetchedData as NSArray
        } catch let error as NSError {
            LogApp.error(String(describing: SearchDAO.self) + LoggingConstants.SEPARATOR + "Fetch failed: \(error) \(error.localizedDescription)")
            let error = NSError(domain: ErrorApp.EXCEPTION_COREDATA_FIND_WITH_IDENTIFIER_DOMAIN, code: ErrorApp.CODE_994_COREDATA_FIND_WITH_IDENTIFIER, userInfo: error.userInfo)
            throw error
        }
        return result
    }
    
    /// Find object.
    /// - parameters:
    ///   - text: Text.
    /// - throws:
    /// - returns: Array with object.
    @objc static func findObject(text: String) throws -> NSArray {
        var result: NSArray = NSArray()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedObjectContext = appDelegate.managedObjectContext
        let predicate1 = NSPredicate(format: "text = %@", text)
        let compound =  NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [predicate1])
        let entityDescription = NSEntityDescription.entity(forEntityName: SearchEntity.name, in: managedObjectContext)
        let sortDescriptor1 = NSSortDescriptor.init(key: SearchAttribute.identifier, ascending: true)
        let fetchRequest = NSFetchRequest<Search>(entityName: SearchEntity.name)
        fetchRequest.entity = entityDescription
        fetchRequest.predicate = compound
        fetchRequest.sortDescriptors = [sortDescriptor1]
        fetchRequest.includesPropertyValues = true
        do {
            let fetchedData = try managedObjectContext.fetch(fetchRequest)
            result = fetchedData as NSArray
        } catch let error as NSError {
            LogApp.error(String(describing: SearchDAO.self) + LoggingConstants.SEPARATOR + "Fetch failed: \(error) \(error.localizedDescription)")
            let error = NSError(domain: ErrorApp.EXCEPTION_COREDATA_FIND_WITH_IDENTIFIER_DOMAIN, code: ErrorApp.CODE_994_COREDATA_FIND_WITH_IDENTIFIER, userInfo: error.userInfo)
            throw error
        }
        return result
    }
    
    /// Delete object.
    /// - parameters:
    ///   - identifier: Identifier.
    /// - throws:
    @objc static func deleteObject(identifier: Int) throws {
        var result: NSArray! = NSArray()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedObjectContext = appDelegate.managedObjectContext
        let predicate1 = NSPredicate(format: SearchAttribute.identifier + " == %d", identifier)
        let compound =  NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [predicate1])
        let entityDescription = NSEntityDescription.entity(forEntityName: SearchEntity.name, in: managedObjectContext)
        let sortDescriptor1 = NSSortDescriptor.init(key: SearchAttribute.identifier, ascending: true)
        let fetchRequest = NSFetchRequest<Search>(entityName: SearchEntity.name)
        fetchRequest.entity = entityDescription
        fetchRequest.predicate = compound
        fetchRequest.sortDescriptors = [sortDescriptor1]
        fetchRequest.includesPropertyValues = true
        do {
            let fetchedData = try managedObjectContext.fetch(fetchRequest)
            result = fetchedData as NSArray
            if let object = result.firstObject {
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                let managedObjectContext = appDelegate.managedObjectContext
                managedObjectContext.delete(object as! NSManagedObject)
            }
        } catch let error as NSError {
            LogApp.error(String(describing: SearchDAO.self) + LoggingConstants.SEPARATOR + "Fetch failed: \(error) \(error.localizedDescription)")
            let error = NSError(domain: ErrorApp.EXCEPTION_COREDATA_DELETE_WITH_IDENTIFIER_DOMAIN, code: ErrorApp.CODE_993_COREDATA_DELETE_WITH_IDENTIFIER, userInfo: error.userInfo)
            throw error
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Private methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
}




