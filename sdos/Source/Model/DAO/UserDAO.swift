//
//  UserDAO.swift
//  sdos
//
//  Created by Juan Miguel Fernández on 05/09/2019.
//  Copyright © 2019 Juan Miguel Fernández Lerena. All rights reserved.
//

import Foundation
import CoreData
import Logging

/// Constants for DAO.
struct UserAttribute {
    
    /// Attributes.
    static let identifier = "identifier"
    static let name = "name"
    static let job = "job"
    static let emailAddress = "emailAddress"
    static let phoneNumber = "phoneNumber"
    /// Relationships.
    static let searches = "searches"
    
}

/// Constants for DAO.
struct UserEntity {
    
    /// Entity.
    static let name = "User"
    
}

/// DAO for User.
class UserDAO: NSObject {
    
    /// Shared instance.
    static let sharedInstance = UserDAO()
    
    /// Init.
    private override init() {
        
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Public methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Create new object.
    /// - parameters:
    ///   - dict: Dict.
    /// - throws:
    /// - returns: Object.
    static func newObject(dict: NSDictionary) -> User? {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedObjectContext = appDelegate.managedObjectContext
        var object: User?
        object = NSEntityDescription.insertNewObject(forEntityName: UserEntity.name, into: managedObjectContext) as? User
        if let identifier = dict.value(forKey: UserAttribute.identifier) as? NSNumber {
            object?.identifier = identifier.int32Value
        }
        if let name = dict.value(forKey: UserAttribute.name) as? String {
            object?.name = name
        }
        if let job = dict.value(forKey: UserAttribute.job) as? String {
            object?.job = job
        }
        if let emailAddress = dict.value(forKey: UserAttribute.emailAddress) as? String {
            object?.emailAddress = emailAddress
        }
        if let phoneNumber = dict.value(forKey: UserAttribute.phoneNumber) as? String {
            object?.phoneNumber = phoneNumber
        }
        return object
    }
    
    /// Update new object.
    /// - parameters:
    ///   - dict: Dict.
    ///   - object: User.
    /// - throws:
    /// - returns: Object.
    static func updateObject(dict: NSDictionary,
                             object: User?) -> User? {
        if let identifier = dict.value(forKey: UserAttribute.identifier) as? NSNumber {
            object?.identifier = identifier.int32Value
        }
        if let name = dict.value(forKey: UserAttribute.name) as? String {
            object?.name = name
        }
        if let job = dict.value(forKey: UserAttribute.job) as? String {
            object?.job = job
        }
        if let emailAddress = dict.value(forKey: UserAttribute.emailAddress) as? String {
            object?.emailAddress = emailAddress
        }
        if let phoneNumber = dict.value(forKey: UserAttribute.phoneNumber) as? String {
            object?.phoneNumber = phoneNumber
        }
        return object
    }
    
    /// Save context.
    /// - throws: Error.
    @objc static func saveContext() throws {
        do {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let managedObjectContext = appDelegate.managedObjectContext
            try managedObjectContext.save()
        } catch let error as NSError  {
            LogApp.error(String(describing: UserDAO.self) + LoggingConstants.SEPARATOR + "Fetch failed: \(error) \(error.localizedDescription)")
            let error = NSError(domain: ErrorApp.EXCEPTION_COREDATA_SAVE_DOMAIN, code: ErrorApp.CODE_997_COREDATA_SAVE, userInfo: error.userInfo)
            throw error
        }
    }
    
    /// Delete all objects.
    /// - throws: Error.
    @objc static func deleteAllObjects() throws {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedObjectContext = appDelegate.managedObjectContext
        let fetchRequest = NSFetchRequest<User>(entityName: UserEntity.name)
        fetchRequest.returnsObjectsAsFaults = false
        do {
            let fetchedData = try managedObjectContext.fetch(fetchRequest)
            for managedObject in fetchedData {
                let managedObjectData:NSManagedObject = managedObject as NSManagedObject
                managedObjectContext.delete(managedObjectData)
            }
        } catch let error as NSError {
            LogApp.error(String(describing: UserDAO.self) + LoggingConstants.SEPARATOR + "Fetch failed: \(error) \(error.localizedDescription)")
            let error = NSError(domain: ErrorApp.EXCEPTION_COREDATA_DELETE_ALL_DOMAIN, code: ErrorApp.CODE_996_COREDATA_DELETE_ALL, userInfo: error.userInfo)
            throw error
        }
    }
    
    /// All objects.
    /// - throws: Fetch failed
    /// - returns: All objects.
    @objc static func findAllObjects() throws -> NSArray {
        var result: NSArray! = NSArray()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedObjectContext = appDelegate.managedObjectContext
        let entityDescription = NSEntityDescription.entity(forEntityName: UserEntity.name, in: managedObjectContext)
        let sortDescriptor1 = NSSortDescriptor.init(key: UserAttribute.identifier, ascending: true)
        let fetchRequest = NSFetchRequest<User>(entityName: UserEntity.name)
        fetchRequest.entity = entityDescription
        fetchRequest.sortDescriptors = [sortDescriptor1]
        fetchRequest.includesPropertyValues = true
        do {
            let fetchedData = try managedObjectContext.fetch(fetchRequest)
            result = fetchedData as NSArray
        } catch let error as NSError {
            LogApp.error(String(describing: UserDAO.self) + LoggingConstants.SEPARATOR + "Fetch failed: \(error) \(error.localizedDescription)")
            let error = NSError(domain: ErrorApp.EXCEPTION_COREDATA_FIND_ALL_DOMAIN, code: ErrorApp.CODE_995_COREDATA_FIND_ALL, userInfo: error.userInfo)
            throw error
        }
        return result
    }
    
    /// Find object.
    /// - parameters:
    ///   - identifier: Identifier.
    /// - throws:
    /// - returns: Array with object.
    @objc static func findObject(identifier: Int) throws -> NSArray {
        var result: NSArray = NSArray()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedObjectContext = appDelegate.managedObjectContext
        let predicate1 = NSPredicate(format: UserAttribute.identifier + " == %d", identifier)
        let compound =  NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [predicate1])
        let entityDescription = NSEntityDescription.entity(forEntityName: UserEntity.name, in: managedObjectContext)
        let sortDescriptor1 = NSSortDescriptor.init(key: UserAttribute.identifier, ascending: true)
        let fetchRequest = NSFetchRequest<User>(entityName: UserEntity.name)
        fetchRequest.entity = entityDescription
        fetchRequest.predicate = compound
        fetchRequest.sortDescriptors = [sortDescriptor1]
        fetchRequest.includesPropertyValues = true
        do {
            let fetchedData = try managedObjectContext.fetch(fetchRequest)
            result = fetchedData as NSArray
        } catch let error as NSError {
            LogApp.error(String(describing: UserDAO.self) + LoggingConstants.SEPARATOR + "Fetch failed: \(error) \(error.localizedDescription)")
            let error = NSError(domain: ErrorApp.EXCEPTION_COREDATA_FIND_WITH_IDENTIFIER_DOMAIN, code: ErrorApp.CODE_994_COREDATA_FIND_WITH_IDENTIFIER, userInfo: error.userInfo)
            throw error
        }
        return result
    }
    
    /// Delete object.
    /// - parameters:
    ///   - identifier: Identifier.
    /// - throws:
    @objc static func deleteObject(identifier: Int) throws {
        var result: NSArray! = NSArray()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedObjectContext = appDelegate.managedObjectContext
        let predicate1 = NSPredicate(format: UserAttribute.identifier + " == %d", identifier)
        let compound =  NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [predicate1])
        let entityDescription = NSEntityDescription.entity(forEntityName: UserEntity.name, in: managedObjectContext)
        let sortDescriptor1 = NSSortDescriptor.init(key: UserAttribute.identifier, ascending: true)
        let fetchRequest = NSFetchRequest<User>(entityName: UserEntity.name)
        fetchRequest.entity = entityDescription
        fetchRequest.predicate = compound
        fetchRequest.sortDescriptors = [sortDescriptor1]
        fetchRequest.includesPropertyValues = true
        do {
            let fetchedData = try managedObjectContext.fetch(fetchRequest)
            result = fetchedData as NSArray
            if let object = result.firstObject {
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                let managedObjectContext = appDelegate.managedObjectContext
                managedObjectContext.delete(object as! NSManagedObject)
            }
        } catch let error as NSError {
            LogApp.error(String(describing: UserDAO.self) + LoggingConstants.SEPARATOR + "Fetch failed: \(error) \(error.localizedDescription)")
            let error = NSError(domain: ErrorApp.EXCEPTION_COREDATA_DELETE_WITH_IDENTIFIER_DOMAIN, code: ErrorApp.CODE_993_COREDATA_DELETE_WITH_IDENTIFIER, userInfo: error.userInfo)
            throw error
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Private methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
}



