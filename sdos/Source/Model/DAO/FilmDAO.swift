//
//  FilmDAO.swift
//  sdos
//
//  Created by Juan Miguel Fernández on 08/09/2019.
//  Copyright © 2019 Juan Miguel Fernández Lerena. All rights reserved.
//

import Foundation
import CoreData
import Logging

/// Constants for DAO.
struct FilmAttribute {
    
    /// Attributes.
    static let imdbID = "imdbID"
    static let image = "image"
    static let title = "title"
    static let date = "date"
    static let duration = "duration"
    static let genders = "genders"
    static let synopsis = "synopsis"
    static let web = "web"
    static let identifier = "identifier"
    static let json = "json"
    /// Relationships.
    
}

/// Constants for DAO.
struct FilmEntity {
    
    /// Entity.
    static let name = "Film"
    
}

/// DAO for Film.
class FilmDAO: NSObject {
    
    /// Shared instance.
    static let sharedInstance = FilmDAO()
    
    /// Init.
    private override init() {
        
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Public methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Create new object.
    /// - parameters:
    ///   - dict: Dict.
    ///   - json: JSON.
    /// - throws:
    /// - returns: Object.
    static func newObject(dict: NSDictionary,
                          json: String) -> Film? {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedObjectContext = appDelegate.managedObjectContext
        var object: Film?
        object = NSEntityDescription.insertNewObject(forEntityName: FilmEntity.name, into: managedObjectContext) as? Film
        if let imdbID = dict.value(forKey: FilmAttribute.imdbID) as? String {
            object?.imdbID = imdbID
        }
        if let title = dict.value(forKey: FilmAttribute.title) as? String {
            object?.title = title
        }
        if let duration = dict.value(forKey: FilmAttribute.duration) as? String {
            object?.duration = duration
        }
        if let date = dict.value(forKey: FilmAttribute.date) as? String {
            object?.date = date
        }
        if let genders = dict.value(forKey: FilmAttribute.genders) as? String {
            object?.genders = genders
        }
        if let web = dict.value(forKey: FilmAttribute.web) as? String {
            object?.web = web
        }
        if let synopsis = dict.value(forKey: FilmAttribute.synopsis) as? String {
            object?.sypnosis = synopsis
        }
        if let image = dict.value(forKey: FilmAttribute.image) as? String {
            object?.image = image
        }
        if let identifier = dict.value(forKey: FilmAttribute.identifier) as? NSNumber {
            let id = identifier.doubleValue * 1000000
            object?.identifier = (id as NSNumber).int64Value 
        }
        object?.json = json
        return object
    }
    
    /// Update new object.
    /// - parameters:
    ///   - dict: Dict.
    ///   - object: Film.
    ///   - json: JSON.
    /// - throws:
    /// - returns: Object.
    static func updateObject(dict: NSDictionary,
                             object: Film?,
                             json: String) -> Film? {
        if let imdbID = dict.value(forKey: FilmAttribute.imdbID) as? String {
            object?.imdbID = imdbID
        }
        if let title = dict.value(forKey: FilmAttribute.title) as? String {
            object?.title = title
        }
        if let duration = dict.value(forKey: FilmAttribute.duration) as? String {
            object?.duration = duration
        }
        if let date = dict.value(forKey: FilmAttribute.date) as? String {
            object?.date = date
        }
        if let genders = dict.value(forKey: FilmAttribute.genders) as? String {
            object?.genders = genders
        }
        if let web = dict.value(forKey: FilmAttribute.web) as? String {
            object?.web = web
        }
        if let synopsis = dict.value(forKey: FilmAttribute.synopsis) as? String {
            object?.sypnosis = synopsis
        }
        if let image = dict.value(forKey: FilmAttribute.image) as? String {
            object?.image = image
        }
        object?.json = json
        return object
    }
    
    /// Save context.
    /// - throws: Error.
    @objc static func saveContext() throws {
        do {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let managedObjectContext = appDelegate.managedObjectContext
            try managedObjectContext.save()
        } catch let error as NSError  {
            LogApp.error(String(describing: FilmDAO.self) + LoggingConstants.SEPARATOR + "Fetch failed: \(error) \(error.localizedDescription)")
            let error = NSError(domain: ErrorApp.EXCEPTION_COREDATA_SAVE_DOMAIN, code: ErrorApp.CODE_997_COREDATA_SAVE, userInfo: error.userInfo)
            throw error
        }
    }
    
    /// Delete all objects.
    /// - throws: Error.
    @objc static func deleteAllObjects() throws {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedObjectContext = appDelegate.managedObjectContext
        let fetchRequest = NSFetchRequest<Film>(entityName: FilmEntity.name)
        fetchRequest.returnsObjectsAsFaults = false
        do {
            let fetchedData = try managedObjectContext.fetch(fetchRequest)
            for managedObject in fetchedData {
                let managedObjectData:NSManagedObject = managedObject as NSManagedObject
                managedObjectContext.delete(managedObjectData)
            }
        } catch let error as NSError {
            LogApp.error(String(describing: FilmDAO.self) + LoggingConstants.SEPARATOR + "Fetch failed: \(error) \(error.localizedDescription)")
            let error = NSError(domain: ErrorApp.EXCEPTION_COREDATA_DELETE_ALL_DOMAIN, code: ErrorApp.CODE_996_COREDATA_DELETE_ALL, userInfo: error.userInfo)
            throw error
        }
    }
    
    /// All objects.
    /// - throws: Fetch failed
    /// - returns: All objects.
    @objc static func findAllObjects() throws -> NSArray {
        var result: NSArray! = NSArray()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedObjectContext = appDelegate.managedObjectContext
        let entityDescription = NSEntityDescription.entity(forEntityName: FilmEntity.name, in: managedObjectContext)
        let sortDescriptor1 = NSSortDescriptor.init(key: FilmAttribute.title, ascending: true)
        let fetchRequest = NSFetchRequest<Film>(entityName: FilmEntity.name)
        fetchRequest.entity = entityDescription
        fetchRequest.sortDescriptors = [sortDescriptor1]
        fetchRequest.includesPropertyValues = true
        do {
            let fetchedData = try managedObjectContext.fetch(fetchRequest)
            result = fetchedData as NSArray
        } catch let error as NSError {
            LogApp.error(String(describing: FilmDAO.self) + LoggingConstants.SEPARATOR + "Fetch failed: \(error) \(error.localizedDescription)")
            let error = NSError(domain: ErrorApp.EXCEPTION_COREDATA_FIND_ALL_DOMAIN, code: ErrorApp.CODE_995_COREDATA_FIND_ALL, userInfo: error.userInfo)
            throw error
        }
        return result
    }
    
    /// Find object.
    /// - parameters:
    ///   - text: Text.
    /// - throws:
    /// - returns: Array with object.
    @objc static func findObject(title: String) throws -> NSArray {
        var result: NSArray = NSArray()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedObjectContext = appDelegate.managedObjectContext
        let predicate1 = NSPredicate(format: "title = %@", title)
        let compound =  NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [predicate1])
        let entityDescription = NSEntityDescription.entity(forEntityName: FilmEntity.name, in: managedObjectContext)
        let sortDescriptor1 = NSSortDescriptor.init(key: FilmAttribute.title, ascending: true)
        let fetchRequest = NSFetchRequest<Film>(entityName: FilmEntity.name)
        fetchRequest.entity = entityDescription
        fetchRequest.predicate = compound
        fetchRequest.sortDescriptors = [sortDescriptor1]
        fetchRequest.includesPropertyValues = true
        do {
            let fetchedData = try managedObjectContext.fetch(fetchRequest)
            result = fetchedData as NSArray
        } catch let error as NSError {
            LogApp.error(String(describing: FilmDAO.self) + LoggingConstants.SEPARATOR + "Fetch failed: \(error) \(error.localizedDescription)")
            let error = NSError(domain: ErrorApp.EXCEPTION_COREDATA_FIND_WITH_IDENTIFIER_DOMAIN, code: ErrorApp.CODE_994_COREDATA_FIND_WITH_IDENTIFIER, userInfo: error.userInfo)
            throw error
        }
        return result
    }
    
    /// Find object.
    /// - parameters:
    ///   - imdbID: Text.
    /// - throws:
    /// - returns: Array with object.
    @objc static func findObject(imdbID: String) throws -> NSArray {
        var result: NSArray = NSArray()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedObjectContext = appDelegate.managedObjectContext
        let predicate1 = NSPredicate(format: "imdbID = %@", imdbID)
        let compound =  NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [predicate1])
        let entityDescription = NSEntityDescription.entity(forEntityName: FilmEntity.name, in: managedObjectContext)
        let sortDescriptor1 = NSSortDescriptor.init(key: FilmAttribute.imdbID, ascending: true)
        let fetchRequest = NSFetchRequest<Film>(entityName: FilmEntity.name)
        fetchRequest.entity = entityDescription
        fetchRequest.predicate = compound
        fetchRequest.sortDescriptors = [sortDescriptor1]
        fetchRequest.includesPropertyValues = true
        do {
            let fetchedData = try managedObjectContext.fetch(fetchRequest)
            result = fetchedData as NSArray
        } catch let error as NSError {
            LogApp.error(String(describing: FilmDAO.self) + LoggingConstants.SEPARATOR + "Fetch failed: \(error) \(error.localizedDescription)")
            let error = NSError(domain: ErrorApp.EXCEPTION_COREDATA_FIND_WITH_IDENTIFIER_DOMAIN, code: ErrorApp.CODE_994_COREDATA_FIND_WITH_IDENTIFIER, userInfo: error.userInfo)
            throw error
        }
        return result
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Private methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
}





