//
//  GalleryImageViewController.swift
//  sdos
//
//  Created by Juan Miguel Fernández on 07/09/2019.
//  Copyright © 2019 Juan Miguel Fernández Lerena. All rights reserved.
//

import UIKit
import Logging
import Alamofire
import AlamofireImage

/// View controller of images.
class GalleryImageViewController: BaseViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    @IBOutlet weak var imageConstraintTop: NSLayoutConstraint!
    @IBOutlet weak var imageConstraintRight: NSLayoutConstraint!
    @IBOutlet weak var imageConstraintLeft: NSLayoutConstraint!
    @IBOutlet weak var imageConstraintBottom: NSLayoutConstraint!
    
    var urlImage: String?
    var indexImage: Int?
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Navigation
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    override func prepare(for segue: UIStoryboardSegue,
                          sender: Any?) {
        
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Public methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Load URL.
    /// - parameters:
    ///   - url: url.
    /// - throws:
    func loadDataWithURL(url: String?) {
        LogApp.info(String(describing: GalleryImageViewController.self) + LoggingConstants.SEPARATOR + "Load data with url...")
        self.urlImage = url
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Lifecycle
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Managing the View
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    override public func viewDidLoad() {
        LogApp.debug(String(describing: GalleryImageViewController.self) + LoggingConstants.SEPARATOR + "Did load...")
        super.viewDidLoad()
        self.configureView()
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Responding to View Events
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override public func viewDidAppear(_ animated: Bool) {
        LogApp.debug(String(describing: GalleryImageViewController.self) + LoggingConstants.SEPARATOR + "View did appear...")
        super.viewDidAppear(animated)
    }
    
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override public func viewDidDisappear(_ animated: Bool) {
        LogApp.debug(String(describing: GalleryImageViewController.self) + LoggingConstants.SEPARATOR + "View did disapper...")
        super.viewDidDisappear(animated)
    }
    
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    override func viewWillTransition(to size: CGSize,
                                     with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to:size, with: coordinator)
        
        coordinator.animate(alongsideTransition: { (UIViewControllerTransitionCoordinatorContext) -> Void in
            
            let orient = UIApplication.shared.statusBarOrientation
            
            switch orient {
            case .portrait:
                
                break
            case .portraitUpsideDown:
                
                break
            case .landscapeLeft:
                
                break
            case .landscapeRight:
                
                break
            default:
                
                break
            }
            
        }, completion: { (UIViewControllerTransitionCoordinatorContext) -> Void in
            
        })
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.updateMinZoomScaleForSize(size: self.imageView.bounds.size)
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Delegate methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: BaseAnyControllerDelegate
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Update preferred font adjustment.
    /// - throws:
    override func updatePreferredFontAdjustment() {
        LogApp.debug(String(describing: GalleryImageViewController.self) + LoggingConstants.SEPARATOR + "Update preferred font adjustment...")
    }
    
    /// Perform reload remote.
    /// - throws:
    override func performReloadRemote() {
        LogApp.debug(String(describing: GalleryImageViewController.self) + LoggingConstants.SEPARATOR + "Perform reolad remote...")
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: UIScrollViewDelegate
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imageView
    }
    
    func scrollViewDidEndZooming(_ scrollView: UIScrollView,
                                 with view: UIView?,
                                 atScale scale: CGFloat) {
        self.updateConstraintsForSize(size: self.view.bounds.size)
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Private methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Update Min Zoom Scale For Size.
    /// - parameters:
    ///   - size: size.
    /// - throws:
    private func updateMinZoomScaleForSize(size: CGSize) {
        LogApp.debug(String(describing: GalleryImageViewController.self) + LoggingConstants.SEPARATOR + "Update min zoom scale for size...")
        let widthScale = size.width / imageView.bounds.width
        let heightScale = size.height / imageView.bounds.height
        let minScale = min(widthScale, heightScale)
        scrollView.minimumZoomScale = minScale
        scrollView.zoomScale = minScale
    }
    
    /// Update constraints.
    /// - parameters:
    ///   - size: size.
    /// - throws:
    private func updateConstraintsForSize(size: CGSize) {
        LogApp.debug(String(describing: GalleryImageViewController.self) + LoggingConstants.SEPARATOR + "Update constraints for size...")
        let yOffset = max(0, (size.height - self.imageView.frame.height) / 2)
        self.imageConstraintTop.constant = yOffset
        self.imageConstraintBottom.constant = yOffset
        let xOffset = max(0, (size.width - self.imageView.frame.width) / 2)
        self.imageConstraintLeft.constant = xOffset
        self.imageConstraintRight.constant = xOffset
        view.layoutIfNeeded()
    }
    
    /// Configure view.
    /// - throws:
    private func configureView() {
        LogApp.debug(String(describing: GalleryImageViewController.self) + LoggingConstants.SEPARATOR + "Configure view...")
        self.activityIndicatorView.startAnimating()
        self.imageView.image = nil
        self.imageView.contentMode = UIView.ContentMode.center
        let urlPicture = self.urlImage
        if let urlImage = urlPicture, (urlPicture?.count)! > 0, (urlPicture != "N/A") {
            let url = URL(string: urlImage)!
            self.imageView.af_setImage(
                withURL: url,
                placeholderImage: nil,
                filter: nil,
                imageTransition: .crossDissolve(0.2)
            )
        } else {
            self.imageView.image = UIImage(named: "Placeholder")
        }
        self.activityIndicatorView.stopAnimating()
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Actions
    ////////////////////////////////////////////////////////////////////////////////////////////
        
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Observer of notification
    ////////////////////////////////////////////////////////////////////////////////////////////
    
}
