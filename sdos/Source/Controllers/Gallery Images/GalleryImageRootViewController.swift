//
//  GalleryImageRootViewController.swift
//  sdos
//
//  Created by Juan Miguel Fernández on 07/09/2019.
//  Copyright © 2019 Juan Miguel Fernández Lerena. All rights reserved.
//


import UIKit
import Logging

/// Root controller of image.
class GalleryImageRootViewController: UIViewController, BasePageViewControllerDataSource, BasePageViewControllerDelegate {
    
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var pageControlLabel: UILabel!
    @IBOutlet weak var closeBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var closeTitleButton: UIButton!
    
    var urlPictures = [String]()
    var selectedPicture = 0
    var pageViewController: BasePageViewController?
    var isPresented = true
    weak var photoFinalToShare: UIImage!
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Navigation
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    override func prepare(for segue: UIStoryboardSegue,
                          sender: Any?) {
        
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Public methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Lifecycle
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Managing the View
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    override func viewDidLoad() {
        LogApp.debug(String(describing: GalleryImageRootViewController.self) + LoggingConstants.SEPARATOR + "Did load...")
        super.viewDidLoad()
        // Instantiate BasePageViewController and set the data source and delegate to 'self'
        let pageViewController = BasePageViewController()
        pageViewController.dataSource = self
        pageViewController.delegate = self
        pageControl.numberOfPages = self.urlPictures.count
        pageControl.isHidden = true
        self.pageControlLabel.text = "\(self.selectedPicture + 1)" + "/" + "\(self.urlPictures.count)"
        // Set the initially selected view controller
        let currentViewController = self.viewControllerAtIndex(index: self.selectedPicture)!
        pageViewController.selectViewController(viewController: currentViewController, direction: .Forward, animated: false, completion: nil)
        // Add BasePageViewController to the root view controller
        self.addChild(pageViewController)
        self.view.insertSubview(pageViewController.view, at: self.selectedPicture) // Insert the page controller view below the navigation buttons
        pageViewController.didMove(toParent: self)
        self.pageViewController = pageViewController
        self.closeTitleButton.setTitle(NSLocalizedString("Close", comment: ""), for: UIControl.State.normal)
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Responding to View Events
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override public func viewDidAppear(_ animated: Bool) {
        LogApp.debug(String(describing: GalleryImageRootViewController.self) + LoggingConstants.SEPARATOR + "View did appear...")
        super.viewDidAppear(animated)
    }
    
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override public func viewDidDisappear(_ animated: Bool) {
        LogApp.debug(String(describing: GalleryImageRootViewController.self) + LoggingConstants.SEPARATOR + "View did disapper...")
        super.viewDidDisappear(animated)
    }
    
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    override func viewWillTransition(to size: CGSize,
                                     with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to:size, with: coordinator)
        
        coordinator.animate(alongsideTransition: { (UIViewControllerTransitionCoordinatorContext) -> Void in
            
            let orient = UIApplication.shared.statusBarOrientation
            
            switch orient {
            case .portrait:
                
                break
            case .portraitUpsideDown:
                
                break
            case .landscapeLeft:
                
                break
            case .landscapeRight:
                
                break
            default:
                
                break
            }
            
        }, completion: { (UIViewControllerTransitionCoordinatorContext) -> Void in
            
        })
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: BasePageViewControllerDataSource
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    func ss_pageViewController(pageViewController: BasePageViewController,
                               viewControllerLeftOfViewController viewController: UIViewController) -> UIViewController? {
        if let viewControllerIndex = self.indexOfViewController(viewController: viewController as! GalleryImageViewController) {
            let leftViewController = self.viewControllerAtIndex(index: viewControllerIndex - 1)
            self.pageControl.currentPage = viewControllerIndex
            self.pageControlLabel.alpha = 0
            UIView.animate(withDuration: 0.8, animations: {
                self.pageControlLabel.text = "\(viewControllerIndex + 1)" + "/" + "\(self.urlPictures.count)"
                self.pageControlLabel.alpha = 1
            })
            return leftViewController
        } else {
            return nil
        }
    }
    
    func ss_pageViewController(pageViewController: BasePageViewController,
                               viewControllerRightOfViewController viewController: UIViewController) -> UIViewController? {
        if let viewControllerIndex = self.indexOfViewController(viewController: viewController as! GalleryImageViewController) {
            let rightViewController = self.viewControllerAtIndex(index: viewControllerIndex + 1)
            self.pageControl.currentPage = viewControllerIndex
            self.pageControlLabel.alpha = 0
            UIView.animate(withDuration: 0.8, animations: {
                self.pageControlLabel.text = "\(viewControllerIndex + 1)" + "/" + "\(self.urlPictures.count)"
                self.pageControlLabel.alpha = 1
            })
            return rightViewController
        } else {
            return nil
        }
    }
    
    func viewControllerAtIndex(index: Int) -> GalleryImageViewController? {
        if (self.urlPictures.count == 0) || (index < 0) || (index >= self.urlPictures.count) {
            return nil
        }
        let viewController = self.storyboard!.instantiateViewController(withIdentifier: "GalleryImageViewController") as! GalleryImageViewController
        viewController.urlImage = self.urlPictures[index]
        viewController.indexImage = index
        return viewController
    }
    
    func indexOfViewController(viewController: GalleryImageViewController) -> Int? {
        if let indexImage: Int = viewController.indexImage {
            return indexImage
        } else {
            return nil
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: BasePageViewControllerDelegate
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    func ss_pageViewController(pageViewController: BasePageViewController,
                               willStartScrollingFrom startViewController: UIViewController,
                               destinationViewController: UIViewController) {
        let startImageImageViewController = startViewController as! GalleryImageViewController
        let destinationImageImageViewController = destinationViewController as! GalleryImageViewController
        LogApp.info("Will start scrolling from \(String(describing: startImageImageViewController.urlImage)) to \(String(describing: destinationImageImageViewController.urlImage)).")
    }
    
    func ss_pageViewController(pageViewController: BasePageViewController,
                               isScrollingFrom startViewController: UIViewController,
                               destinationViewController: UIViewController,
                               progress: CGFloat) {
        let startImageImageViewController = startViewController as! GalleryImageViewController
        let destinationImageImageViewController = destinationViewController as! GalleryImageViewController
        // Ease the labels' alphas in and out
        let absoluteProgress = abs(progress)
        startImageImageViewController.imageView.alpha = pow(1 - absoluteProgress, 2)
        destinationImageImageViewController.imageView.alpha = pow(absoluteProgress, 2)
        LogApp.info("Is scrolling from \(String(describing: startImageImageViewController.urlImage)) to \(String(describing: destinationImageImageViewController.urlImage)) with progress '\(progress)'.")
    }
    
    func ss_pageViewController(pageViewController: BasePageViewController,
                               didFinishScrollingFrom startViewController: UIViewController?,
                               destinationViewController: UIViewController,
                               transitionSuccessful: Bool) {
        let startViewController = startViewController as! GalleryImageViewController?
        let destinationViewController = destinationViewController as! GalleryImageViewController
        // If the transition is successful, the new selected view controller is the destination view controller.
        // If it wasn't successful, the selected view controller is the start view controller
        if transitionSuccessful {
            if (self.indexOfViewController(viewController: destinationViewController) == 0) {
            } else {
            }
            
            if (self.indexOfViewController(viewController: destinationViewController) == self.urlPictures.count - 1) {
            } else {
            }
        }
        LogApp.info("Finished scrolling from \(String(describing: startViewController?.urlImage)) to \(String(describing: destinationViewController.urlImage)). Transition successful? \(transitionSuccessful)")
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Private methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Update constraints.
    /// - parameters:
    ///   - image: Image.
    ///   - error: Error.
    ///   - contextInfo: Context.
    /// - throws:
    @objc func image(_ image: UIImage,
                     didFinishSavingWithError error: NSError?,
                     contextInfo: UnsafeRawPointer) {
        LogApp.debug(String(describing: GalleryImageViewController.self) + LoggingConstants.SEPARATOR + "Configure view...")
        if error == nil {
            let alertController: BaseAlertViewController = BaseAlertViewController(title: NSLocalizedString("Info", comment:""),
                                                                                   message: NSLocalizedString("The photo has been saved successfully.", comment:""),
                                                                                   preferredStyle: UIAlertController.Style.alert)
            
            let nowAction = UIAlertAction(title: NSLocalizedString("OK", comment:""),
                                          style: .default) { (_) in
                                            
            }
            nowAction.isEnabled = true
            alertController.addAction(nowAction)
            self.present(alertController, animated: true, completion: nil)
        } else {
            let alertController: BaseAlertViewController = BaseAlertViewController(title: NSLocalizedString("Error", comment:""),
                                                                                   message: NSLocalizedString("Upps, there was an error saving the photo.", comment:""),
                                                                                   preferredStyle: UIAlertController.Style.alert)
            
            let nowAction = UIAlertAction(title: NSLocalizedString("OK", comment:""),
                                          style: .default) { (_) in
                                            
            }
            nowAction.isEnabled = true
            alertController.addAction(nowAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Actions
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Close.
    /// - parameters:
    ///   - sender: Sender.
    /// - throws:
    @IBAction func close(sender: AnyObject) {
        LogApp.info(String(describing: GalleryImageRootViewController.self) + LoggingConstants.SEPARATOR + "Pressed close button...")
        isPresented = false
        self.navigationController?.dismiss(animated: true, completion: { () in
            
        })
    }
    
    /// Pressed download.
    /// - parameters:
    ///   - sender: Sender.
    /// - throws:
    @IBAction func pressedDownload(_ sender: Any) {
        LogApp.info(String(describing: GalleryImageRootViewController.self) + LoggingConstants.SEPARATOR + "Pressed download button...")
        UIImageWriteToSavedPhotosAlbum(self.photoFinalToShare, self, #selector(GalleryImageRootViewController.image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Observer of notification
    ////////////////////////////////////////////////////////////////////////////////////////////
    
}

////////////////////////////////////////////////////////////////////////////////////////////
// MARK: UIDocumentInteractionControllerDelegate
////////////////////////////////////////////////////////////////////////////////////////////

extension GalleryImageRootViewController: UIDocumentInteractionControllerDelegate {
    
    func documentsDirectory() -> String {
        let documentsFolderPath = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0]
        return documentsFolderPath
    }
    
}
