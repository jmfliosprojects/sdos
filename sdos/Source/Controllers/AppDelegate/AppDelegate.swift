//
//  AppDelegate.swift
//  sdos
//
//  Created by Juan Miguel Fernández Lerenaon 04/09/2019.
//  Copyright © 2019 JUAN MIGUEL. All rights reserved.
//

import UIKit
import CoreData
import Logging
import Connection

@UIApplicationMain
/// The UIApplication class provides a centralized point of control and coordination for apps running in iOS. Every app has exactly one instance of UIApplication (or, very rarely, a subclass of UIApplication). When an app is launched, the system calls the UIApplicationMain function; among its other tasks, this function creates a singleton UIApplication object. Thereafter you access the object by calling the sharedApplication class method.
class AppDelegate: UIResponder, UIApplicationDelegate {

    // The window to use when presenting a storyboard.
    var window: UIWindow?

    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: UIApplicationDelegate
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Responding to App State Changes and System Events
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// application(_application:willFinishLaunchingWithOptions:)
    /// Tells the delegate that the launch process has begun but that state restoration has not yet occurred.
    /// - parameters:
    ///   - application: Your singleton app object.
    ///   - launchOptions: A dictionary indicating the reason the app was launched (if any). The contents of this dictionary may be empty in situations where the user launched the app directly. For information about the possible keys in this dictionary and how to handle them, see Launch Options Keys.
    /// - throws:
    /// - returns: false if the app cannot handle the URL resource or continue a user activity, or if the app should not perform the application:performActionForShortcutItem:completionHandler: method because you’re handling the invocation of a Home screen quick action in this method; otherwise return true. The return value is ignored if the app is launched as a result of a remote notification.
    func application(_ application: UIApplication,
                     willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        do {
            try self.prepareConfiguration()
        } catch _ as NSError {
            LogApp.error(String(describing: AppDelegate.self) + LoggingConstants.SEPARATOR + "Error launching with options...")
            abort()
        }
        LogApp.info(String(describing: AppDelegate.self) + LoggingConstants.SEPARATOR + "The app will finish launching with options...")
        return true
    }
    
    /// application(_:didFinishLaunchingWithOptions:)
    /// - parameters:
    ///   - application: Your singleton app object.
    ///   - launchOptions: UIApplicationLaunchOptionsKey.
    /// - throws:
    /// - returns:
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        LogApp.info(String(describing: AppDelegate.self) + LoggingConstants.SEPARATOR + "The app did finish launching with options...")
        self.startConfiguration(application, launchOptions)
        return true
    }
    
    /// applicationDidFinishLaunching(_:)
    ///
    /// - parameters:
    ///   - application: Your singleton app object.
    /// - throws:
    func applicationDidFinishLaunching(_ application: UIApplication) {
        LogApp.info(String(describing: AppDelegate.self) + LoggingConstants.SEPARATOR + "The app did finish launching...")
    }
    
    /// applicationDidBecomeActive(_:)
    /// Tells the delegate that the app has become active.
    /// - parameters:
    ///   - application: Your singleton app object.
    /// - throws:
    func applicationDidBecomeActive(_ application: UIApplication) {
        LogApp.info(String(describing: AppDelegate.self) + LoggingConstants.SEPARATOR + "The app has become active...")
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        self.updateConfiguration(application)
    }
    
    /// applicationWillResignActive(_:)
    /// Tells the delegate that the app is about to become inactive.
    /// - parameters:
    /// - throws:
    func applicationWillResignActive(_ application: UIApplication) {
        LogApp.info(String(describing: AppDelegate.self) + LoggingConstants.SEPARATOR + "The app is about terminate...")
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    /// applicationDidEnterBackground(_:)
    /// Tells the delegate that the app is now in the background.
    /// - parameters:
    ///   - application: Your singleton app object.
    /// - throws:
    func applicationDidEnterBackground(_ application: UIApplication) {
        LogApp.info(String(describing: AppDelegate.self) + LoggingConstants.SEPARATOR + "The app is about terminate...")
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    /// applicationWillEnterForeground(_:)
    /// Tells the delegate that the app is about to enter the foreground.
    /// - parameters:
    ///   - application: Your singleton app object.
    /// - throws:
    func applicationWillEnterForeground(_ application: UIApplication) {
        LogApp.info(String(describing: AppDelegate.self) + LoggingConstants.SEPARATOR + "The app is about enter the foreground...")
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    /// applicationWillTerminate(_:)
    /// Tells the delegate when the app is about to terminate.
    /// - parameters:
    ///   - application: Your singleton app object.
    /// - throws:
    func applicationWillTerminate(_ application: UIApplication) {
        LogApp.info(String(describing: AppDelegate.self) + LoggingConstants.SEPARATOR + "The app is about terminate...")
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    /// applicationProtectedDataWillBecomeUnavailable(_:)
    ///
    /// - parameters:
    /// - throws:
    /// - returns:
    
    /// applicationProtectedDataDidBecomeAvailable(_:)
    ///
    /// - parameters:
    /// - throws:
    /// - returns:
    
    /// applicationDidReceiveMemoryWarning(_:)
    ///
    /// - parameters:
    /// - throws:
    /// - returns:
    
    /// applicationSignificantTimeChange(_:)
    ///
    /// - parameters:
    /// - throws:
    /// - returns:
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Managing App State Restoration
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// application(_:shouldSaveApplicationState:)
    ///
    /// - parameters:
    /// - throws:
    /// - returns:
    
    /// application(_:shouldRestoreApplicationState:)
    ///
    /// - parameters:
    /// - throws:
    /// - returns:
    
    /// application(_:viewControllerWithRestorationIdentifierPath:coder:)
    ///
    /// - parameters:
    /// - throws:
    /// - returns:
    
    /// application(_:willEncodeRestorableStateWithCoder:)
    ///
    /// - parameters:
    /// - throws:
    /// - returns:
    
    /// application(_:didDecodeRestorableStateWithCoder:)
    ///
    /// - parameters:
    /// - throws:
    /// - returns:
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Downloading Data in the Background
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Handling Local and Remote Notifications
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// application(_:didRegister:)
    ///
    /// - parameters:
    /// - throws:
    /// - returns:
    
    /// application(_:didRegisterForRemoteNotificationsWithDeviceToken:)
    ///
    /// - parameters:
    /// - throws:
    /// - returns:
    
    /// application(_:didFailToRegisterForRemoteNotificationsWithError:)
    ///
    /// - parameters:
    /// - throws:
    /// - returns:
    
    /// application(_:didReceiveRemoteNotification:fetchCompletionHandler:)
    ///
    /// - parameters:
    /// - throws:
    /// - returns:
    
    /// application(_:didReceive:)
    ///
    /// - parameters:
    /// - throws:
    /// - returns:
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Continuing User Activity and Handling Quick Actions
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// application(_:willContinueUserActivityWithType:)
    ///
    /// - parameters:
    /// - throws:
    /// - returns:
    
    /// application(_:continueUserActivity:restorationHandler:)
    ///
    /// - parameters:
    /// - throws:
    /// - returns:
    private func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        // Respond to Universal Links
        // pass the url to the handle deep link call
        return true
    }
    
    /// application(_:didUpdateUserActivity:)
    ///
    /// - parameters:
    /// - throws:
    /// - returns:
    
    /// application(_:didFailToContinueUserActivityWithType:error:)
    ///
    /// - parameters:
    /// - throws:
    /// - returns:
    
    /// application(_:performActionForShortcutItem:completionHandler:)
    ///
    /// - parameters:
    /// - throws:
    /// - returns:
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Interacting With WatchKit
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// application(_:handleWatchKitExtensionRequest:reply:)
    ///
    /// - parameters:
    /// - throws:
    /// - returns:
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Interacting With HealthKit
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// applicationShouldRequestHealthAuthorization(_:)
    ///
    /// - parameters:
    /// - throws:
    /// - returns:
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Opening a URL-Specified Resource
    ////////////////////////////////////////////////////////////////////////////////////////////

    /// application(_:openURL:options:)
    ///
    /// - parameters:
    /// - throws:
    /// - returns:
    
    /// application(_:open:sourceApplication:annotation:)
    ///
    /// - parameters:
    /// - throws:
    /// - returns:
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Disallowing Specified App Extension Types
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// application(_:shouldAllowExtensionPointIdentifier:)
    ///
    /// - parameters:
    /// - throws:
    /// - returns:
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Managing Interface Geometry
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// application(_:supportedInterfaceOrientationsForWindow:)
    ///
    /// - parameters:
    /// - throws:
    /// - returns:
    func application(_ application: UIApplication,
                     supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return self.supportedInterfaceOrientationsApp()
    }
    
    /// application(_:willChangeStatusBarOrientation:duration:)
    ///
    /// - parameters:
    /// - throws:
    /// - returns:
    
    /// application(_:didChangeStatusBarOrientation:)
    ///
    /// - parameters:
    /// - throws:
    /// - returns:
    
    /// application(_:willChangeStatusBarFrame:)
    ///
    /// - parameters:
    /// - throws:
    /// - returns:
    
    /// application(_:didChangeStatusBarFrame:)
    ///
    /// - parameters:
    /// - throws:
    /// - returns:
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Core Data stack
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// applicationDocumentsDirectory
    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory  in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1] as NSURL
    }()
    
    /// managedObjectModel
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = Bundle.main.url(forResource: "sdos", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    /// persistentStoreCoordinator
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        var coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("sdos.sqlite")
        
        // Load the existing database
        if !FileManager.default.fileExists(atPath: url?.path ?? "") {
            let sourceSqliteURLs = [Bundle.main.url(forResource: "sdos", withExtension: "sqlite")!, Bundle.main.url(forResource: "sdos", withExtension: "sqlite-wal")!, Bundle.main.url(forResource: "sdos", withExtension: "sqlite-shm")!]
            let destSqliteURLs = [self.applicationDocumentsDirectory.appendingPathComponent("sdos.sqlite"), self.applicationDocumentsDirectory.appendingPathComponent("sdos.sqlite-wal"), self.applicationDocumentsDirectory.appendingPathComponent("sdos.sqlite-shm")]
            
            for index in 0 ..< sourceSqliteURLs.count {
                do {
                    try FileManager.default.copyItem(at: sourceSqliteURLs[index], to: destSqliteURLs[index] ?? Bundle.main.url(forResource: "sdos", withExtension: "momd")!)
                } catch {
                    print(error)
                }
            }
        } else {
            
        }
        
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            let mOptions = [NSMigratePersistentStoresAutomaticallyOption: true,
                            NSInferMappingModelAutomaticallyOption: true]
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: mOptions)
        } catch {
            do {
                try FileManager.default.removeItem(at: url!)
                
                coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
                
                do {
                    try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
                } catch {
                    // Report any error we got.
                    var dict = [String: AnyObject]()
                    dict[NSLocalizedDescriptionKey] = NSLocalizedString("Failed to initialize saved application data", comment: "") as AnyObject?
                    dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?
                    
                    dict[NSUnderlyingErrorKey] = error as NSError
                    let wrappedError = NSError(domain: ErrorApp.EXCEPTION_COREDATA_DOMAIN, code: ErrorApp.CODE_998_COREDATA, userInfo: dict)
                    // Replace this with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    LogApp.error(String(describing: AppDelegate.self) + LoggingConstants.SEPARATOR + "Error initializing saved application data...")
                    abort()
                }
            } catch {
                // Report any error we got.
                var dict = [String: AnyObject]()
                dict[NSLocalizedDescriptionKey] = NSLocalizedString("Failed to initialize saved application data", comment: "") as AnyObject?
                dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?
                
                dict[NSUnderlyingErrorKey] = error as NSError
                let wrappedError = NSError(domain: ErrorApp.EXCEPTION_COREDATA_DOMAIN, code: ErrorApp.CODE_998_COREDATA, userInfo: dict)
                // Replace this with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                LogApp.error(String(describing: AppDelegate.self) + LoggingConstants.SEPARATOR + "Error initializing saved application data...")
                abort()
            }
        }
        
        return coordinator
    }()
    
    /// managedObjectContext
    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Core Data Saving support
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// saveContext(_:)
    /// Save Context.
    /// - throws:
    func saveContext() {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                LogApp.error(String(describing: AppDelegate.self) + LoggingConstants.SEPARATOR + "Error initializing saved application data...")
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Private Methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Pre-configure app.
    /// - throws: ConfigurationError.ErrorSerialization
    private func prepareConfiguration() throws {
        // Setting Configuration
        try ConfigurationManager.settingConfigurationWithBundleName()
        // Setting Logging
        LogApp.settingLogging(appID: ConfigurationManager.swiftyBeaverAppID(), appSecret: ConfigurationManager.swiftyBeaverAppSecret(), encryptionKey: ConfigurationManager.swiftyBeaverEncryptionKey())
        // Setting Custom
        try CustomConfigurationManager.settingConfigurationWithBundleName()
        // Provider
        try ProviderConfigurationManager.settingConfigurationWithBundleName()
        // Validations
        try ValidationConfigurationManager.settingConfigurationWithBundleName()
        // User defaults from Settings
        try UserManager.registerUserDefaultsFromSettingsBundle()
        // Setting Crash
        CrashManager.settingCrash()
        // Init User
        UserManager.initUser()
        LogApp.debug(String(describing: AppDelegate.self) + LoggingConstants.SEPARATOR + "Prepared configuration...")
    }
    
    /// Configure app.
    /// - parameters:
    ///   - application: Your singleton app object.
    ///   - launchOptions: Launch Options.
    /// - throws:
    private func startConfiguration(_ application: UIApplication,
                                    _ launchOptions: [UIApplication.LaunchOptionsKey: Any]?) {
        LogApp.debug(String(describing: AppDelegate.self) + LoggingConstants.SEPARATOR + "Start configuration...")
        // Start Reachability
        ReachabilityManager.sharedInstance.startRechabilityMonitor()
        // Update settings of user
        UserManager.initUserDefaultsForSettingsBundle()
        // Start User
        UserManager.startUser()
        // Clean results
        ResultManager.performCleanResults()
    }
    
    /// Update Configure app.
    /// - parameters:
    ///   - application: Your singleton app object.
    /// - throws:
    private func updateConfiguration(_ application: UIApplication) {
        LogApp.debug(String(describing: AppDelegate.self) + LoggingConstants.SEPARATOR + "Update configuration...")
        // Update settings of user
        UserManager.updateUserDefaultsForSettingsBundle()
    }
    
    /// Supported interfaceOrientationsApp.
    /// - throws:
    private func supportedInterfaceOrientationsApp() -> UIInterfaceOrientationMask {
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
            return UIInterfaceOrientationMask.portrait
        case .pad:
            return UIInterfaceOrientationMask.all
        case .unspecified:
            return UIInterfaceOrientationMask.portrait
        default:
            return UIInterfaceOrientationMask.portrait
        }
    }

}

