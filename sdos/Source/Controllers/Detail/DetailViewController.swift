//
//  DetailViewController.swift
//  sdos
//
//  Created by Juan Miguel Fernández on 06/09/2019.
//  Copyright © 2019 Juan Miguel Fernández Lerena. All rights reserved.
//

import UIKit
import Logging
import Alamofire

/// Controller of more view.
class DetailViewController: BaseTableViewController {
    
    @IBOutlet weak var galleryImageButton: UIButton!
    @IBOutlet var sharedBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!{
        didSet {
            self.titleLabel.text = "-"
        }
    }
    @IBOutlet weak var dateLabel: UILabel!{
        didSet {
            self.dateLabel.text = "-"
        }
    }
    @IBOutlet weak var durationLabel: UILabel! {
        didSet {
            self.durationLabel.text = "-"
        }
    }
    @IBOutlet weak var gendersLabel: UILabel! {
        didSet {
            self.gendersLabel.text = "-"
        }
    }
    @IBOutlet weak var synopsisTextView: UITextView! {
        didSet {
            self.synopsisTextView.text = "-"
        }
    }
    @IBOutlet weak var webTextView: UITextView! {
        didSet {
            self.webTextView.text = "-"
        }
    }
    
    private var movie: Film?
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Navigation
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    override func prepare(for segue: UIStoryboardSegue,
                          sender: Any?) {
        if segue.identifier == "Images" {
            let galleryImageRootNavigationViewController = segue.destination as? UINavigationController
            let galleryImageRootViewController: GalleryImageRootViewController? = galleryImageRootNavigationViewController?.visibleViewController as? GalleryImageRootViewController
            var identifierSelectedImage = 0
            if (sender != nil) {
                identifierSelectedImage = sender as! Int
            }
            galleryImageRootViewController?.selectedPicture = identifierSelectedImage
            let urlsImageArray = NSMutableArray()
            if let url = movie?.image {
                urlsImageArray.add(url)
            }
            galleryImageRootViewController?.urlPictures = urlsImageArray as NSArray as! [String]
            galleryImageRootViewController?.photoFinalToShare = self.photoImageView.image ?? UIImage(named: "Placeholder")
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Public methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Load detail.
    /// - parameters:
    ///   - detail: Detail.
    /// - throws:
    public func loadDetail(detail: Film) {
        LogApp.info(String(describing: DetailViewController.self) + LoggingConstants.SEPARATOR + "Load detail...")
        self.movie = detail
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Lifecycle
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Managing the View
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    override public func viewDidLoad() {
        LogApp.debug(String(describing: DetailViewController.self) + LoggingConstants.SEPARATOR + "Did load...")
        super.viewDidLoad()
        AnalyticsManager.logContentViewForMovie()
        self.addObservers()
        self.configureView()
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Responding to View Events
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ResultManager.performResultRequests(self.movie?.imdbID ?? "", { (isReachability, success, errorMessage) in
            if success {
                
            } else {
                LogApp.error(String(describing: ResultsViewController.self) + LoggingConstants.SEPARATOR + "Perform result requests: \(String(describing: errorMessage))")
                let alertController: BaseAlertViewController = BaseAlertViewController(title: NSLocalizedString("Error", comment:""),
                                                                                       message: errorMessage ?? NSLocalizedString("Upps, there was an error.", comment:""),
                                                                                       preferredStyle: UIAlertController.Style.alert)
                
                let nowAction = UIAlertAction(title: NSLocalizedString("OK", comment:""),
                                              style: .default) { (_) in
                                                
                }
                nowAction.isEnabled = true
                alertController.addAction(nowAction)
                self.present(alertController, animated: true, completion: nil)
            }
        })
    }
    
    override public func viewDidAppear(_ animated: Bool) {
        LogApp.debug(String(describing: DetailViewController.self) + LoggingConstants.SEPARATOR + "View did appear...")
        super.viewDidAppear(animated)
    }
    
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override public func viewDidDisappear(_ animated: Bool) {
        LogApp.debug(String(describing: DetailViewController.self) + LoggingConstants.SEPARATOR + "View did disapper...")
        super.viewDidDisappear(animated)
        self.removeObservers()
    }
    
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.default
    }
    
    override func viewWillTransition(to size: CGSize,
                                     with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to:size, with: coordinator)
        
        coordinator.animate(alongsideTransition: { (UIViewControllerTransitionCoordinatorContext) -> Void in
            
            let orient = UIApplication.shared.statusBarOrientation
            
            switch orient {
            case .portrait:
                
                break
            case .portraitUpsideDown:
                
                break
            case .landscapeLeft:
                
                break
            case .landscapeRight:
                
                break
            default:
                
                break
            }
            
        }, completion: { (UIViewControllerTransitionCoordinatorContext) -> Void in
            
        })
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Delegate methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: BaseAnyControllerDelegate
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Update preferred font adjustment.
    /// - throws:
    override func updatePreferredFontAdjustment() {
        LogApp.debug(String(describing: DetailViewController.self) + LoggingConstants.SEPARATOR + "Update preferred font adjustment...")
        super.updatePreferredFontAdjustment()
        self.titleLabel.font = UIFont.preferredFont(forTextStyle: .title1)
        self.dateLabel.font = UIFont.preferredFont(forTextStyle: .subheadline)
        self.durationLabel.font = UIFont.preferredFont(forTextStyle: .subheadline)
        self.gendersLabel.font = UIFont.preferredFont(forTextStyle: .subheadline)
        self.synopsisTextView.font = UIFont.preferredFont(forTextStyle: .body)
        self.webTextView.font = UIFont.preferredFont(forTextStyle: .body)
    }
    
    /// Perform reload remote.
    /// - throws:
    override func performReloadRemote() {
        LogApp.debug(String(describing: DetailViewController.self) + LoggingConstants.SEPARATOR + "Perform reolad remote...")
        super.performReloadRemote()
        self.configureView()
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Private methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Add observers.
    /// - throws:
    private func addObservers() {
        LogApp.debug(String(describing: DetailViewController.self) + LoggingConstants.SEPARATOR + "Add observers...")
        NotificationCenter.default.addObserver(self, selector: #selector(DetailViewController.performResultReceivedNotification(notification:)), name: NSNotification.Name(rawValue: ResultNotificationConstants.PERFORM_RESULT_DATA), object: nil)
    }
    
    /// Remove observers.
    /// - throws:
    private func removeObservers() {
        LogApp.debug(String(describing: DetailViewController.self) + LoggingConstants.SEPARATOR + "Remove observers...")
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: ResultNotificationConstants.PERFORM_RESULT_DATA), object: nil)
    }
    
    /// Configure view.
    /// - throws:
    private func configureView() {
        LogApp.debug(String(describing: DetailViewController.self) + LoggingConstants.SEPARATOR + "Configure view...")
        self.navigationItem.title = NSLocalizedString("Movie", comment: "").uppercased()
        self.navigationItem.setRightBarButtonItems([self.sharedBarButtonItem], animated: true)
        self.titleLabel.text = self.movie?.title
        self.dateLabel.text = self.movie?.date
        self.durationLabel.text = self.movie?.duration
        self.gendersLabel.text = self.movie?.genders
        self.webTextView.text = self.movie?.web ?? "-"
        self.synopsisTextView.text = self.movie?.sypnosis ?? "-"
        self.photoImageView.image = nil
        let urlImage = self.movie?.image ?? ""
        let customAllowedSet =  CharacterSet.init(charactersIn: " ").inverted
        let escapedString = urlImage.addingPercentEncoding(withAllowedCharacters: customAllowedSet) ?? ""
        if (escapedString).count > 0 && (escapedString != "N/A") {
            self.galleryImageButton.isHidden = false
            let urlImageURL = URL(string: escapedString) ?? nil
            self.photoImageView!.af_setImage(
                withURL: urlImageURL!,
                placeholderImage: nil,
                filter: nil,
                imageTransition: .crossDissolve(0.2))
        } else {
            self.photoImageView.image = UIImage(named: "Placeholder")
            self.galleryImageButton.isHidden = true
        }
        self.photoImageView.clipsToBounds = true
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Actions
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Pressed shared.
    /// - parameters:
    ///   - sender: Sender.
    /// - throws:
    @IBAction func pressedShared(_ sender: Any) {
        LogApp.info(String(describing: DetailViewController.self) + LoggingConstants.SEPARATOR + "Pressed shared button...")
        let text = NSLocalizedString("Take a look at the movie", comment: "") + " " + (self.movie?.web ?? "")
        let textToShare = [text]
        let activityViewController = BaseActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.barButtonItem = self.sharedBarButtonItem
        activityViewController.excludedActivityTypes = [UIActivity.ActivityType.airDrop]
        self.present(activityViewController, animated: true, completion: {  () in
            
        })
    }
    
    /// Pressed image.
    /// - parameters:
    ///   - sender: Sender.
    /// - throws:
    @IBAction func pressedImage(_ sender: Any) {
        LogApp.info(String(describing: DetailViewController.self) + LoggingConstants.SEPARATOR + "Pressed image button...")
        self.performSegue(withIdentifier: "Images", sender: nil)
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Observer of notification
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Perform result received notification.
    /// - parameters:
    ///   - sender: Sender.
    /// - throws:
    @objc private func performResultReceivedNotification(notification: NSNotification) {
        LogApp.info(String(describing: DetailViewController.self) + LoggingConstants.SEPARATOR + "Perform result received notification...")
        if notification.name == NSNotification.Name(rawValue: ResultNotificationConstants.PERFORM_RESULT_DATA) {
            self.performReloadRemote()
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// MARK: UITableViewDataSource
////////////////////////////////////////////////////////////////////////////////////////////

extension DetailViewController {
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Configuring a Table View
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Inserting or Deleting Table Rows
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Customizing Section Names
    ////////////////////////////////////////////////////////////////////////////////////////////
    
}

////////////////////////////////////////////////////////////////////////////////////////////
// MARK: UITableViewDelegate
////////////////////////////////////////////////////////////////////////////////////////////

extension DetailViewController {
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Configuring Rows for the Table View
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Managing Accessory Views
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Managing Selections
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Modifying the Header and Footer of Sections
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Editing Table Rows
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Reordering Table Rows
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Tracking the Removal of Views
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Copying and Pasting Row Content
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Managing Table View Highlighting
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Managing Table View Focus
    ////////////////////////////////////////////////////////////////////////////////////////////
    
}




