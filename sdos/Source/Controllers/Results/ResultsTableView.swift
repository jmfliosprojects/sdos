//
//  ResultsTableView.swift
//  sdos
//
//  Created by Juan Miguel Fernández on 06/09/2019.
//  Copyright © 2019 Juan Miguel Fernández Lerena. All rights reserved.
//

import UIKit
import Logging

/// Table View for searcher.
class ResultsTableView: UITableView {
    
    var infoLabel: UILabel
    var anyResultsLabel: UILabel
    var errorResultsLabel: UILabel
    var minSearchLabel: UILabel
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Lifecycle
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Init.
    /// - parameters:
    ///   - aDecoder: Decoder.
    /// - throws:
    required init?(coder aDecoder: NSCoder) {
        self.infoLabel = UILabel()
        self.infoLabel.text = NSLocalizedString("Turn Off Airplane Mode or Use Wi-Fi to Access the Open Movie Database", comment: "")
        self.infoLabel.font = UIFont.preferredFont(forTextStyle: .title1)
        self.infoLabel.textColor = UIColor.lightGray
        self.infoLabel.textAlignment = NSTextAlignment.center
        self.infoLabel.numberOfLines = 0
        self.infoLabel.isHidden = true
        self.anyResultsLabel = UILabel()
        self.anyResultsLabel.text = NSLocalizedString("Any result", comment: "")
        self.anyResultsLabel.font = UIFont.preferredFont(forTextStyle: .title1)
        self.anyResultsLabel.textColor = UIColor.lightGray
        self.anyResultsLabel.textAlignment = NSTextAlignment.center
        self.anyResultsLabel.numberOfLines = 0
        self.anyResultsLabel.isHidden = true
        self.errorResultsLabel = UILabel()
        self.errorResultsLabel.text = ""
        self.errorResultsLabel.font = UIFont.preferredFont(forTextStyle: .title1)
        self.errorResultsLabel.textColor = UIColor.lightGray
        self.errorResultsLabel.textAlignment = NSTextAlignment.center
        self.errorResultsLabel.numberOfLines = 0
        self.errorResultsLabel.isHidden = true
        self.minSearchLabel = UILabel()
        self.minSearchLabel.text = NSLocalizedString("Enter more text", comment: "")
        self.minSearchLabel.font = UIFont.preferredFont(forTextStyle: .title1)
        self.minSearchLabel.textColor = UIColor.lightGray
        self.minSearchLabel.textAlignment = NSTextAlignment.center
        self.minSearchLabel.numberOfLines = 0
        self.minSearchLabel.isHidden = true
        super.init(coder: aDecoder)
        adjustSizeOfLabels()
        addSubview(self.infoLabel)
        addSubview(self.anyResultsLabel)
        addSubview(self.errorResultsLabel)
        addSubview(self.minSearchLabel)
    }
    
    /// Reload data.
    /// - throws:
    override func reloadData() {
        
    }
    
    // Adjust the size so that the indicator is always in the middle of the screen.
    /// - throws:
    override func layoutSubviews() {
        super.layoutSubviews()
        self.adjustSizeOfLabels()
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Public methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    //// Update preferred font adjustment.
    /// - throws:
    func updatePreferredFontAdjustment() {
        LogApp.info(String(describing: ResultsTableView.self) + LoggingConstants.SEPARATOR + "Update preferred font adjustment...")
        self.infoLabel.font = UIFont.preferredFont(forTextStyle: .title1)
        self.anyResultsLabel.font = UIFont.preferredFont(forTextStyle: .title1)
        self.errorResultsLabel.font = UIFont.preferredFont(forTextStyle: .title1)
        self.minSearchLabel.font = UIFont.preferredFont(forTextStyle: .title1)
    }
    
    /// Show indicator with not reachable.
    /// - throws:
    func showIndicatorWithNotReachable() {
        LogApp.info(String(describing: ResultsTableView.self) + LoggingConstants.SEPARATOR + "Show indicator with not reachable...")
        self.infoLabel.isHidden = false
        self.infoLabel.alpha = 1
    }
    
    /// Show indicator with reachable.
    /// - throws:
    func showIndicatorWithReachable() {
        LogApp.info(String(describing: ResultsTableView.self) + LoggingConstants.SEPARATOR + "Show indicator with reachable...")
        self.infoLabel.isHidden = true
        self.infoLabel.alpha = 0
    }
    
    /// Show any results.
    /// - throws:
    func showAnyResults() {
        LogApp.info(String(describing: ResultsTableView.self) + LoggingConstants.SEPARATOR + "Show any results...")
        self.anyResultsLabel.isHidden = false
        self.anyResultsLabel.alpha = 1
    }
    
    /// Hide any results.
    /// - throws:
    func hideAnyResults() {
        LogApp.info(String(describing: ResultsTableView.self) + LoggingConstants.SEPARATOR + "Hide any results...")
        self.anyResultsLabel.isHidden = true
        self.anyResultsLabel.alpha = 0
    }
    
    /// Show error.
    /// - parameters:
    ///     - message: Message.
    /// - throws:
    func showErrorResults(_ message: String) {
        LogApp.info(String(describing: ResultsTableView.self) + LoggingConstants.SEPARATOR + "Show error results...")
        self.errorResultsLabel.text = message
        self.errorResultsLabel.isHidden = false
        self.errorResultsLabel.alpha = 1
    }
    
    /// Hide error results.
    /// - throws:
    func hideErrorResults() {
        LogApp.info(String(describing: ResultsTableView.self) + LoggingConstants.SEPARATOR + "Hide error results...")
        self.errorResultsLabel.isHidden = true
        self.errorResultsLabel.alpha = 0
    }
    
    /// Show min search.
    /// - throws:
    func showMinSearch() {
        LogApp.info(String(describing: ResultsTableView.self) + LoggingConstants.SEPARATOR + "Show min search...")
        self.minSearchLabel.isHidden = false
        self.minSearchLabel.alpha = 1
    }
    
    /// Hide min search.
    /// - throws:
    func hideMinSearch() {
        LogApp.info(String(describing: ResultsTableView.self) + LoggingConstants.SEPARATOR + "Hide min seach...")
        self.minSearchLabel.isHidden = true
        self.minSearchLabel.alpha = 0
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Private methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Adjust size.
    /// - throws:
    fileprivate func adjustSizeOfLabels() {
        if UIDevice.current.orientation.isLandscape {
            self.infoLabel.frame = CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.width - 500)
            self.anyResultsLabel.frame = CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.width - 500)
            self.errorResultsLabel.frame = CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.width - 500)
            self.minSearchLabel.frame = CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.width - 500)
        } else {
            self.infoLabel.frame = CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.width)
            self.anyResultsLabel.frame = CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.width)
            self.errorResultsLabel.frame = CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.width)
            self.minSearchLabel.frame = CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.width)
        }
    }
    
}
