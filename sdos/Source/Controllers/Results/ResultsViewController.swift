//
//  ResultsViewController.swift
//  sdos
//
//  Created by Juan Miguel Fernández on 06/09/2019.
//  Copyright © 2019 Juan Miguel Fernández Lerena. All rights reserved.
//

import UIKit
import Logging
import AlamofireImage
import CoreData

/// Controller of results.
class ResultsViewController: BaseTableViewController {
    
    @IBOutlet weak var moreResultsButton: UIButton! {
        didSet {
            self.moreResultsButton.setTitle(NSLocalizedString("More results...", comment: ""), for: UIControl.State())
        }
    }
    
    var searcherViewController: SearcherViewController?
    var activityView = UIActivityIndicatorView(style: .gray)
    
    private var context: NSManagedObjectContext!
    private var frc: NSFetchedResultsController = NSFetchedResultsController<Film>()
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Navigation
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    override func prepare(for segue: UIStoryboardSegue,
                          sender: Any?) {
        
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Public methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Show searching.
    /// - throws:
    func showSearching() {
        LogApp.info(String(describing: ResultsViewController.self) + LoggingConstants.SEPARATOR + "Show searching...")
        self.moreResultsButton.isHidden = true
        UIView.animate(withDuration: 1, animations: {
            (self.tableView as? ResultsTableView)?.hideErrorResults()
        })
        self.activityView.startAnimating()
    }
    
    /// Hide searching.
    /// - throws:
    func hideSearching() {
        LogApp.info(String(describing: ResultsViewController.self) + LoggingConstants.SEPARATOR + "Hide searching...")
        self.moreResultsButton.isHidden = false
        self.activityView.stopAnimating()
    }
    
    /// Show error.
    /// - parameters:
    ///     - message: Message.
    /// - throws:
    func showError(_ message: String) {
        LogApp.info(String(describing: ResultsViewController.self) + LoggingConstants.SEPARATOR + "Show error...")
        self.moreResultsButton.isHidden = true
        UIView.animate(withDuration: 1, animations: {
            (self.tableView as? ResultsTableView)?.showErrorResults(message)
            (self.tableView as? ResultsTableView)?.hideAnyResults()
        })
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Lifecycle
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Managing the View
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    override public func viewDidLoad() {
        LogApp.debug(String(describing: ResultsViewController.self) + LoggingConstants.SEPARATOR + "Did load...")
        super.viewDidLoad()
        AnalyticsManager.logContentViewForResults()
        self.addObservers()
        self.configureView()
        self.setupFetchedResultsController()
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Responding to View Events
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override public func viewDidAppear(_ animated: Bool) {
        LogApp.debug(String(describing: ResultsViewController.self) + LoggingConstants.SEPARATOR + "View did appear...")
        super.viewDidAppear(animated)
    }
    
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override public func viewDidDisappear(_ animated: Bool) {
        LogApp.debug(String(describing: ResultsViewController.self) + LoggingConstants.SEPARATOR + "View did disapper...")
        super.viewDidDisappear(animated)
        self.removeObservers()
    }
    
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.default
    }
    
    override func viewWillTransition(to size: CGSize,
                                     with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to:size, with: coordinator)
        
        coordinator.animate(alongsideTransition: { (UIViewControllerTransitionCoordinatorContext) -> Void in
            
            let orient = UIApplication.shared.statusBarOrientation
            
            (self.tableView as? ResultsTableView)?.layoutSubviews()
            
            switch orient {
            case .portrait:
                
                break
            case .portraitUpsideDown:
                
                break
            case .landscapeLeft:
                
                break
            case .landscapeRight:
                
                break
            default:
                
                break
            }
            
        }, completion: { (UIViewControllerTransitionCoordinatorContext) -> Void in
            
        })
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Delegate methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: BaseAnyControllerDelegate
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Update preferred font adjustment.
    /// - throws:
    override func updatePreferredFontAdjustment() {
        LogApp.debug(String(describing: ResultsViewController.self) + LoggingConstants.SEPARATOR + "Update preferred font adjustment...")
        super.updatePreferredFontAdjustment()
        (self.tableView as? ResultsTableView)?.updatePreferredFontAdjustment()
    }
    
    /// Perform reload remote.
    /// - throws:
    override func performReloadRemote() {
        LogApp.debug(String(describing: ResultsViewController.self) + LoggingConstants.SEPARATOR + "Perform reolad remote...")
        super.performReloadRemote()
        self.configureView()
    }
    
    /// Show indicator with not reachable.
    /// - throws:
    override func showIndicatorWithNotReachable() {
        LogApp.debug(String(describing: ResultsViewController.self) + LoggingConstants.SEPARATOR + "Show indicator with not reachable...")
        UIView.animate(withDuration: 1, animations: {
            (self.tableView as? ResultsTableView)?.showIndicatorWithNotReachable()
            self.hideTableViewState()
            self.tableView.beginUpdates()
            self.tableView.reloadData()
            self.tableView.endUpdates()
        })
    }
    
    /// Show indicator with is reachable.
    /// - throws:
    override func showIndicatorWithIsReachable() {
        LogApp.debug(String(describing: ResultsViewController.self) + LoggingConstants.SEPARATOR + "Show indicator with is reachable...")
        UIView.animate(withDuration: 1, animations: {
            (self.tableView as? ResultsTableView)?.showIndicatorWithReachable()
            self.showTableViewState()
            self.tableView.beginUpdates()
            self.tableView.reloadData()
            self.tableView.endUpdates()
        })
    }    

    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Private methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Add observers.
    /// - throws:
    private func addObservers() {
        LogApp.debug(String(describing: ResultsViewController.self) + LoggingConstants.SEPARATOR + "Add observers...")
        
    }
    
    /// Remove observers.
    /// - throws:
    private func removeObservers() {
        LogApp.debug(String(describing: ResultsViewController.self) + LoggingConstants.SEPARATOR + "Remove observers...")
        
    }
    
    /// Configure view.
    /// - throws:
    private func configureView() {
        LogApp.debug(String(describing: ResultsViewController.self) + LoggingConstants.SEPARATOR + "Configure view...")
        self.activityView.frame = CGRect(x: self.view.bounds.width/2 - 40, y: 0, width: 80, height: 80)
        self.view.addSubview(self.activityView)
    }
    
    /// Setup search controller.
    /// - throws:
    private func setupFetchedResultsController() {
        LogApp.debug(String(describing: ResultsViewController.self) + LoggingConstants.SEPARATOR + "Setup fetched results controller...")
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedObjectContext = appDelegate.managedObjectContext
        self.context = managedObjectContext
        self.frc = getFRC()
        self.fetch(frcToFetch: self.frc)
    }
    
    /// Fetch.
    /// - parameters:
    ///   - frcToFetch: frcToFetch.
    /// - throws:
    private func fetch(frcToFetch: NSFetchedResultsController<Film>) {
        LogApp.debug(String(describing: ResultsViewController.self) + LoggingConstants.SEPARATOR + "Fetch...")
        do {
            try frcToFetch.performFetch()
        } catch {
            return
        }
    }
    
    /// Fetch request.
    /// - throws:
    /// - return: Fetch request.
    private func fetchRequest() -> NSFetchRequest<Film> {
        LogApp.debug(String(describing: ResultsViewController.self) + LoggingConstants.SEPARATOR + "Fetch request...")
        let fetchRequest = NSFetchRequest<Film>(entityName: FilmEntity.name)
        let sortDescriptor1 = NSSortDescriptor.init(key: FilmAttribute.identifier, ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor1]
        let compound =  NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [])
        fetchRequest.predicate = compound
        return fetchRequest
    }
    
    /// Get FRC.
    /// - throws:
    /// - return: FRC
    private func getFRC() -> NSFetchedResultsController<Film> {
        LogApp.debug(String(describing: ResultsViewController.self) + LoggingConstants.SEPARATOR + "Get FRC...")
        frc = NSFetchedResultsController(
            fetchRequest: fetchRequest(),
            managedObjectContext: self.context,
            sectionNameKeyPath: nil,
            cacheName: nil)
        frc.delegate = self
        return frc
    }
    
    
    /// Configure empty table view.
    /// - throws:
    private func configureEmptyTableView() {
        LogApp.debug(String(describing: ResultsViewController.self) + LoggingConstants.SEPARATOR + "Configure empty table view...")
        self.moreResultsButton.isHidden = true
        UIView.animate(withDuration: 1, animations: {
            (self.tableView as? ResultsTableView)?.hideAnyResults()
        })
    }
    
    /// Configure not empty table view.
    /// - throws:
    private func configureNotEmptyTableView() {
        LogApp.debug(String(describing: ResultsViewController.self) + LoggingConstants.SEPARATOR + "Configure not empty table view...")
        self.moreResultsButton.isHidden = false
        UIView.animate(withDuration: 1, animations: {
            (self.tableView as? ResultsTableView)?.hideAnyResults()
        })
    }
    
    /// Hide table view state.
    /// - throws:
    private func hideTableViewState() {
        LogApp.debug(String(describing: ResultsViewController.self) + LoggingConstants.SEPARATOR + "Hide table view state...")
        
    }
    
    /// Show table view state.
    /// - throws:
    private func showTableViewState() {
        LogApp.debug(String(describing: ResultsViewController.self) + LoggingConstants.SEPARATOR + "Show table view state...")
        UIView.animate(withDuration: 1, animations: {
            (self.tableView as? ResultsTableView)?.hideAnyResults()
        })
    }
    
    /// Configure cell.
    /// - parameters:
    ///   - cell: Cell.
    /// - throws:
    private func configureCell(cell: ResultTableViewCell,
                               atIndexPath indexPath: NSIndexPath) {
        LogApp.debug(String(describing: ResultsViewController.self) + LoggingConstants.SEPARATOR + "Configure cell...")
        let movie = self.frc.object(at: indexPath as IndexPath)
        cell.titleLabel.text = movie.title
        cell.dateLabel.text = movie.date
        cell.durationLabel.text = movie.duration
        cell.gendersLabel.text = movie.genders
        cell.detailImageView.image = nil
        let urlImage = movie.image ?? ""
        let size = CGSize(width: 80, height: 80 )
        let imageScaledFilter = AspectScaledToFillSizeFilter(size: size)
        let customAllowedSet =  CharacterSet.init(charactersIn: " ").inverted
        let escapedString = urlImage.addingPercentEncoding(withAllowedCharacters: customAllowedSet) ?? ""
        if (escapedString).count > 0 && (escapedString != "N/A"){
            cell.detailImageView.layer.cornerRadius = 8
            let urlImageURL = URL(string: escapedString) ?? nil
            cell.detailImageView!.af_setImage(
                withURL: urlImageURL!,
                placeholderImage: nil,
                filter: imageScaledFilter,
                imageTransition: .crossDissolve(0.2))
        } else {
            cell.detailImageView.contentMode = UIView.ContentMode.scaleAspectFill
            cell.detailImageView.image = UIImage(named: "Placeholder")
            cell.detailImageView.layer.cornerRadius = 0
        }
        cell.detailImageView.clipsToBounds = true
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Actions
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Pressed more results button.
        /// - parameters:
        ///   - sender: Sender.
        /// - throws:
        @IBAction func pressedMoreResults(_ sender: Any) {
            LogApp.info(String(describing: SearcherViewController.self) + LoggingConstants.SEPARATOR + "Pressed more results button...")
            self.searcherViewController?.performMoreResults()
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Observer of notification
    ////////////////////////////////////////////////////////////////////////////////////////////
    
}

////////////////////////////////////////////////////////////////////////////////////////////
// MARK: UITableViewDataSource
////////////////////////////////////////////////////////////////////////////////////////////

extension ResultsViewController {
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Configuring a Table View
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    override func tableView(_ tableView: UITableView,
                            numberOfRowsInSection section: Int) -> Int {
        if let sections = self.frc.sections {
            let currentSection = sections[section]
            return currentSection.numberOfObjects
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView,
                            cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ResultTableViewCell = tableView.dequeueReusableCell(withIdentifier: ResultCell.identifier, for: indexPath) as! ResultTableViewCell
        cell.prepareForReuse()
        self.configureCell(cell: cell, atIndexPath: indexPath as NSIndexPath)
        return cell
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Inserting or Deleting Table Rows
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Customizing Section Names
    ////////////////////////////////////////////////////////////////////////////////////////////
    
}

////////////////////////////////////////////////////////////////////////////////////////////
// MARK: UITableViewDelegate
////////////////////////////////////////////////////////////////////////////////////////////

extension ResultsViewController {
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Configuring Rows for the Table View
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    override func tableView(_ tableView: UITableView,
                            estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView,
                            heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView,
                            willDisplay cell: UITableViewCell,
                            forRowAt indexPath: IndexPath) {
    }
    
    override func tableView(_ tableView: UITableView,
                            didEndDisplaying cell: UITableViewCell,
                            forRowAt indexPath: IndexPath) {
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Managing Accessory Views
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Managing Selections
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    override func tableView(_ tableView: UITableView,
                            willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        return indexPath
    }
    
    override func tableView(_ tableView: UITableView,
                            didSelectRowAt indexPath: IndexPath) {
        let object = self.frc.object(at: indexPath as IndexPath)
        let cell = tableView.cellForRow(at: indexPath)
        cell?.setSelected(false, animated: true)
        self.tableView.deselectRow(at: indexPath, animated: true)
        self.searcherViewController?.openDetail(detail: object)
    }
    
    override func tableView(_ tableView: UITableView,
                            willDeselectRowAt indexPath: IndexPath) -> IndexPath? {
        return indexPath
    }
    
    override func tableView(_ tableView: UITableView,
                            didDeselectRowAt indexPath: IndexPath) {
        
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Modifying the Header and Footer of Sections
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Editing Table Rows
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Reordering Table Rows
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Tracking the Removal of Views
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Copying and Pasting Row Content
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Managing Table View Highlighting
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    override func tableView(_ tableView: UITableView,
                            shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView,
                            didHighlightRowAt indexPath: IndexPath) {
        let cell  = tableView.cellForRow(at: indexPath)
        cell?.setHighlighted(true, animated: true)
    }
    
    override func tableView(_ tableView: UITableView,
                            didUnhighlightRowAt indexPath: IndexPath) {
        let cell  = tableView.cellForRow(at: indexPath)
        cell?.setHighlighted(false, animated: true)
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Managing Table View Focus
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    override func tableView(_ tableView: UITableView,
                            canFocusRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
}

////////////////////////////////////////////////////////////////////////////////////////////
// MARK: NSFetchedResultsControllerDelegate
////////////////////////////////////////////////////////////////////////////////////////////

extension ResultsViewController: NSFetchedResultsControllerDelegate {
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Responding to Changes
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>,
                    didChange anObject: Any,
                    at indexPath: IndexPath?,
                    for type: NSFetchedResultsChangeType,
                    newIndexPath: IndexPath?) {
        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                self.tableView.insertRows(at: [indexPath as IndexPath], with: .fade)
            }
            break;
        case .delete:
            if let indexPath = indexPath {
                self.tableView.deleteRows(at: [indexPath as IndexPath], with: .fade)
            }
            break;
        case .update:
            if let indexPath = indexPath {
                self.tableView.reloadRows(at: [indexPath as IndexPath], with: .fade)
            }
            break;
        case .move:
            if let indexPath = indexPath {
                self.tableView.deleteRows(at: [indexPath as IndexPath], with: .fade)
            }
            if let newIndexPath = newIndexPath {
                self.tableView.insertRows(at: [newIndexPath as IndexPath], with: .fade)
            }
            break;
        @unknown default:
            LogApp.error(String(describing: ResultsViewController.self) + LoggingConstants.SEPARATOR + "Error responding to Changes...")
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>,
                    didChange sectionInfo: NSFetchedResultsSectionInfo,
                    atSectionIndex sectionIndex: Int,
                    for type: NSFetchedResultsChangeType) {
        switch type {
        case .insert:
            self.tableView.insertSections(NSIndexSet(index: sectionIndex) as IndexSet, with: .fade)
        case .delete:
            self.tableView.deleteSections(NSIndexSet(index: sectionIndex) as IndexSet, with: .fade)
        case .move:
            break
        case .update:
            break
        @unknown default:
            LogApp.error(String(describing: ResultsViewController.self) + LoggingConstants.SEPARATOR + "Error responding to Changes...")
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.endUpdates()
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Customizing Section Names
    ////////////////////////////////////////////////////////////////////////////////////////////
    
}

////////////////////////////////////////////////////////////////////////////////////////////
// MARK: UITableViewDataSourcePrefetching
////////////////////////////////////////////////////////////////////////////////////////////

extension ResultsViewController: UITableViewDataSourcePrefetching {
    
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        LogApp.debug(String(describing: ResultsViewController.self) + LoggingConstants.SEPARATOR + "prefetchRowsAt...")
        
    }
    
    func tableView(_ tableView: UITableView,
                   cancelPrefetchingForRowsAt indexPaths: [IndexPath]) {
        LogApp.debug(String(describing: ResultsViewController.self) + LoggingConstants.SEPARATOR + "cancelPrefetchingForRowsAt...")
        
    }
    
}




