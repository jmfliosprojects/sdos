//
//  ResultTableViewCell.swift
//  sdos
//
//  Created by Juan Miguel Fernández on 06/09/2019.
//  Copyright © 2019 Juan Miguel Fernández Lerena. All rights reserved.
//

import UIKit

/// Constants for cell.
struct ResultCell {
    
    static let identifier = "Result"
    
}

/// Cell for Resutl.
class ResultTableViewCell: UITableViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var detailImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var gendersLabel: UILabel!
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // Managing Cell Selection and Highlighting
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    override func setSelected(_ selected: Bool,
                              animated: Bool) {
        super.setSelected(selected, animated: animated)
        if (selected) {
            self.accessoryType = .none
        } else {
            self.accessoryType = .none
        }
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        if highlighted {
            self.accessoryType = .none
            self.containerView.backgroundColor = UIButton(type: .system).tintColor
            self.titleLabel.textColor = UIColor.white
            self.dateLabel.textColor = UIColor.white
            self.durationLabel.textColor = UIColor.white
            self.gendersLabel.textColor = UIColor.white
        } else {
            self.accessoryType = .none
            self.containerView.backgroundColor = UIColor.clear
            self.titleLabel.textColor = UIColor.darkText
            self.dateLabel.textColor = UIColor.lightGray
            self.durationLabel.textColor = UIColor.lightGray
            self.gendersLabel.textColor = UIColor.lightGray
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // Reusing Cells
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Lifecycle
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
}

