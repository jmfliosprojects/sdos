//
//  TabBarController.swift
//  sdos
//
//  Created by Juan Miguel Fernández on 05/09/2019.
//  Copyright © 2019 Juan Miguel Fernández Lerena. All rights reserved.
//

import Foundation
import Logging

/// Controller of tabs.
class TabBarController: UITabBarController {
    
    private static let TOTAL_TABS: Int = 2
    private static let SEARCH_TAB: Int = 0
    private static let INFO_TAB: Int = 1
    private static let SELECTED_TAB: Int = 0
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Navigation
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    override func prepare(for segue: UIStoryboardSegue,
                          sender: Any?) {
        
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Public methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Lifecycle
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Managing the View
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    override public func viewDidLoad() {
        LogApp.debug(String(describing: TabBarController.self) + LoggingConstants.SEPARATOR + "Did load...")
        super.viewDidLoad()
        self.addObservers()
        self.configureView()
        self.setUpTabBarItemTags()
        self.loadCustomTabBarItemsOrder()
        self.getSavedTabBarItemsOrder()
        self.performSelectedTab()
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Responding to View Events
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override public func viewDidAppear(_ animated: Bool) {
        LogApp.debug(String(describing: TabBarController.self) + LoggingConstants.SEPARATOR + "View did appear...")
        super.viewDidAppear(animated)
    }
    
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override public func viewDidDisappear(_ animated: Bool) {
        LogApp.debug(String(describing: TabBarController.self) + LoggingConstants.SEPARATOR + "View did disapper...")
        super.viewDidDisappear(animated)
        self.removeObservers()
    }
    
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.default
    }
    
    override func viewWillTransition(to size: CGSize,
                                     with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to:size, with: coordinator)
        
        coordinator.animate(alongsideTransition: { (UIViewControllerTransitionCoordinatorContext) -> Void in
            
            let orient = UIApplication.shared.statusBarOrientation
            
            switch orient {
            case .portrait:
                
                break
            case .portraitUpsideDown:
                
                break
            case .landscapeLeft:
                
                break
            case .landscapeRight:
                
                break
            default:
                
                break
            }
            
        }, completion: { (UIViewControllerTransitionCoordinatorContext) -> Void in
            
        })
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Delegate methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Private methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Add observers.
    /// - throws:
    private func addObservers() {
        LogApp.debug(String(describing: TabBarController.self) + LoggingConstants.SEPARATOR + "Add observers...")
        
    }
    
    /// Remove observers.
    /// - throws:
    private func removeObservers() {
        LogApp.debug(String(describing: TabBarController.self) + LoggingConstants.SEPARATOR + "Remove observers...")
        
    }
    
    /// Configure view.
    /// - throws:
    private func configureView() {
        LogApp.debug(String(describing: TabBarController.self) + LoggingConstants.SEPARATOR + "Configure view...")
        let viewControllers = self.viewControllers
        if let totalTabs = viewControllers?.count {
            switch totalTabs {
            case TabBarController.TOTAL_TABS:
                let xxx = self.tabBar.items?[TabBarController.SEARCH_TAB]
                xxx?.title = "ss"
                self.tabBar.items?[TabBarController.SEARCH_TAB].title = NSLocalizedString("Search", comment: "")
                self.tabBar.items?[TabBarController.INFO_TAB].title = NSLocalizedString("Info", comment: "")
                break
            default:
                break
            }
        }
        self.delegate = self
    }
    
    /// Perform selected tab.
    /// - throws:
    private func performSelectedTab() {
        LogApp.debug(String(describing: TabBarController.self) + LoggingConstants.SEPARATOR + "Check state app...")
        self.selectedIndex = TabBarController.SELECTED_TAB
    }
    
    /// Set up tag to compare with when pulling from defaults and for saving initial tab bar change.
    /// - throws:
    private func setUpTabBarItemTags() {
        LogApp.debug(String(describing: TabBarController.self) + LoggingConstants.SEPARATOR + "Set up tabBarItem tags...")
        var tag = 0
        if let viewControllers = viewControllers {
            for view in viewControllers {
                view.tabBarItem.tag = tag
                tag += 1
            }
        }
    }
    
    /// Load custom tab bar items order.
    /// - throws:
    private func loadCustomTabBarItemsOrder() {
        LogApp.debug(String(describing: TabBarController.self) + LoggingConstants.SEPARATOR + "Load custom tab bar items order...")
        var orderedTagItems = [Int]()
        let tabBarOrderKeyArray = CustomConfigurationManager.tabBarOrderKey()
        for tagTabBar in tabBarOrderKeyArray {
            orderedTagItems.append(tagTabBar)
        }
        UserDefaults.standard.set(orderedTagItems, forKey: "tabBarOrderKey")
    }
    
    /// Get Saved Tab Bar Order from defaults.
    /// - throws:
    private func getSavedTabBarItemsOrder() {
        LogApp.debug(String(describing: TabBarController.self) + LoggingConstants.SEPARATOR + "Get Saved Tab Bar Order from defaults...")
        var newViewControllerOrder = [UIViewController]()
        if let initialViewControllers = viewControllers {
            if let tabBarOrder = UserDefaults.standard.object(forKey: "tabBarOrderKey") as? [Int] {
                for tag in tabBarOrder {
                    newViewControllerOrder.append(initialViewControllers[tag])
                }
                setViewControllers(newViewControllerOrder, animated: false)
            }
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Actions
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Observer of notification
    ////////////////////////////////////////////////////////////////////////////////////////////
    
}

////////////////////////////////////////////////////////////////////////////////////////////
// MARK: UITabBarControllerDelegate
////////////////////////////////////////////////////////////////////////////////////////////

extension TabBarController: UITabBarControllerDelegate {
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Managing Tab Bar Selections
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    func tabBarController(_ tabBarController: UITabBarController,
                          shouldSelect viewController: UIViewController) -> Bool {
        return true
    }
    
    func tabBarController(_ tabBarController: UITabBarController,
                          didSelect viewController: UIViewController) {
        
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Managing Tab Bar Customizations
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    func tabBarController(_ tabBarController: UITabBarController,
                          willBeginCustomizing viewControllers: [UIViewController]) {
        
    }
    
    func tabBarController(_ tabBarController: UITabBarController,
                          willEndCustomizing viewControllers: [UIViewController],
                          changed: Bool) {
        
    }
    
    func tabBarController(_ tabBarController: UITabBarController,
                          didEndCustomizing viewControllers: [UIViewController],
                          changed: Bool) {
        
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Overriding View Rotation Settings
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    func tabBarControllerSupportedInterfaceOrientations(_ tabBarController: UITabBarController) -> UIInterfaceOrientationMask {
        return .all
    }
    
    func tabBarControllerPreferredInterfaceOrientationForPresentation(_ tabBarController: UITabBarController) -> UIInterfaceOrientation {
        let orient = UIApplication.shared.statusBarOrientation
        switch orient {
        case .portrait:
            return UIInterfaceOrientation.portrait
        case .portraitUpsideDown:
            return UIInterfaceOrientation.portraitUpsideDown
        case .landscapeLeft:
            return UIInterfaceOrientation.landscapeLeft
        case .landscapeRight:
            return UIInterfaceOrientation.landscapeRight
        default:
            return UIInterfaceOrientation.portrait
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Supporting Custom Tab Bar Transition Animations
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    func tabBarController(_ tabBarController: UITabBarController,
                          animationControllerForTransitionFrom fromVC: UIViewController,
                          to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return nil
    }
    
    func tabBarController(_ tabBarController: UITabBarController,
                          interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return nil
    }
    
}
