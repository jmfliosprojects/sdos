//
//  MoreViewController.swift
//  sdos
//
//  Created by Juan Miguel Fernández on 05/09/2019.
//  Copyright © 2019 Juan Miguel Fernández Lerena. All rights reserved.
//

import UIKit
import Logging

/// Controller of more view.
class MoreViewController: BaseTabTableViewController {

    @IBOutlet weak var closeBarButtonItem: UIBarButtonItem! {
        didSet {
            self.closeBarButtonItem.title = NSLocalizedString("Close", comment: "")
        }
    }
    @IBOutlet weak var settingsBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var userTableViewCell: UITableViewCell!
    @IBOutlet weak var emailInfoTableViewCell: UITableViewCell!
    @IBOutlet weak var phoneNumberTableViewCell: UITableViewCell!
    @IBOutlet weak var meImageView: UIImageView!
    @IBOutlet weak var noteTextView: UITextView! {
        didSet {
            self.noteTextView.text =
                NSLocalizedString("The technical test is to develop an application that allows you to search for movies through the API “The Open Movie Database” http://www.omdbapi.com/", comment: "")
        }
    }
    @IBOutlet weak var nameLabel: UILabel!{
        didSet {
            self.nameLabel.text = "-"
        }
    }
    @IBOutlet weak var jobLabel: UILabel!{
        didSet {
            self.jobLabel.text = "-"
        }
    }
    @IBOutlet weak var titleEmailLabel: UILabel! {
        didSet {
            self.titleEmailLabel.text = NSLocalizedString("Email", comment: "")
        }
    }
    @IBOutlet weak var emailTextView: UITextView!{
        didSet {
            self.emailTextView.text = "-"
        }
    }
    @IBOutlet weak var titlePhoneNumberLabel: UILabel! {
        didSet {
            self.titlePhoneNumberLabel.text = NSLocalizedString("Phone number", comment: "")
        }
    }
    @IBOutlet weak var phoneNumberTextView: UITextView! {
        didSet {
            self.phoneNumberTextView.text = "-"
        }
    }
    @IBOutlet weak var noteView: UIView!
    
    /// To active modal presentation.
    public var modalPresentationOn: Bool = false
    
    private static let TAB_PRESENTATION_SECTIONS: Int = 3
    private static let MODAL_PRESENTATION_SECTIONS: Int = 2
    private static let DESCRIPTION_SECTION: Int = 2
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Navigation
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    override func prepare(for segue: UIStoryboardSegue,
                          sender: Any?) {
        
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Public methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Lifecycle
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Managing the View
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    override public func viewDidLoad() {
        LogApp.debug(String(describing: MoreViewController.self) + LoggingConstants.SEPARATOR + "Did load...")
        super.viewDidLoad()
        AnalyticsManager.logContentViewForMore()
        self.addObservers()
        self.configureView()
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Responding to View Events
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override public func viewDidAppear(_ animated: Bool) {
        LogApp.debug(String(describing: MoreViewController.self) + LoggingConstants.SEPARATOR + "View did appear...")
        super.viewDidAppear(animated)
    }
    
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override public func viewDidDisappear(_ animated: Bool) {
        LogApp.debug(String(describing: MoreViewController.self) + LoggingConstants.SEPARATOR + "View did disapper...")
        super.viewDidDisappear(animated)
        self.removeObservers()
    }
    
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.default
    }
    
    override func viewWillTransition(to size: CGSize,
                                     with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to:size, with: coordinator)
        
        coordinator.animate(alongsideTransition: { (UIViewControllerTransitionCoordinatorContext) -> Void in
            
            let orient = UIApplication.shared.statusBarOrientation
            
            switch orient {
            case .portrait:
                
                break
            case .portraitUpsideDown:
                
                break
            case .landscapeLeft:
                
                break
            case .landscapeRight:
                
                break
            default:
                
                break
            }
            
        }, completion: { (UIViewControllerTransitionCoordinatorContext) -> Void in
            
        })
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Delegate methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: BaseAnyControllerDelegate
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Update preferred font adjustment.
    /// - throws:
    override func updatePreferredFontAdjustment() {
        LogApp.debug(String(describing: MoreViewController.self) + LoggingConstants.SEPARATOR + "Update preferred font adjustment...")
        super.updatePreferredFontAdjustment()
        self.noteTextView.font = UIFont.preferredFont(forTextStyle: .footnote)
        self.nameLabel.font = UIFont.preferredFont(forTextStyle: .title1)
        self.jobLabel.font = UIFont.preferredFont(forTextStyle: .callout)
        self.titleEmailLabel.font = UIFont.preferredFont(forTextStyle: .callout)
        self.titlePhoneNumberLabel.font = UIFont.preferredFont(forTextStyle: .callout)
        self.emailTextView.font = UIFont.preferredFont(forTextStyle: .body)
        self.phoneNumberTextView.font = UIFont.preferredFont(forTextStyle: .body)
    }
    
    /// Perform reload remote.
    /// - throws:
    override func performReloadRemote() {
        LogApp.debug(String(describing: MoreViewController.self) + LoggingConstants.SEPARATOR + "Perform reolad remote...")
        super.performReloadRemote()
        self.configureView()
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Private methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Add observers.
    /// - throws:
    private func addObservers() {
        LogApp.debug(String(describing: MoreViewController.self) + LoggingConstants.SEPARATOR + "Add observers...")
        
    }
    
    /// Remove observers.
    /// - throws:
    private func removeObservers() {
        LogApp.debug(String(describing: MoreViewController.self) + LoggingConstants.SEPARATOR + "Remove observers...")
        
    }
    
    /// Configure view.
    /// - throws:
    private func configureView() {
        LogApp.debug(String(describing: MoreViewController.self) + LoggingConstants.SEPARATOR + "Configure view...")
        if !self.modalPresentationOn {
            self.closeBarButtonItem.tintColor = UIColor.clear
            self.closeBarButtonItem.isEnabled = false
            self.closeBarButtonItem.isAccessibilityElement = false
            self.navigationItem.title = NSLocalizedString("One More Thing", comment: "").uppercased()
        } else {
            self.settingsBarButtonItem.tintColor = UIColor.clear
            self.settingsBarButtonItem.isEnabled = false
            self.settingsBarButtonItem.isAccessibilityElement = false
            self.noteView.isHidden = true
            self.navigationItem.title = NSLocalizedString("Info", comment: "").uppercased()            
        }
        if let user: User = UserManager.loggedUser() {
            self.nameLabel.text = user.name
            self.jobLabel.text = user.job
            self.emailTextView.text = user.emailAddress
            self.phoneNumberTextView.text = user.phoneNumber
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Actions
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Close.
    /// - parameters:
    ///   - sender: Sender.
    /// - throws:
    @IBAction func closeButton(_ sender: AnyObject) {
        LogApp.info(String(describing: MoreViewController.self) + LoggingConstants.SEPARATOR + "Close...")
        self.dismiss(animated: true, completion: {
            
        })
    }
    
    /// Pressed settings.
    /// - parameters:
    ///   - sender: Sender.
    /// - throws:
    @IBAction func pressedSettingsButton(_ sender: Any) {
        LogApp.info(String(describing: MoreViewController.self) + LoggingConstants.SEPARATOR + "Pressed settings button...")
        if let url = URL(string:UIApplication.openSettingsURLString) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Observer of notification
    ////////////////////////////////////////////////////////////////////////////////////////////
    
}

////////////////////////////////////////////////////////////////////////////////////////////
// MARK: UITableViewDataSource
////////////////////////////////////////////////////////////////////////////////////////////

extension MoreViewController {
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Configuring a Table View
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        if self.modalPresentationOn {
            return MoreViewController.MODAL_PRESENTATION_SECTIONS
        }
        return MoreViewController.TAB_PRESENTATION_SECTIONS
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == MoreViewController.DESCRIPTION_SECTION {
            return NSLocalizedString("Note", comment: "").uppercased()
        }
        return nil
    }
    
    override func tableView(_ tableView: UITableView,
                            titleForFooterInSection section: Int) -> String? {
        return nil
    }
    
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Inserting or Deleting Table Rows
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Customizing Section Names
    ////////////////////////////////////////////////////////////////////////////////////////////
    
}

////////////////////////////////////////////////////////////////////////////////////////////
// MARK: UITableViewDelegate
////////////////////////////////////////////////////////////////////////////////////////////

extension MoreViewController {
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Configuring Rows for the Table View
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    override func tableView(_ tableView: UITableView,
                            estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView,
                            heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView,
                            willDisplay cell: UITableViewCell,
                            forRowAt indexPath: IndexPath) {
    }
    
    override func tableView(_ tableView: UITableView,
                            didEndDisplaying cell: UITableViewCell,
                            forRowAt indexPath: IndexPath) {
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Managing Accessory Views
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Managing Selections
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    override func tableView(_ tableView: UITableView,
                            willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        return indexPath
    }
    
    override func tableView(_ tableView: UITableView,
                            didSelectRowAt indexPath: IndexPath) {
        
    }
    
    override func tableView(_ tableView: UITableView,
                            willDeselectRowAt indexPath: IndexPath) -> IndexPath? {
        return indexPath
    }
    
    override func tableView(_ tableView: UITableView,
                            didDeselectRowAt indexPath: IndexPath) {
        
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Modifying the Header and Footer of Sections
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    override func tableView(_ tableView: UITableView,
                            willDisplayHeaderView view: UIView,
                            forSection section: Int) {
        if section == MoreViewController.DESCRIPTION_SECTION {
            view.tintColor = UIColor.clear
            let header = view as! UITableViewHeaderFooterView
            header.textLabel?.textColor = UIColor.lightGray
        }
    }
    
    override func tableView(_ tableView: UITableView,
                            willDisplayFooterView view: UIView,
                            forSection section: Int) {
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Editing Table Rows
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Reordering Table Rows
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Tracking the Removal of Views
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Copying and Pasting Row Content
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Managing Table View Highlighting
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Managing Table View Focus
    ////////////////////////////////////////////////////////////////////////////////////////////
    
}

