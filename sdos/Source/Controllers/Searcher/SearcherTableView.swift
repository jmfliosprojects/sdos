//
//  SearcherTableView.swift
//  sdos
//
//  Created by Juan Miguel Fernández on 06/09/2019.
//  Copyright © 2019 Juan Miguel Fernández Lerena. All rights reserved.
//

import UIKit
import Logging

/// Constants for cell.
struct SearchView {
    
    static let HEIGHT_VIEW: CGFloat = 44
    
}

/// Table View for searcher.
class SearcherTableView: UITableView {
    
    var anyResultsLabel: UILabel
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Lifecycle
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Init.
    /// - parameters:
    ///   - aDecoder: Decoder.
    /// - throws:
    required init?(coder aDecoder: NSCoder) {
        self.anyResultsLabel = UILabel()
        self.anyResultsLabel.text = NSLocalizedString("Start looking for movies, don't miss any!", comment: "")
        self.anyResultsLabel.font = UIFont.preferredFont(forTextStyle: .title1)
        self.anyResultsLabel.textColor = UIColor.lightGray
        self.anyResultsLabel.textAlignment = NSTextAlignment.center
        self.anyResultsLabel.numberOfLines = 0
        self.anyResultsLabel.isHidden = true
        super.init(coder: aDecoder)
        adjustSizeOfLabels()
        addSubview(self.anyResultsLabel)
    }
    
    /// Reload data.
    /// - throws:
    override func reloadData() {
        
    }
    
    // Adjust the size so that the indicator is always in the middle of the screen.
    /// - throws:
    override func layoutSubviews() {
        super.layoutSubviews()
        self.adjustSizeOfLabels()
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Public methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    //// Update preferred font adjustment.
    /// - throws:
    func updatePreferredFontAdjustment() {
        LogApp.info(String(describing: SearcherTableView.self) + LoggingConstants.SEPARATOR + "Update preferred font adjustment...")
        self.anyResultsLabel.font = UIFont.preferredFont(forTextStyle: .title1)
    }
    
    /// Show any results.
    /// - throws:
    func showAnyResults() {
        LogApp.info(String(describing: SearcherTableView.self) + LoggingConstants.SEPARATOR + "Show any results...")
        self.anyResultsLabel.isHidden = false
        self.anyResultsLabel.alpha = 1
    }
    
    /// Hide any results.
    /// - throws:
    func hideAnyResults() {
        LogApp.info(String(describing: SearcherTableView.self) + LoggingConstants.SEPARATOR + "Hide any results...")
        self.anyResultsLabel.isHidden = true
        self.anyResultsLabel.alpha = 0
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Private methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Adjust size.
    /// - throws:
    fileprivate func adjustSizeOfLabels() {
        if UIDevice.current.orientation.isLandscape {
            self.anyResultsLabel.frame = CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.width - 500)
        } else {
            self.anyResultsLabel.frame = CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.width)
        }
    }

}
