//
//  SearchTableViewCell.swift
//  sdos
//
//  Created by Juan Miguel Fernández on 06/09/2019.
//  Copyright © 2019 Juan Miguel Fernández Lerena. All rights reserved.
//

import UIKit

/// Constants for cell.
struct SearchCell {
    
    static let identifier = "Search"
    
    static let HEIGHT_VIEW: CGFloat = 54
    
}

/// Cell for Search.
class SearchTableViewCell: UITableViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // Managing Cell Selection and Highlighting
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    override func setSelected(_ selected: Bool,
                              animated: Bool) {
        super.setSelected(selected, animated: animated)
        if (selected) {
            self.accessoryType = .none
        } else {
            self.accessoryType = .none
        }
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        if highlighted {
            self.accessoryType = .none
            self.containerView.backgroundColor = UIButton(type: .system).tintColor
            self.titleLabel.textColor = UIColor.white
        } else {
            self.accessoryType = .none
            self.containerView.backgroundColor = UIColor.clear
            self.titleLabel.textColor = UIButton(type: .system).tintColor
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // Reusing Cells
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Lifecycle
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
}
