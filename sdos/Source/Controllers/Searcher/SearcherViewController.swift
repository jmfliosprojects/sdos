//
//  SearcherViewController.swift
//  sdos
//
//  Created by Juan Miguel Fernández on 06/09/2019.
//  Copyright © 2019 Juan Miguel Fernández Lerena. All rights reserved.
//

import UIKit
import Logging
import CoreData

/// Controller of searcher view.
class SearcherViewController: BaseTabTableViewController {
    
    @IBOutlet weak var removeSearchesButton: UIButton! {
        didSet {
            self.removeSearchesButton.setTitle(NSLocalizedString("Clear searches", comment: ""), for: UIControl.State())
        }
    }
    
    private var context: NSManagedObjectContext!
    private var frc: NSFetchedResultsController = NSFetchedResultsController<Search>()
    private var searchController: UISearchController!
    private var textFieldInsideSearchBar:  UITextField?
    private var textFieldInsideSearchBarLabel: UILabel?
    private var resultsTableViewController: ResultsViewController!
    private var wasActive: Bool = false
    var infoLabel: UILabel = UILabel()
    var page: Int = 1
    var totalPages: Int = 1
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Navigation
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    override func prepare(for segue: UIStoryboardSegue,
                          sender: Any?) {
        if segue.identifier == "User" {
            let moreNavigationViewController = segue.destination as? UINavigationController
            let moreViewController: MoreViewController? = moreNavigationViewController?.visibleViewController as? MoreViewController
            moreViewController?.modalPresentationOn = true
        } else if let vc = segue.destination as? DetailViewController
            , segue.identifier == "Detail" {
            self.wasActive = true
            self.navigationItem.backBarButtonItem = UIBarButtonItem.init(title: NSLocalizedString("Search", comment: ""), style: UIBarButtonItem.Style.plain, target: nil, action: nil)
            if let movie = sender as? Film {
                vc.loadDetail(detail: movie)
            }
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Public methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Open detail.
    /// - parameters:
    ///   - detail: Detail.
    /// - throws:
    public func openDetail(detail: Film) {
        LogApp.info(String(describing: SearcherViewController.self) + LoggingConstants.SEPARATOR + "Open detail...")
        self.performSegue(withIdentifier: "Detail", sender: detail)
    }
    
    /// Perform more results.
    /// - throws:
    public func performMoreResults() {
        LogApp.info(String(describing: SearcherViewController.self) + LoggingConstants.SEPARATOR + "Perform more results...")
        if (self.page < self.totalPages + 1) {
            guard let query = self.searchController.searchBar.text, query.trimmingCharacters(in: .whitespacesAndNewlines) != "" else {
                
                return
            }
            let trimmedSearchString = query.trimmingCharacters(in: .whitespacesAndNewlines)
            self.page += 1
            ResultManager.performResultsRequests(trimmedSearchString, page: String(self.page), { (isReachability, success, errorMessage, numResults) in
                self.resultsTableViewController.hideSearching()
                if success {
                    if !(self.page < self.totalPages + 1) {
                        self.resultsTableViewController.moreResultsButton.isHidden = true
                    }
                } else {
                    LogApp.error(String(describing: SearcherViewController.self) + LoggingConstants.SEPARATOR + "Perform results requests: \(String(describing: errorMessage))")
                    self.resultsTableViewController.showError(errorMessage ?? "")
                }
            })
        } else {
            self.resultsTableViewController.moreResultsButton.isHidden = true
        }
    }
    
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Lifecycle
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Managing the View
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    override public func viewDidLoad() {
        LogApp.debug(String(describing: SearcherViewController.self) + LoggingConstants.SEPARATOR + "Did load...")
        super.viewDidLoad()
        AnalyticsManager.logContentViewForSearcher()
        self.addObservers()
        self.configureView()
        self.setupFetchedResultsController()
        self.setupResultsController()
        self.setupSearchController()
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Responding to View Events
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override public func viewDidAppear(_ animated: Bool) {
        LogApp.debug(String(describing: SearcherViewController.self) + LoggingConstants.SEPARATOR + "View did appear...")
        super.viewDidAppear(animated)
        self.restoreSearchController()
    }
    
    override public func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override public func viewDidDisappear(_ animated: Bool) {
        LogApp.debug(String(describing: SearcherViewController.self) + LoggingConstants.SEPARATOR + "View did disapper...")
        super.viewDidDisappear(animated)
        self.removeObservers()
    }
    
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.default
    }
    
    override func viewWillTransition(to size: CGSize,
                                     with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to:size, with: coordinator)
        
        coordinator.animate(alongsideTransition: { (UIViewControllerTransitionCoordinatorContext) -> Void in
            
            let orient = UIApplication.shared.statusBarOrientation
            
            (self.tableView as? SearcherTableView)?.layoutSubviews()
            
            if UIDevice.current.orientation.isLandscape {
                self.infoLabel.frame = CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.width - 300)
            } else {
                self.infoLabel.frame = CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.width + 100)
            }
            
            switch orient {
            case .portrait:
                
                break
            case .portraitUpsideDown:
                
                break
            case .landscapeLeft:
                
                break
            case .landscapeRight:
                
                break
            default:
                
                break
            }
            
        }, completion: { (UIViewControllerTransitionCoordinatorContext) -> Void in
            
        })
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Delegate methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: BaseAnyControllerDelegate
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Update preferred font adjustment.
    /// - throws:
    override func updatePreferredFontAdjustment() {
        LogApp.debug(String(describing: SearcherViewController.self) + LoggingConstants.SEPARATOR + "Update preferred font adjustment...")
        super.updatePreferredFontAdjustment()
        self.textFieldInsideSearchBar?.font = UIFont.preferredFont(forTextStyle: .body)
        self.textFieldInsideSearchBarLabel?.font = UIFont.preferredFont(forTextStyle: .body)
        self.infoLabel.font = UIFont.preferredFont(forTextStyle: .title1)
        (self.tableView as? SearcherTableView)?.updatePreferredFontAdjustment()
    }
    
    /// Perform reload remote.
    /// - throws:
    override func performReloadRemote() {
        LogApp.debug(String(describing: SearcherViewController.self) + LoggingConstants.SEPARATOR + "Perform reolad remote...")
        super.performReloadRemote()
        self.configureView()
    }
    
    /// Show indicator with not reachable.
    /// - throws:
    override func showIndicatorWithNotReachable() {
        LogApp.debug(String(describing: SearcherViewController.self) + LoggingConstants.SEPARATOR + "Show indicator with not reachable...")
        self.infoLabel.text = NSLocalizedString("Turn Off Airplane Mode or Use Wi-Fi to Access the Open Movie Database", comment: "")
        self.infoLabel.font = UIFont.preferredFont(forTextStyle: .title1)
        self.infoLabel.textColor = UIColor.lightGray
        self.infoLabel.textAlignment = NSTextAlignment.center
        self.infoLabel.numberOfLines = 0
        self.infoLabel.backgroundColor = UIColor.white
        self.infoLabel.alpha = 0
        self.infoLabel.isUserInteractionEnabled = true
        if UIDevice.current.orientation.isLandscape {
            self.infoLabel.frame = CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.width - 300)
        } else {
            self.infoLabel.frame = CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.width + 100)
        }
        self.view.addSubview(self.infoLabel)
        self.view.bringSubviewToFront(self.infoLabel)
        self.infoLabel.isHidden = false
        UIView.animate(withDuration: 1, animations: {
            self.infoLabel.alpha = 1
            self.hideTableViewState()
            self.tableView.beginUpdates()
            self.tableView.reloadData()
            self.tableView.endUpdates()
        })
    }
    
    /// Show indicator with is reachable.
    /// - throws:
    override func showIndicatorWithIsReachable() {
        LogApp.debug(String(describing: SearcherViewController.self) + LoggingConstants.SEPARATOR + "Show indicator with is reachable...")
        UIView.animate(withDuration: 1, animations: {
            self.infoLabel.alpha = 0
            self.infoLabel.removeFromSuperview()
            self.showTableViewState()
            self.tableView.beginUpdates()
            self.tableView.reloadData()
            self.tableView.endUpdates()
        })
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Private methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Add observers.
    /// - throws:
    private func addObservers() {
        LogApp.debug(String(describing: SearcherViewController.self) + LoggingConstants.SEPARATOR + "Add observers...")
        
    }
    
    /// Remove observers.
    /// - throws:
    private func removeObservers() {
        LogApp.debug(String(describing: SearcherViewController.self) + LoggingConstants.SEPARATOR + "Remove observers...")
        
    }
    
    /// Configure view.
    /// - throws:
    private func configureView() {
        LogApp.debug(String(describing: SearcherViewController.self) + LoggingConstants.SEPARATOR + "Configure view...")
        self.navigationItem.title = NSLocalizedString("Search", comment: "").uppercased()
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    /// Setup search controller.
    /// - throws:
    private func setupFetchedResultsController() {
        LogApp.debug(String(describing: SearcherViewController.self) + LoggingConstants.SEPARATOR + "Setup fetched results controller...")
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedObjectContext = appDelegate.managedObjectContext
        self.context = managedObjectContext
        self.frc = getFRC()
        self.fetch(frcToFetch: self.frc)
    }
    
    /// Fetch.
    /// - parameters:
    ///   - frcToFetch: frcToFetch.
    /// - throws:
    private func fetch(frcToFetch: NSFetchedResultsController<Search>) {
        LogApp.debug(String(describing: SearcherViewController.self) + LoggingConstants.SEPARATOR + "Fetch...")
        do {
            try frcToFetch.performFetch()
        } catch {
            return
        }
    }
    
    /// Fetch request.
    /// - throws:
    /// - return: Fetch request.
    private func fetchRequest() -> NSFetchRequest<Search> {
        LogApp.debug(String(describing: SearcherViewController.self) + LoggingConstants.SEPARATOR + "Fetch request...")
        let fetchRequest = NSFetchRequest<Search>(entityName: SearchEntity.name)
        let sortDescriptor1 = NSSortDescriptor.init(key: SearchAttribute.created, ascending: true)
        let sortDescriptor2 = NSSortDescriptor.init(key: SearchAttribute.identifier, ascending: false)
        fetchRequest.sortDescriptors = [sortDescriptor1, sortDescriptor2]
        let predicate1 = NSPredicate(format: SearchAttribute.identifier + " > %d", 0)
        let compound =  NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: [predicate1])
        fetchRequest.predicate = compound
        fetchRequest.fetchLimit = 5
        return fetchRequest
    }
    
    /// Get FRC.
    /// - throws:
    /// - return: FRC
    private func getFRC() -> NSFetchedResultsController<Search> {
        LogApp.debug(String(describing: SearcherViewController.self) + LoggingConstants.SEPARATOR + "Get FRC...")
        frc = NSFetchedResultsController(
            fetchRequest: fetchRequest(),
            managedObjectContext: self.context,
            sectionNameKeyPath: #keyPath(Search.committeeNameInitial),
            cacheName: nil)
        frc.delegate = self
        return frc
    }
    
    /// Setup results controller.
    /// - throws:
    private func setupResultsController() {
        LogApp.debug(String(describing: SearcherViewController.self) + LoggingConstants.SEPARATOR + "Setup results controller...")
        self.resultsTableViewController = UIStoryboard(name: "App", bundle: nil).instantiateViewController(withIdentifier: "ResultsViewController") as? ResultsViewController
        self.resultsTableViewController.searcherViewController = self
    }
    
    /// Setup fetched results controller.
    /// - throws:
    private func setupSearchController() {
        LogApp.debug(String(describing: SearcherViewController.self) + LoggingConstants.SEPARATOR + "Setup search controller...")
        self.searchController = UISearchController(searchResultsController: self.resultsTableViewController)
        self.searchController.delegate = self
        self.searchController.searchResultsUpdater = self
        self.searchController.searchBar.delegate = self
        self.searchController.searchBar.autocapitalizationType = .none
        self.searchController.searchBar.sizeToFit()
        self.searchController.searchBar.placeholder = NSLocalizedString("Open Movie Database", comment: "")
        self.textFieldInsideSearchBar = self.searchController.searchBar.value(forKey: "searchField") as? UITextField
        self.textFieldInsideSearchBar?.font = UIFont.preferredFont(forTextStyle: .body)
        self.textFieldInsideSearchBarLabel = self.textFieldInsideSearchBar!.value(forKey: "placeholderLabel") as? UILabel
        self.textFieldInsideSearchBarLabel?.font = UIFont.preferredFont(forTextStyle: .body)
        self.textFieldInsideSearchBar?.returnKeyType = UIReturnKeyType.done
        self.searchController.searchBar.showsCancelButton = false
        self.searchController.isActive = false
        self.searchController.definesPresentationContext = true
        self.searchController.dimsBackgroundDuringPresentation = true
        self.searchController.hidesNavigationBarDuringPresentation = true
        self.navigationItem.searchController = self.searchController
        self.definesPresentationContext = true
    }
    
    /// Restore search controller.
    /// - throws:
    private func restoreSearchController() {
        LogApp.debug(String(describing: SearcherViewController.self) + LoggingConstants.SEPARATOR + "Restore search controller...")
        if self.wasActive {
            searchController.searchBar.becomeFirstResponder()
        }
    }
    
    /// Configure empty table view.
    /// - throws:
    private func configureEmptyTableView() {
        LogApp.debug(String(describing: SearcherViewController.self) + LoggingConstants.SEPARATOR + "Configure empty table view...")
        UIView.animate(withDuration: 1, animations: {
            (self.tableView as? SearcherTableView)?.showAnyResults()
            self.removeSearchesButton.isHidden = true
        })
    }
    
    /// Configure not empty table view.
    /// - throws:
    private func configureNotEmptyTableView() {
        LogApp.debug(String(describing: SearcherViewController.self) + LoggingConstants.SEPARATOR + "Configure not empty table view...")
        UIView.animate(withDuration: 1, animations: {
            (self.tableView as? SearcherTableView)?.hideAnyResults()
            self.removeSearchesButton.isHidden = false
        })
    }
    
    /// Hide table view state.
    /// - throws:
    private func hideTableViewState() {
        LogApp.debug(String(describing: SearcherViewController.self) + LoggingConstants.SEPARATOR + "Hide table view state...")
        if SearchManager.performAllSearches().count == 0 {
            self.removeSearchesButton.isHidden = true
            UIView.animate(withDuration: 1, animations: {
                (self.tableView as? SearcherTableView)?.hideAnyResults()
            })
        } else {
            self.removeSearchesButton.isHidden = true
        }
    }
    
    /// Show table view state.
    /// - throws:
    private func showTableViewState() {
        LogApp.debug(String(describing: SearcherViewController.self) + LoggingConstants.SEPARATOR + "Show table view state...")
        if SearchManager.performAllSearches().count == 0 {
            UIView.animate(withDuration: 1, animations: {
                (self.tableView as? SearcherTableView)?.showAnyResults()
                self.removeSearchesButton.isHidden = true
            })
        } else {
            UIView.animate(withDuration: 1, animations: {
                (self.tableView as? SearcherTableView)?.hideAnyResults()
                self.removeSearchesButton.isHidden = false
            })
        }
    }
    
    /// Configure cell.
    /// - parameters:
    ///   - cell: Cell.
    /// - throws:
    private func configureCell(cell: SearchTableViewCell,
                               atIndexPath indexPath: NSIndexPath) {
        LogApp.debug(String(describing: SearcherViewController.self) + LoggingConstants.SEPARATOR + "Configure cell...")
        let object = self.frc.object(at: indexPath as IndexPath)
        var title = ""
        if let textObject = object.text {
            title = textObject
        }
        cell.titleLabel.text = title
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Actions
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Pressed remove searches button.
    /// - parameters:
    ///   - sender: Sender.
    /// - throws:
    @IBAction func pressedRemoveSearchesButton(_ sender: Any) {
        LogApp.info(String(describing: SearcherViewController.self) + LoggingConstants.SEPARATOR + "Pressed remove searches button...")
        let alertController: BaseAlertViewController = BaseAlertViewController(title: NSLocalizedString("Are you sure?", comment:""),
                                                                               message: NSLocalizedString("Then press OK to delete your searches.", comment:""),
                                                                               preferredStyle: UIAlertController.Style.alert)
        
        let laterAction = UIAlertAction(title: NSLocalizedString("Cancel", comment:""),
                                        style: .default) { (_) in
                                            
        }
        laterAction.isEnabled = true
        let nowAction = UIAlertAction(title: NSLocalizedString("OK", comment:""),
                                      style: .default) { (_) in
                                        SearchManager.performRemoveAllSearches()
        }
        nowAction.isEnabled = true
        alertController.addAction(laterAction)
        alertController.addAction(nowAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Observer of notification
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Release search.
    /// - parameters:
    ///   - sender: Sender.
    /// - throws:
    @objc func releaseSearch(_ searchBar: UISearchBar) {
        LogApp.info(String(describing: SearcherViewController.self) + LoggingConstants.SEPARATOR + "Release search...")
        guard let query = searchBar.text, query.trimmingCharacters(in: .whitespacesAndNewlines) != "" else {
            
            return
        }
        let trimmedSearchString = query.trimmingCharacters(in: .whitespacesAndNewlines)
        if trimmedSearchString.count > ValidationConfigurationManager.minCharacterForSearcher() {
            ResultManager.performCleanResults()
                ResultManager.performResultsRequests(trimmedSearchString, page: String(self.page), { (isReachability, success, errorMessage, numResults) in
                    self.resultsTableViewController.hideSearching()
                    if success {
                        let result: Double = Double(numResults ?? 1) / 10
                        self.totalPages = Int(round(result))
                    } else {
                        LogApp.error(String(describing: SearcherViewController.self) + LoggingConstants.SEPARATOR + "Perform results requests: \(String(describing: errorMessage))")
                        self.resultsTableViewController.showError(errorMessage ?? "")
                    }
                })
        } else {
            ResultManager.performCleanResults()
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// MARK: UITableViewDataSource
////////////////////////////////////////////////////////////////////////////////////////////

extension SearcherViewController {
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Configuring a Table View
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        if let sections = self.frc.sections {
            if sections.count == 0 {
                self.configureEmptyTableView()
            } else {
                let currentSection = sections[0]
                let numberOfObjects = currentSection.numberOfObjects
                if numberOfObjects == 0 {
                    self.configureEmptyTableView()
                } else {
                    self.configureNotEmptyTableView()
                }
            }
            return sections.count
        } else {
            self.configureEmptyTableView()
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView,
                            numberOfRowsInSection section: Int) -> Int {
        if let sections = self.frc.sections {
            let currentSection = sections[section]
            return currentSection.numberOfObjects
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView,
                            cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SearchTableViewCell = tableView.dequeueReusableCell(withIdentifier: SearchCell.identifier, for: indexPath) as! SearchTableViewCell
        cell.prepareForReuse()
        self.configureCell(cell: cell, atIndexPath: indexPath as NSIndexPath)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if let sections = self.frc.sections {
            let currentSection = sections[section]
            return currentSection.name
        }
        return nil
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Inserting or Deleting Table Rows
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Customizing Section Names
    ////////////////////////////////////////////////////////////////////////////////////////////
    
}


////////////////////////////////////////////////////////////////////////////////////////////
// MARK: UITableViewDelegate
////////////////////////////////////////////////////////////////////////////////////////////

extension SearcherViewController {

    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Configuring Rows for the Table View
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    override func tableView(_ tableView: UITableView,
                            heightForRowAt indexPath: IndexPath) -> CGFloat {
        return SearchCell.HEIGHT_VIEW
    }
    
    override func tableView(_ tableView: UITableView,
                            estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView,
                            willDisplay cell: UITableViewCell,
                            forRowAt indexPath: IndexPath) {
        _ = self.frc.object(at: indexPath as IndexPath)
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Managing Accessory Views
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Managing Selections
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    override func tableView(_ tableView: UITableView,
                            willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        return indexPath
    }
    
    override func tableView(_ tableView: UITableView,
                            didSelectRowAt indexPath: IndexPath) {
        let object = self.frc.object(at: indexPath as IndexPath)
        let cell = tableView.cellForRow(at: indexPath)
        cell?.setSelected(false, animated: true)
        self.tableView.deselectRow(at: indexPath, animated: true)
        self.textFieldInsideSearchBar?.becomeFirstResponder()
        self.searchController.searchBar.text = object.text ?? ""
    }
    
    override func tableView(_ tableView: UITableView,
                            willDeselectRowAt indexPath: IndexPath) -> IndexPath? {
        return indexPath
    }
    
    override func tableView(_ tableView: UITableView,
                            didDeselectRowAt indexPath: IndexPath) {
        _ = self.frc.object(at: indexPath as IndexPath)
        
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Modifying the Header and Footer of Sections
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    override func tableView(_ tableView: UITableView,
                            heightForHeaderInSection section: Int) -> CGFloat {
        return SearchView.HEIGHT_VIEW
    }
    
    override func tableView(_ tableView: UITableView,
                            estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return SearchView.HEIGHT_VIEW
    }
    
    override func tableView(_ tableView: UITableView,
                            willDisplayHeaderView view: UIView,
                            forSection section: Int) {
        view.tintColor = UIColor.white
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = UIColor.darkText
        header.textLabel?.font = UIFont.preferredFont(forTextStyle: .title2)
        header.backgroundColor = UIColor.white
    }
    
    override func tableView(_ tableView: UITableView,
                            willDisplayFooterView view: UIView,
                            forSection section: Int) {
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Editing Table Rows
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Reordering Table Rows
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Tracking the Removal of Views
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Copying and Pasting Row Content
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Managing Table View Highlighting
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    override func tableView(_ tableView: UITableView,
                            shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView,
                            didHighlightRowAt indexPath: IndexPath) {
        let cell  = tableView.cellForRow(at: indexPath)
        cell?.setHighlighted(true, animated: true)
    }
    
    override func tableView(_ tableView: UITableView,
                            didUnhighlightRowAt indexPath: IndexPath) {
        let cell  = tableView.cellForRow(at: indexPath)
        cell?.setHighlighted(false, animated: true)
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Managing Table View Focus
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    override func tableView(_ tableView: UITableView,
                            canFocusRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
}

////////////////////////////////////////////////////////////////////////////////////////////
// MARK: NSFetchedResultsControllerDelegate
////////////////////////////////////////////////////////////////////////////////////////////

extension SearcherViewController: NSFetchedResultsControllerDelegate {
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Responding to Changes
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>,
                    didChange anObject: Any,
                    at indexPath: IndexPath?,
                    for type: NSFetchedResultsChangeType,
                    newIndexPath: IndexPath?) {
        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                self.tableView.insertRows(at: [indexPath as IndexPath], with: .fade)
            }
            break;
        case .delete:
            if let indexPath = indexPath {
                self.tableView.deleteRows(at: [indexPath as IndexPath], with: .fade)
            }
            break;
        case .update:
            if let indexPath = indexPath {
                self.tableView.reloadRows(at: [indexPath as IndexPath], with: .fade)
            }
            break;
        case .move:
            if let indexPath = indexPath {
                self.tableView.deleteRows(at: [indexPath as IndexPath], with: .fade)
            }
            if let newIndexPath = newIndexPath {
                self.tableView.insertRows(at: [newIndexPath as IndexPath], with: .fade)
            }
            break;
        @unknown default:
            LogApp.error(String(describing: SearcherViewController.self) + LoggingConstants.SEPARATOR + "Error responding to Changes...")
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>,
                    didChange sectionInfo: NSFetchedResultsSectionInfo,
                    atSectionIndex sectionIndex: Int,
                    for type: NSFetchedResultsChangeType) {
        switch type {
        case .insert:
            self.tableView.insertSections(NSIndexSet(index: sectionIndex) as IndexSet, with: .fade)
        case .delete:
            self.tableView.deleteSections(NSIndexSet(index: sectionIndex) as IndexSet, with: .fade)
        case .move:
            break
        case .update:
            break
        @unknown default:
            LogApp.error(String(describing: SearcherViewController.self) + LoggingConstants.SEPARATOR + "Error responding to Changes...")
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.endUpdates()
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Customizing Section Names
    ////////////////////////////////////////////////////////////////////////////////////////////
    
}

////////////////////////////////////////////////////////////////////////////////////////////
// MARK: UISearchControllerDelegate
////////////////////////////////////////////////////////////////////////////////////////////

extension SearcherViewController: UISearchControllerDelegate {
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Presenting and Dismissing the Search Controller
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    func didDismissSearchController(_ searchController: UISearchController) {
        LogApp.debug(String(describing: SearcherViewController.self) + LoggingConstants.SEPARATOR + "didDismissSearchController...")
        if searchController.isActive {
            
        } else {
            self.tableView.reloadSectionIndexTitles()
        }
    }
    
    func didPresentSearchController(_ searchController: UISearchController) {
        LogApp.debug(String(describing: SearcherViewController.self) + LoggingConstants.SEPARATOR + "didPresentSearchController...")
        searchController.searchBar.showsCancelButton = true
        searchController.searchBar.becomeFirstResponder()
    }
    
    func presentSearchController(_ searchController: UISearchController) {
        LogApp.debug(String(describing: SearcherViewController.self) + LoggingConstants.SEPARATOR + "presentSearchController...")
        if searchController.isActive {
            
        } else {
            self.tableView.reloadSectionIndexTitles()
        }
    }
    
    func willDismissSearchController(_ searchController: UISearchController) {
        LogApp.debug(String(describing: SearcherViewController.self) + LoggingConstants.SEPARATOR + "willDismissSearchController...")
        if searchController.isActive {
            
        } else {
            self.tableView.reloadSectionIndexTitles()
        }
    }
    
    func willPresentSearchController(_ searchController: UISearchController) {
        LogApp.debug(String(describing: SearcherViewController.self) + LoggingConstants.SEPARATOR + "willPresentSearchController...")
        if searchController.isActive {
            
        } else {
            self.tableView.reloadSectionIndexTitles()
        }
        (self.resultsTableViewController.tableView as! ResultsTableView).reloadData()
    }
    
}

////////////////////////////////////////////////////////////////////////////////////////////
// MARK: UISearchResultsUpdating
////////////////////////////////////////////////////////////////////////////////////////////

extension SearcherViewController: UISearchResultsUpdating {
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Updating the Search Bar
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    func updateSearchResults(for searchController: UISearchController) {
        LogApp.debug(String(describing: SearcherViewController.self) + LoggingConstants.SEPARATOR + "updateSearchResults...")
        if self.wasActive {
            self.wasActive = false
        } else {
            guard let query = searchController.searchBar.text, query.trimmingCharacters(in: .whitespacesAndNewlines) != "" else {
                return
            }
            let trimmedSearchString = query.trimmingCharacters(in: .whitespacesAndNewlines)
            if trimmedSearchString.count > ValidationConfigurationManager.minCharacterForSearcher() {
                ResultManager.performCleanResults()
                UIView.animate(withDuration: 1, animations: {
                    (self.resultsTableViewController.tableView as! ResultsTableView).hideAnyResults()
                    (self.resultsTableViewController.tableView as! ResultsTableView).hideErrorResults()
                    (self.resultsTableViewController.tableView as! ResultsTableView).hideMinSearch()
                })
                self.resultsTableViewController.showSearching()
                NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.releaseSearch(_:)), object: searchController.searchBar)
                perform(#selector(self.releaseSearch(_:)), with: searchController.searchBar, afterDelay: 0.75)
            } else {
                ResultManager.performCleanResults()
                self.resultsTableViewController.hideSearching()
                UIView.animate(withDuration: 1, animations: {
                    (self.resultsTableViewController.tableView as! ResultsTableView).hideAnyResults()
                    (self.resultsTableViewController.tableView as! ResultsTableView).hideErrorResults()
                    (self.resultsTableViewController.tableView as! ResultsTableView).showMinSearch()
                    self.resultsTableViewController.moreResultsButton.isHidden = true
                })
            }
        }
    }
    
}

////////////////////////////////////////////////////////////////////////////////////////////
// MARK: UISearchBarDelegate
////////////////////////////////////////////////////////////////////////////////////////////

extension SearcherViewController: UISearchBarDelegate {
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Editing Text
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    func searchBar(_ searchBar: UISearchBar,
                   textDidChange searchText: String) {
        LogApp.debug(String(describing: SearcherViewController.self) + LoggingConstants.SEPARATOR + "searchBar:textDidChange:...")
        if !searchText.isEmpty {
            self.tableView.reloadData()
        } else {
            ResultManager.performCleanResults()
        }
    }
    
    func searchBar(_ searchBar: UISearchBar,
                   shouldChangeTextIn range: NSRange,
                   replacementText text: String) -> Bool {
        LogApp.debug(String(describing: SearcherViewController.self) + LoggingConstants.SEPARATOR + "shouldChangeTextIn:replacementText:...")
        return true
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        LogApp.debug(String(describing: SearcherViewController.self) + LoggingConstants.SEPARATOR + "searchBarShouldBeginEditing...")
        return true
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        LogApp.debug(String(describing: SearcherViewController.self) + LoggingConstants.SEPARATOR + "searchBarTextDidBeginEditing...")
        
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        LogApp.debug(String(describing: SearcherViewController.self) + LoggingConstants.SEPARATOR + "searchBarShouldEndEditing...")
        return true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        LogApp.debug(String(describing: SearcherViewController.self) + LoggingConstants.SEPARATOR + "searchBarTextDidEndEditing...")
        
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Clicking Buttons
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    func searchBarBookmarkButtonClicked(_ searchBar: UISearchBar) {
        LogApp.debug(String(describing: SearcherViewController.self) + LoggingConstants.SEPARATOR + "searchBarBookmarkButtonClicked...")
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        LogApp.debug(String(describing: SearcherViewController.self) + LoggingConstants.SEPARATOR + "searchBarCancelButtonClicked...")
        ResultManager.performCleanResults()
        UIView.animate(withDuration: 1, animations: {
            (self.resultsTableViewController.tableView as! ResultsTableView).hideAnyResults()
            (self.resultsTableViewController.tableView as! ResultsTableView).hideErrorResults()
            (self.resultsTableViewController.tableView as! ResultsTableView).hideMinSearch()
        })
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        LogApp.debug(String(describing: SearcherViewController.self) + LoggingConstants.SEPARATOR + "searchBarSearchButtonClicked...")
        let searchText = searchController.searchBar.text!
        let trimmedSearchString = searchText.trimmingCharacters(in: .whitespacesAndNewlines)
        if !trimmedSearchString.isEmpty {
            let createdDate: Date = Date()
            SearchManager.performNewSearch(text: trimmedSearchString, createdDate: createdDate)
        }
        self.tableView.reloadData()
    }
    
    func searchBarResultsListButtonClicked(_ searchBar: UISearchBar) {
        LogApp.debug(String(describing: SearcherViewController.self) + LoggingConstants.SEPARATOR + "searchBarResultsListButtonClicked...")
        
        
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Scope Button
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    func searchBar(_ searchBar: UISearchBar,
                   selectedScopeButtonIndexDidChange selectedScope: Int) {
        LogApp.debug(String(describing: SearcherViewController.self) + LoggingConstants.SEPARATOR + "selectedScopeButtonIndexDidChange...")
        
    }
    
}
