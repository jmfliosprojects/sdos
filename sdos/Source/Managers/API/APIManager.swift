//
//  APIManager.swift
//  sdos
//
//  Created by Juan Miguel Fernández on 07/09/2019.
//  Copyright © 2019 Juan Miguel Fernández Lerena. All rights reserved.
//

import Foundation
import Logging
import Connection

/// Constants for manager.
struct APIParameters {
    
    static let ACCEPT_LANGUAGE_HEADER = "Accept-Language"
    static let UUID_HEADER = "UUID"
    
    static let TEXT_QUERY = "s"
    static let PAGE_QUERY = "page"
    static let IMDBID_QUERY = "i"
    static let API_KEY_QUERY = "apikey"
    
}

/// Manager of API.
class APIManager: NSObject {
    
    /// Shared instance.
    static let sharedInstance = APIManager()
    
    /// Init.
    private override init() {
        
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Public methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Obtain API Headers.
    /// - returns: API Domain.
    static func apiHeadersAuthenticated() -> [String : String] {
        LogApp.info(String(describing: APIManager.self) + LoggingConstants.SEPARATOR + "API headers for authenticated...")
        let language: String = NSLocale.preferredLanguages[0] as String? ?? ""
        let uuid: String = ConfigurationManager.appUserDefaultsForSettingsUUIDDevice() ?? ""
        let headers: [String : String] = [
            APIParameters.ACCEPT_LANGUAGE_HEADER: language,
            APIParameters.UUID_HEADER: uuid
        ]
        return headers
    }

    /// Obtain app error.
    /// - parameters:
    ///   - apiError: API error.
    /// - throws:
    /// - returns: Error of app.
    static func errorApp(apiError: NSError?) -> NSError? {
        if let codeError = apiError?.code {
            var error: NSError
            switch(codeError) {
            case RestAPIError.CODE_8999_INVALID_URL:
                var dict = [String: AnyObject]()
                dict[NSLocalizedDescriptionKey] = NSLocalizedString("Unexpected error", comment: "") as AnyObject?
                dict[NSLocalizedFailureReasonErrorKey] = apiError?.localizedFailureReason as AnyObject?
                dict[NSUnderlyingErrorKey] = apiError! as NSError
                error = NSError(domain: ErrorApp.EXCEPTION_BUSINESS_DOMAIN, code: ErrorApp.CODE_100_UNEXPECTED, userInfo: dict)
                break
            case RestAPIError.CODE_8998_PARAMETER_ENCODING_FAILED:
                var dict = [String: AnyObject]()
                dict[NSLocalizedDescriptionKey] = NSLocalizedString("Unexpected error", comment: "") as AnyObject?
                dict[NSLocalizedFailureReasonErrorKey] = apiError?.localizedFailureReason as AnyObject?
                dict[NSUnderlyingErrorKey] = apiError! as NSError
                error = NSError(domain: ErrorApp.EXCEPTION_BUSINESS_DOMAIN, code: ErrorApp.CODE_100_UNEXPECTED, userInfo: dict)
                break
            case RestAPIError.CODE_8997_MULTIPART_ENCODING_FAILED:
                var dict = [String: AnyObject]()
                dict[NSLocalizedDescriptionKey] = NSLocalizedString("Unexpected error", comment: "") as AnyObject?
                dict[NSLocalizedFailureReasonErrorKey] = apiError?.localizedFailureReason as AnyObject?
                dict[NSUnderlyingErrorKey] = apiError! as NSError
                error = NSError(domain: ErrorApp.EXCEPTION_BUSINESS_DOMAIN, code: ErrorApp.CODE_100_UNEXPECTED, userInfo: dict)
                break
            case RestAPIError.CODE_8996_DOWNLOADED_FILE_COULD_NOT_BE_READ:
                var dict = [String: AnyObject]()
                dict[NSLocalizedDescriptionKey] = NSLocalizedString("Unexpected error", comment: "") as AnyObject?
                dict[NSLocalizedFailureReasonErrorKey] = apiError?.localizedFailureReason as AnyObject?
                dict[NSUnderlyingErrorKey] = apiError! as NSError
                error = NSError(domain: ErrorApp.EXCEPTION_BUSINESS_DOMAIN, code: ErrorApp.CODE_100_UNEXPECTED, userInfo: dict)
                break
            case RestAPIError.CODE_8995_MISSING_CONTENT_TYPE:
                var dict = [String: AnyObject]()
                dict[NSLocalizedDescriptionKey] = NSLocalizedString("Unexpected error", comment: "") as AnyObject?
                dict[NSLocalizedFailureReasonErrorKey] = apiError?.localizedFailureReason as AnyObject?
                dict[NSUnderlyingErrorKey] = apiError! as NSError
                error = NSError(domain: ErrorApp.EXCEPTION_BUSINESS_DOMAIN, code: ErrorApp.CODE_100_UNEXPECTED, userInfo: dict)
                break
            case RestAPIError.CODE_8994_UNACCEPTABLE_CONTENT_TYPE:
                var dict = [String: AnyObject]()
                dict[NSLocalizedDescriptionKey] = NSLocalizedString("Unexpected error", comment: "") as AnyObject?
                dict[NSLocalizedFailureReasonErrorKey] = apiError?.localizedFailureReason as AnyObject?
                dict[NSUnderlyingErrorKey] = apiError! as NSError
                error = NSError(domain: ErrorApp.EXCEPTION_BUSINESS_DOMAIN, code: ErrorApp.CODE_100_UNEXPECTED, userInfo: dict)
                break
            case RestAPIError.CODE_8993_UNACCEPTABLE_STATUS_CODE:
                var dict = [String: AnyObject]()
                dict[NSLocalizedDescriptionKey] = NSLocalizedString("Unexpected error", comment: "") as AnyObject?
                dict[NSLocalizedFailureReasonErrorKey] = apiError?.localizedFailureReason as AnyObject?
                dict[NSUnderlyingErrorKey] = apiError! as NSError
                error = NSError(domain: ErrorApp.EXCEPTION_BUSINESS_DOMAIN, code: ErrorApp.CODE_100_UNEXPECTED, userInfo: dict)
                break
            case RestAPIError.CODE_8992_RESPONSE_SERIALIZATION_FAILED_CODE:
                var dict = [String: AnyObject]()
                dict[NSLocalizedDescriptionKey] = NSLocalizedString("Unexpected error", comment: "") as AnyObject?
                dict[NSLocalizedFailureReasonErrorKey] = apiError?.localizedFailureReason as AnyObject?
                dict[NSUnderlyingErrorKey] = apiError! as NSError
                error = NSError(domain: ErrorApp.EXCEPTION_BUSINESS_DOMAIN, code: ErrorApp.CODE_100_UNEXPECTED, userInfo: dict)
                break
            case RestAPIError.CODE_8990_UNDEFINED:
                var dict = [String: AnyObject]()
                dict[NSLocalizedDescriptionKey] = NSLocalizedString("Unexpected error", comment: "") as AnyObject?
                dict[NSLocalizedFailureReasonErrorKey] = apiError?.localizedFailureReason as AnyObject?
                dict[NSUnderlyingErrorKey] = apiError! as NSError
                error = NSError(domain: ErrorApp.EXCEPTION_BUSINESS_DOMAIN, code: ErrorApp.CODE_100_UNEXPECTED, userInfo: dict)
                break
            case RestAPIError.CODE_8991_URL_ERROR_CODE:
                var dict = [String: AnyObject]()
                dict[NSLocalizedDescriptionKey] = NSLocalizedString("Unexpected error", comment: "") as AnyObject?
                dict[NSLocalizedFailureReasonErrorKey] = apiError?.localizedFailureReason as AnyObject?
                dict[NSUnderlyingErrorKey] = apiError! as NSError
                error = NSError(domain: ErrorApp.EXCEPTION_BUSINESS_DOMAIN, code: ErrorApp.CODE_100_UNEXPECTED, userInfo: dict)
                break
            case RestAPIError.CODE_400_BAD_REQUEST:
                var dict = [String: AnyObject]()
                dict[NSLocalizedDescriptionKey] = NSLocalizedString("Wrong request", comment: "") as AnyObject?
                dict[NSLocalizedFailureReasonErrorKey] = apiError?.localizedFailureReason as AnyObject?
                dict[NSUnderlyingErrorKey] = apiError! as NSError
                error = NSError(domain: ErrorApp.EXCEPTION_BUSINESS_DOMAIN, code: ErrorApp.CODE_101_BAD_REQUEST, userInfo: dict)
                break
            case RestAPIError.CODE_401_UNAUTHORIZED:
                var dict = [String: AnyObject]()
                dict[NSLocalizedDescriptionKey] = NSLocalizedString("Authentication failure", comment: "") as AnyObject?
                dict[NSLocalizedFailureReasonErrorKey] = apiError?.localizedFailureReason as AnyObject?
                dict[NSUnderlyingErrorKey] = apiError! as NSError
                error = NSError(domain: ErrorApp.EXCEPTION_BUSINESS_DOMAIN, code: ErrorApp.CODE_102_UNAUTHORIZED, userInfo: dict)
                break
            case RestAPIError.CODE_403_FORBIDDEN:
                var dict = [String: AnyObject]()
                dict[NSLocalizedDescriptionKey] = NSLocalizedString("Restricted request", comment: "") as AnyObject?
                dict[NSLocalizedFailureReasonErrorKey] = apiError?.localizedFailureReason as AnyObject?
                dict[NSUnderlyingErrorKey] = apiError! as NSError
                error = NSError(domain: ErrorApp.EXCEPTION_BUSINESS_DOMAIN, code: ErrorApp.CODE_103_FORBIDDEN, userInfo: dict)
                break
            case RestAPIError.CODE_404_NOT_FOUND:
                var dict = [String: AnyObject]()
                dict[NSLocalizedDescriptionKey] = NSLocalizedString("Server cant find a valid match request", comment: "") as AnyObject?
                dict[NSLocalizedFailureReasonErrorKey] = apiError?.localizedFailureReason as AnyObject?
                dict[NSUnderlyingErrorKey] = apiError! as NSError
                error = NSError(domain: ErrorApp.EXCEPTION_BUSINESS_DOMAIN, code: ErrorApp.CODE_104_NOT_FOUND, userInfo: dict)
                break
            case RestAPIError.CODE_500_INTERNAL_ERROR:
                var dict = [String: AnyObject]()
                dict[NSLocalizedDescriptionKey] = NSLocalizedString("Internal server error", comment: "") as AnyObject?
                dict[NSLocalizedFailureReasonErrorKey] = apiError?.localizedFailureReason as AnyObject?
                dict[NSUnderlyingErrorKey] = apiError! as NSError
                error = NSError(domain: ErrorApp.EXCEPTION_BUSINESS_DOMAIN, code: ErrorApp.CODE_105_INTERNAL_ERROR, userInfo: dict)
                break
            case RestAPIError.CODE_503_NOT_AVAILABLE:
                var dict = [String: AnyObject]()
                dict[NSLocalizedDescriptionKey] = NSLocalizedString("Non available server", comment: "") as AnyObject?
                dict[NSLocalizedFailureReasonErrorKey] = apiError?.localizedFailureReason as AnyObject?
                dict[NSUnderlyingErrorKey] = apiError! as NSError
                error = NSError(domain: ErrorApp.EXCEPTION_BUSINESS_DOMAIN, code: ErrorApp.CODE_106_NOT_AVAILABLE, userInfo: dict)
                break
            default:
                var dict = [String: AnyObject]()
                dict[NSLocalizedDescriptionKey] = NSLocalizedString("Unexpected error", comment: "") as AnyObject?
                dict[NSLocalizedFailureReasonErrorKey] = apiError?.localizedFailureReason as AnyObject?
                dict[NSUnderlyingErrorKey] = apiError! as NSError
                error = NSError(domain: ErrorApp.EXCEPTION_BUSINESS_DOMAIN, code: ErrorApp.CODE_100_UNEXPECTED, userInfo: dict)
                break
            }
            return error
        }
        return apiError
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Private methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
}
