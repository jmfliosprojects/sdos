//
//  CrashManager.swift
//  sdos
//
//  Created by Juan Miguel Fernández on 05/09/2019.
//  Copyright © 2019 Juan Miguel Fernández Lerena. All rights reserved.
//

import Foundation
import Fabric
import Crashlytics
import Logging

/// Manager of Crash.
class CrashManager: NSObject {
    
    /// Shared instance.
    static let sharedInstance = CrashManager()
    
    /// Init.
    private override init() {
        
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Public methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Setting Crashlytics.
    /// - throws:
    static func settingCrash() {
        LogApp.info(String(describing: CrashManager.self) + LoggingConstants.SEPARATOR + "Setting crash...")
        Fabric.with([Crashlytics.self])
    }
    
    /// User information to identify it.
    /// - parameters:
    ///   - userEmail: User email.
    ///   - userIdentifier: User identifier.
    ///   - userName: User name.
    /// - throws:
    static func logUser(userEmail: String,
                        userIdentifier: String,
                        userName: String) {
        LogApp.info(String(describing: CrashManager.self) + LoggingConstants.SEPARATOR + "Crash for user...")
        Crashlytics.sharedInstance().setUserEmail(userEmail)
        Crashlytics.sharedInstance().setUserIdentifier(userIdentifier)
        Crashlytics.sharedInstance().setUserName(userName)
        LogApp.debug(String(describing: CrashManager.self) + LoggingConstants.SEPARATOR + userEmail)
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Private methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
}

