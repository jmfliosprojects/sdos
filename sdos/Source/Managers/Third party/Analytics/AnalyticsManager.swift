//
//  AnalyticsManager.swift
//  sdos
//
//  Created by Juan Miguel Fernández on 05/09/2019.
//  Copyright © 2019 Juan Miguel Fernández Lerena. All rights reserved.
//

import Foundation
import Fabric
import Crashlytics
import Logging

/// Constants for manager.
struct AnalyticsConstants {

    static let LOGIN_METHOD = "Login"
    static let SIGNIN_METHOD = "Sign in"
    static let SCREEN_SEARCHER = "Searcher"
    static let SCREEN_MORE = "More"
    static let SCREEN_RESULTS = "Results"
    static let SCREEN_MOVIE = "Movie"
    
    static let SEARCHING_EVENT = "Searching"

}

/// Manager of Answers Events.
class AnalyticsManager: NSObject {
    
    /// Shared instance.
    static let sharedInstance = AnalyticsManager()
    
    /// Init.
    private override init() {
        
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Public methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Sign.
    /// - parameters:
    ///   - success: If success.
    /// - throws:
    static func sign(success: Bool) {
        LogApp.info(String(describing: AnalyticsManager.self) + LoggingConstants.SEPARATOR + "Sign...")
        let customAttributes: [String: String]!  = ["" : ""]
        AnalyticsManager.logSignIn(method: AnalyticsConstants.SIGNIN_METHOD, success: success, customAttributes: customAttributes)
    }
    
    /// Login.
    /// - parameters:
    ///   - success: If success.
    /// - throws:
    static func login(success: Bool) {
        LogApp.info(String(describing: AnalyticsManager.self) + LoggingConstants.SEPARATOR + "Login...")
        let customAttributes: [String: String]!  = ["" : ""]
        AnalyticsManager.loglogin(method: AnalyticsConstants.LOGIN_METHOD, success: success, customAttributes: customAttributes)
    }
    
    /// Searcher.
    /// - throws:
    static func logContentViewForSearcher() {
        LogApp.info(String(describing: AnalyticsManager.self) + LoggingConstants.SEPARATOR + "Content view for searcher...")
        let name: String! = AnalyticsConstants.SCREEN_SEARCHER
        let contentType: String! = ""
        let contentId: String! = ""
        let customAttributes: [String: String]!  = ["" : ""]
        AnalyticsManager.logContentView(name: name, contentType: contentType, contentId: contentId, customAttributes: customAttributes)
    }
    
    /// Results.
    /// - throws:
    static func logContentViewForResults() {
        LogApp.info(String(describing: AnalyticsManager.self) + LoggingConstants.SEPARATOR + "Content view for results...")
        let name: String! = AnalyticsConstants.SCREEN_RESULTS
        let contentType: String! = ""
        let contentId: String! = ""
        let customAttributes: [String: String]!  = ["" : ""]
        AnalyticsManager.logContentView(name: name, contentType: contentType, contentId: contentId, customAttributes: customAttributes)
    }
    
    /// Movie.
    /// - throws:
    static func logContentViewForMovie() {
        LogApp.info(String(describing: AnalyticsManager.self) + LoggingConstants.SEPARATOR + "Content view for movie...")
        let name: String! = AnalyticsConstants.SCREEN_MOVIE
        let contentType: String! = ""
        let contentId: String! = ""
        let customAttributes: [String: String]!  = ["" : ""]
        AnalyticsManager.logContentView(name: name, contentType: contentType, contentId: contentId, customAttributes: customAttributes)
    }
    
    /// More.
    /// - throws:
    static func logContentViewForMore() {
        LogApp.info(String(describing: AnalyticsManager.self) + LoggingConstants.SEPARATOR + "Content view for more...")
        let name: String! = AnalyticsConstants.SCREEN_MORE
        let contentType: String! = ""
        let contentId: String! = ""
        let customAttributes: [String: String]!  = ["" : ""]
        AnalyticsManager.logContentView(name: name, contentType: contentType, contentId: contentId, customAttributes: customAttributes)
    }
    
    /// Searching.
    /// - throws:
    static func logEventForSearching() {
        LogApp.info(String(describing: AnalyticsManager.self) + LoggingConstants.SEPARATOR + "Event for searching...")
        let customAttributes: [String: String]!  = ["" : ""]
        AnalyticsManager.logCustomEvent(name: AnalyticsConstants.SEARCHING_EVENT, customAttributes: customAttributes)
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Private methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Purchase
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Add to Cart
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Start Checkout
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Content View
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Log content view.
    /// - parameters:
    ///   - name: Name.
    ///   - contentType: Content type.
    ///   - contentId: Content id.
    ///   - customAttributes: Custom attributes.
    /// - throws:
    private static func logContentView(name: String,
                                       contentType: String,
                                       contentId: String,
                                       customAttributes: [String: String]) {
        LogApp.debug(String(describing: AnalyticsManager.self) + LoggingConstants.SEPARATOR + "Content view with name...")
        Answers.logContentView(withName: name, contentType: contentType, contentId: contentId, customAttributes: customAttributes)
        LogApp.debug(String(describing: AnalyticsManager.self) + LoggingConstants.SEPARATOR + name)
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Search
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Log search.
    /// - parameters:
    ///   - query: query.
    ///   - customAttributes: Custom attributes.
    /// - throws:
    private static func logSearch(query: String,
                                  customAttributes: [String: String]) {
        LogApp.debug(String(describing: AnalyticsManager.self) + LoggingConstants.SEPARATOR + "Search with query...")
        Answers.logSearch(withQuery: query, customAttributes: customAttributes)
        LogApp.debug(String(describing: AnalyticsManager.self) + LoggingConstants.SEPARATOR + query)
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Share
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Rated Content
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Sign Up
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Sign Up.
    /// - parameters:
    ///   - method: Name of method.
    ///   - success: If success.
    ///   - customAttributes: Custom attributes.
    /// - throws:
    private static func logSignIn(method: String,
                                  success: Bool,
                                  customAttributes: [String: String]) {
        LogApp.debug(String(describing: AnalyticsManager.self) + LoggingConstants.SEPARATOR + "Sign in method...")
        Answers.logSignUp(withMethod: AnalyticsConstants.LOGIN_METHOD, success: success as NSNumber?, customAttributes: customAttributes)
        LogApp.debug(String(describing: AnalyticsManager.self) + LoggingConstants.SEPARATOR + method)
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Log In
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Login.
    /// - parameters:
    ///   - method: Name of method.
    ///   - success: If success.
    ///   - customAttributes: Custom attributes.
    /// - throws:
    private static func loglogin(method: String,
                                 success: Bool,
                                 customAttributes: [String: String]) {
        LogApp.debug(String(describing: AnalyticsManager.self) + LoggingConstants.SEPARATOR + "Login with method...")
        Answers.logLogin(withMethod: AnalyticsConstants.LOGIN_METHOD, success: success as NSNumber?, customAttributes: customAttributes)
        LogApp.debug(String(describing: AnalyticsManager.self) + LoggingConstants.SEPARATOR + method)
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Invite
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Custom Event
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Log custom event.
    /// - parameters:
    ///   - name: Name of event.
    ///   - customAttributes: Custom attributes.
    /// - throws:
    private static func logCustomEvent(name: String,
                                       customAttributes: [String: String]) {
        LogApp.debug(String(describing: AnalyticsManager.self) + LoggingConstants.SEPARATOR + "Custom event with name...")
        Answers.logCustomEvent(withName: name, customAttributes: customAttributes)
        LogApp.debug(String(describing: AnalyticsManager.self) + LoggingConstants.SEPARATOR + name)
    }
    
}

