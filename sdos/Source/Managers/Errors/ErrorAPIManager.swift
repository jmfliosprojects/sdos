//
//  ErrorAPIManager.swift
//  sdos
//
//  Created by Juan Miguel Fernández on 05/09/2019.
//  Copyright © 2019 Juan Miguel Fernández Lerena. All rights reserved.
//

import Foundation

/// Constants for manager.
struct APIKeyError {
    
    static let GENERIC = "generic"
    static let url_not_found = "url.not_found"
    
}
