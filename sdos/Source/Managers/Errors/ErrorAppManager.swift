//
//  ErrorAppManager.swift
//  sdos
//
//  Created by Juan Miguel Fernández on 05/09/2019.
//  Copyright © 2019 Juan Miguel Fernández Lerena. All rights reserved.
//

import Foundation
import Logging
import SwiftyJSON

/// Error App.
struct ErrorApp {
    
    static let EXCEPTION_CONFIGURATION_DOMAIN = "SSExceptionConfigurationDomain"
    static let CODE_9999_CONFIGURATION = 9999
    static let EXCEPTION_SETTINGS_DOMAIN = "ExceptionSettingsDomain"
    static let CODE_999_SETTINGS = 999
    static let EXCEPTION_COREDATA_DOMAIN = "ExceptionCoreDataDomain"
    static let CODE_998_COREDATA = 998
    static let EXCEPTION_COREDATA_SAVE_DOMAIN = "ExceptionCoreDataSaveDomain"
    static let CODE_997_COREDATA_SAVE = 997
    static let EXCEPTION_COREDATA_DELETE_ALL_DOMAIN = "ExceptionCoreDataDeleteAllDomain"
    static let CODE_996_COREDATA_DELETE_ALL = 996
    static let EXCEPTION_COREDATA_FIND_ALL_DOMAIN = "ExceptionCoreDataFindAllDomain"
    static let CODE_995_COREDATA_FIND_ALL = 995
    static let EXCEPTION_COREDATA_FIND_WITH_IDENTIFIER_DOMAIN = "ExceptionCoreDataFindWithIdentifierDomain"
    static let CODE_994_COREDATA_FIND_WITH_IDENTIFIER = 994
    static let EXCEPTION_COREDATA_DELETE_WITH_IDENTIFIER_DOMAIN = "ExceptionCoreDataDeleteWithIdentifierDomain"
    static let CODE_993_COREDATA_DELETE_WITH_IDENTIFIER = 993
    static let EXCEPTION_BUSINESS_DOMAIN = "ExceptionBusinessDomain"
    static let CODE_100_UNEXPECTED = 100
    static let CODE_101_BAD_REQUEST = 101
    static let CODE_102_UNAUTHORIZED = 102
    static let CODE_103_FORBIDDEN = 103
    static let CODE_104_NOT_FOUND = 104
    static let CODE_105_INTERNAL_ERROR = 105
    static let CODE_106_NOT_AVAILABLE = 106
    static let CODE_198_PROCESSING_DATA = 198
    static let CODE_199_FAILURE = 199
    
}

/// Manager of Error.
class ErrorAppManager: NSObject {
    
    /// Shared instance.
    static let sharedInstance = ErrorAppManager()
    
    /// Init.
    private override init() {
        
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Public methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Obtain message error of app.
    /// - parameters:
    ///   - apiError: API error.
    ///   - json: JSON.
    /// - throws:
    /// - returns: Message error of app.
    static func messageErrorApp(appError: NSError?,
                                json: JSON?) -> String? {
        LogApp.info(String(describing: ErrorAppManager.self) + LoggingConstants.SEPARATOR + "Message error app...")
        LogApp.error(String(describing: ErrorAppManager.self) + LoggingConstants.SEPARATOR + "Error: \(String(describing: appError)) \(String(describing: appError?.localizedDescription))")
        var message: String?
        if json == JSON.null {
            if appError?.code == ErrorApp.CODE_9999_CONFIGURATION {
                message = appError?.localizedDescription
            } else if appError?.code == ErrorApp.CODE_999_SETTINGS {
                message = appError?.localizedDescription
            } else if appError?.code == ErrorApp.CODE_998_COREDATA {
                message = appError?.localizedDescription
            } else if appError?.code == ErrorApp.CODE_997_COREDATA_SAVE {
                message = appError?.localizedDescription
            } else if appError?.code == ErrorApp.CODE_996_COREDATA_DELETE_ALL {
                message = appError?.localizedDescription
            } else if appError?.code == ErrorApp.CODE_995_COREDATA_FIND_ALL {
                message = appError?.localizedDescription
            } else if appError?.code == ErrorApp.CODE_994_COREDATA_FIND_WITH_IDENTIFIER {
                message = appError?.localizedDescription
            } else if appError?.code == ErrorApp.CODE_993_COREDATA_DELETE_WITH_IDENTIFIER {
                message = appError?.localizedDescription
            } else if appError?.code == ErrorApp.CODE_100_UNEXPECTED {
                message = appError?.localizedDescription
            } else {
                message = appError?.localizedDescription
            }
            message = appError?.localizedDescription
        } else {
            if appError?.code == ErrorApp.CODE_102_UNAUTHORIZED { // 401
                if let arrayKeyError = json?.arrayObject  {
                    let arrayMessageError: NSMutableArray = NSMutableArray()
                    for keyError in arrayKeyError {
                        if keyError as? String == APIKeyError.GENERIC {
                            message = NSLocalizedString("Try it later.", comment: "")
                            arrayMessageError.add(message ?? "")
                        } else {
                            message = NSLocalizedString("Indefinite api error.", comment: "")
                            arrayMessageError.add(message ?? "")
                        }
                    }
                    message = arrayMessageError.componentsJoined(by: "\n")
                } else {
                    message = appError?.localizedDescription
                }
            } else if appError?.code == ErrorApp.CODE_101_BAD_REQUEST { // 400
                if let arrayKeyError = json?.arrayObject  {
                    let arrayMessageError: NSMutableArray = NSMutableArray()
                    for keyError in arrayKeyError {
                        if keyError as? String == APIKeyError.GENERIC {
                            message = NSLocalizedString("Try it later.", comment: "")
                            arrayMessageError.add(message ?? "")
                        } else {
                            message = NSLocalizedString("Indefinite api error.", comment: "")
                            arrayMessageError.add(message ?? "")
                        }
                    }
                    message = arrayMessageError.componentsJoined(by: "\n")
                } else {
                    message = appError?.localizedDescription
                }
            } else if appError?.code == ErrorApp.CODE_103_FORBIDDEN { // 403
                message = appError?.localizedDescription
            } else if appError?.code == ErrorApp.CODE_104_NOT_FOUND { // 404
                if let arrayKeyError = json?.arrayObject  {
                    let arrayMessageError: NSMutableArray = NSMutableArray()
                    for keyError in arrayKeyError {
                        if keyError as? String == APIKeyError.GENERIC {
                            message = NSLocalizedString("Try it later.", comment: "")
                            arrayMessageError.add(message ?? "")
                        } else if keyError as? String == APIKeyError.url_not_found {
                            message = NSLocalizedString("URL not found.", comment: "")
                            arrayMessageError.add(message ?? "")
                        } else {
                            message = NSLocalizedString("Indefinite api error.", comment: "")
                            arrayMessageError.add(message ?? "")
                        }
                    }
                    message = arrayMessageError.componentsJoined(by: "\n")
                } else {
                    message = appError?.localizedDescription
                }
            } else if appError?.code == ErrorApp.CODE_105_INTERNAL_ERROR { // 500
                if let arrayKeyError = json?.arrayObject  {
                    let arrayMessageError: NSMutableArray = NSMutableArray()
                    for keyError in arrayKeyError {
                        if keyError as? String == APIKeyError.GENERIC {
                            message = NSLocalizedString("Generic error.", comment: "")
                            arrayMessageError.add(message ?? "")
                        } else {
                            message = NSLocalizedString("Indefinite api error.", comment: "")
                            arrayMessageError.add(message ?? "")
                        }
                    }
                    message = arrayMessageError.componentsJoined(by: "\n")
                } else {
                    message = appError?.localizedDescription
                }
            } else if appError?.code == ErrorApp.CODE_106_NOT_AVAILABLE { // 503
                message = appError?.localizedDescription
            } else if appError?.code == ErrorApp.CODE_198_PROCESSING_DATA { // 100
                message = appError?.localizedDescription
            } else if appError?.code == ErrorApp.CODE_199_FAILURE { // 100
                message = appError?.localizedDescription
            } else {
                message = appError?.localizedDescription
            }
        }
        return message
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Private methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
}
