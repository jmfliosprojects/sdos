//
//  SearchManager.swift
//  sdos
//
//  Created by Juan Miguel Fernández on 06/09/2019.
//  Copyright © 2019 Juan Miguel Fernández Lerena. All rights reserved.
//

import Foundation
import Logging

/// Constants for manager.
struct SearchNotificationConstants {
    
    static let PERFORM_INSERT_SEARCH_DATA = "PerformInsertSearchData"
    static let PERFORM_REMOVE_ALL_SEARCHES_DATA = "PerformRemoveAllSearchesData"
    
}

/// Manager of Search.
@objc class SearchManager: NSObject {
    
    /// Shared instance.
    static let sharedInstance = SearchManager()
    
    /// Init.
    private override init() {
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Public methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Perform new search.
    /// - parameters:
    ///     - text: Text.
    ///     - createdDate: Date.
    /// - throws:
    static func performNewSearch(text: String, createdDate: Date) {
        LogApp.info(String(describing: SearchManager.self) + LoggingConstants.SEPARATOR + "Perform new search...")
        do {
            let arrayObjects: NSArray? = try SearchService.findAll(text: text)
            if arrayObjects?.count == 0 {
                SearchService.newSearch(text: text, createdDate: createdDate)
            }
        } catch let coreDataError as NSError  {
            LogApp.error(String(describing: SearchManager.self) + LoggingConstants.SEPARATOR + "Perform remove all: \(coreDataError) \(coreDataError.localizedDescription)")
        }
    }
    
    /// Perform all searchers.
    /// - throws: Error.
    /// - returns: All objects.
    static func performAllSearches() -> NSArray {
        LogApp.info(String(describing: SearchManager.self) + LoggingConstants.SEPARATOR + "Perform all searchers...")
        do {
            return try SearchService.findAll()
        } catch let coreDataError as NSError  {
            LogApp.error(String(describing: SearchManager.self) + LoggingConstants.SEPARATOR + "Perform all searchers: \(coreDataError) \(coreDataError.localizedDescription)")
            return []
        }
    }
    
    /// Perform remove all.
    /// - throws:
    static func performRemoveAllSearches() {
        LogApp.info(String(describing: SearchManager.self) + LoggingConstants.SEPARATOR + "Perform remove all...")
        do {
            try SearchService.deleteAll()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: SearchNotificationConstants.PERFORM_REMOVE_ALL_SEARCHES_DATA), object: nil)
        } catch let coreDataError as NSError  {
            LogApp.error(String(describing: SearchManager.self) + LoggingConstants.SEPARATOR + "Perform remove all: \(coreDataError) \(coreDataError.localizedDescription)")
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Private methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
}

