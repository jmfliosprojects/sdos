//
//  UserManager.swift
//  sdos
//
//  Created by Juan Miguel Fernández on 05/09/2019.
//  Copyright © 2019 Juan Miguel Fernández Lerena. All rights reserved.
//

import Foundation
import Logging

/// Constants for manager.
struct UserNotificationConstants {
    
    static let PERFORM_USER_DATA = "PerformUserData"
    
}

/// Manager of User.
@objc class UserManager: NSObject {
    
    /// Shared instance.
    static let sharedInstance = UserManager()
    
    /// Init.
    private override init() {
    }

    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Public methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Register user defaults from settings bundle.
    /// - throws: EXCEPTION_SETTINGS_DOMAIN
    static func registerUserDefaultsFromSettingsBundle() throws {
        guard let settingsBundle = Bundle.main.path(forResource: "Settings", ofType: "bundle") else {
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = NSLocalizedString("Configuration searching failure", comment: "") as AnyObject?
            let failureReason = "There was an error creating or loading the application's saved data."
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?
            
            let notFoundSettingsError = NSError(domain: ErrorApp.EXCEPTION_SETTINGS_DOMAIN, code: ErrorApp.CODE_999_SETTINGS, userInfo: dict)
            throw notFoundSettingsError
        }
        let settings = NSDictionary(contentsOfFile: (settingsBundle as NSString).appendingPathComponent("Root.plist"))!
        let preferences = settings["PreferenceSpecifiers"] as! NSArray
        var defaultsToRegister = [String: AnyObject]()
        for prefSpecification in preferences {
            guard let key = (prefSpecification as! NSDictionary)["Key"] as? String,
                let defaultValue = (prefSpecification as! NSDictionary)["DefaultValue"] else {
                    continue
            }
            defaultsToRegister[key] = defaultValue as AnyObject?
        }
        UserDefaults.standard.register(defaults: defaultsToRegister)
    }
    
    /// Init user defaults for settings bundle.
    /// - throws:
    static func initUserDefaultsForSettingsBundle() {
        LogApp.info(String(describing: UserManager.self) + LoggingConstants.SEPARATOR + "Init user defaults for settings bundle...")
        if (!UserManager.isInstallation()) {
            ConfigurationManager.appUserDefaultsForSettingsVersion(string: ConfigurationManager.versionApp())
            ConfigurationManager.appUserDefaultsForSettingsEntity(string: ConfigurationManager.apiKey())
            ConfigurationManager.appUserDefaultsForSettingsServer(string: ConfigurationManager.apiURL())
            ConfigurationManager.appUserDefaultsForSettingsEnterprise(string: ProviderConfigurationManager.providerVendor())
            UserManager.isInstallation(first: true)
        }
    }
    
    /// Update user defaults for settings bundle.
    /// - throws:
    static func updateUserDefaultsForSettingsBundle() {
        LogApp.info(String(describing: UserManager.self) + LoggingConstants.SEPARATOR + "Update user defaults for settings bundle...")
        
    }
    
    /// Debug mode.
    /// - throws:
    /// - returns: If debug mode.
    static func isDebugMode() -> Bool {
        var result = false
        result = UserDefaults.standard.object(forKey: ConfigurationUserDefaultsForSettingsConstants.APP_DEBUG) as? Bool ?? false
        return result
    }
    
    /// Init user.
    /// - throws:
    static func initUser() {
        LogApp.info(String(describing: UserManager.self) + LoggingConstants.SEPARATOR + "Init user...")
        if ConfigurationManager.appUserDefaultsForSettingsUUIDDevice() == nil {
            ConfigurationManager.initUUIDDevice()
            ConfigurationManager.initInstalationDate()
        }
        UserService.preloadUserData()
    }
    
    /// Init user Carsh for user.
    /// - throws:
    static func startUser() {
        LogApp.info(String(describing: UserManager.self) + LoggingConstants.SEPARATOR + "Start user...")
        var userName = ""
        var identifier = ConfigurationManager.appUserDefaultsForSettingsUUIDDevice() ?? ""
        var email = ""
        if let user: User = UserManager.loggedUser() {
            userName = user.name ?? (ConfigurationManager.appUserDefaultsForSettingsUUIDDevice() ?? "")
            identifier = String(user.identifier)
            email = user.emailAddress ?? (ConfigurationManager.appUserDefaultsForSettingsUUIDDevice() ?? "")
        }
        // Logging
        LogApp.settingAnalytics(userName: userName)
        // Crash
        CrashManager.logUser(userEmail: email,
                             userIdentifier: identifier,
                             userName: userName)
    }

    /// Logged user.
    /// - throws:
    /// - returns: Logged user.
    static func loggedUser() -> User? {
        LogApp.info(String(describing: UserManager.self) + LoggingConstants.SEPARATOR + "Logged user...")
        var user: User?
        do {
            let users = try UserService.findAll()
            user = users.firstObject as? User
        } catch let error as NSError  {
            LogApp.info(String(describing: UserManager.self) + LoggingConstants.SEPARATOR + "Could not get \(error), \(error.userInfo)")
        }
        return user
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Private methods
    ////////////////////////////////////////////////////////////////////////////////////////////

    /// NSAppInstallation.
    /// - throws:
    /// - returns: NSAppInstallation
    private static func isInstallation() -> Bool {
        var result = false
        result = UserDefaults.standard.object(forKey: ConfigurationUserDefaultsForSettingsConstants.APP_INSTALLATION) as? Bool ?? false
        return result
    }
    
    /// NSAppInstallation.
    /// - parameters:
    ///   - string: NSAppInstallation.
    /// - throws:
    private static func isInstallation(first: Bool) {
        UserDefaults.standard.set(first, forKey: ConfigurationUserDefaultsForSettingsConstants.APP_INSTALLATION)
    }
}
