//
//  ResultManager.swift
//  sdos
//
//  Created by Juan Miguel Fernández on 07/09/2019.
//  Copyright © 2019 Juan Miguel Fernández Lerena. All rights reserved.
//

import Foundation
import Logging
import Connection

/// Constants for manager.
struct ResultNotificationConstants {
    
    static let PERFORM_RESULTS_DATA = "PerformResultsData"
    static let PERFORM_RESULT_DATA = "PerformResultData"
    
}

/// Manager of Result.
@objc class ResultManager: NSObject {
    
    /// Completion handler for request in background.
    typealias ResultRequestsCompletionHandler = (_ isReachability: Bool, _ success: Bool, _ errorMessage: String?, _ numResults: Int?) -> Void
    /// Completion handler for request in background.
    typealias ResultRequestCompletionHandler = (_ isReachability: Bool, _ success: Bool, _ errorMessage: String?) -> Void
    
    /// Shared instance.
    static let sharedInstance = ResultManager()
    
    /// Init.
    private override init() {
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Public methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    /// Perform results requests.
    /// - parameters:
    ///     - text: Text.
    ///     - page: Page.
    ///     - completionHandler: CompletionHandler.
    /// - throws:
    static func performResultsRequests(_ text: String,
                                         page: String,
                                       _ completionHandler: @escaping ResultRequestsCompletionHandler) {
        LogApp.info(String(describing: ResultManager.self) + LoggingConstants.SEPARATOR + "Perform results requests...")
        let apiKey = ConfigurationManager.appUserDefaultsForSettingsEntity()
        let domain = ConfigurationManager.appUserDefaultsForSettingsServer()
        let service = ""
        let method = ""
        let headers = APIManager.apiHeadersAuthenticated()
        let parameters: [String : Any] = [
            APIParameters.TEXT_QUERY: text,
            APIParameters.PAGE_QUERY: page,
            APIParameters.API_KEY_QUERY: apiKey
        ]
        RestAPIManager.performRequest(httpMethod: RestAPIMethod.GET,
                                      domain: domain,
                                      service: service,
                                      method: method,
                                      headers: headers,
                                      parameters: parameters,
                                      completionHandler: { (isReachability, success, json, apiError, syncedAt) in
                                        if success {
                                            if apiError != nil {
                                                let appError = APIManager.errorApp(apiError: apiError)
                                                let message = ErrorAppManager.messageErrorApp(appError: appError, json: json)
                                                completionHandler(isReachability, false, message, 0)
                                            } else {
                                                let response = json["Response"].boolValue
                                                if !response {
                                                    completionHandler(isReachability, false, json["Error"].string, 0)
                                                } else {
                                                    do {
                                                        let search = json["Search"]
                                                        try FilmService.saveArray(dataJSON: search, syncedAt: syncedAt)
                                                        let numResults = json["totalResults"].intValue
                                                        completionHandler(isReachability, true, nil, numResults)
                                                    } catch let coreDataError as NSError  {
                                                        LogApp.error(String(describing: ResultManager.self) + LoggingConstants.SEPARATOR + "Perform results requests: \(coreDataError) \(coreDataError.localizedDescription)")
                                                        var dict = [String: AnyObject]()
                                                        dict[NSLocalizedDescriptionKey] = NSLocalizedString("Processing data", comment: "") as AnyObject?
                                                        dict[NSLocalizedFailureReasonErrorKey] = coreDataError.localizedFailureReason as AnyObject?
                                                        dict[NSUnderlyingErrorKey] = coreDataError
                                                        let error = NSError(domain: ErrorApp.EXCEPTION_BUSINESS_DOMAIN, code: ErrorApp.CODE_198_PROCESSING_DATA, userInfo: dict)
                                                        let message = ErrorAppManager.messageErrorApp(appError: error, json: nil)
                                                        completionHandler(isReachability, false, message, 0)
                                                    }
                                                }
                                            }
                                        } else {
                                            LogApp.error(String(describing: ResultManager.self) + LoggingConstants.SEPARATOR + "Perform results requests...")
                                            var dict = [String: AnyObject]()
                                            dict[NSLocalizedDescriptionKey] = NSLocalizedString("Try it later", comment: "") as AnyObject?
                                            dict[NSLocalizedFailureReasonErrorKey] = apiError?.localizedFailureReason as AnyObject?
                                            dict[NSUnderlyingErrorKey] = apiError! as NSError
                                            let error = NSError(domain: ErrorApp.EXCEPTION_BUSINESS_DOMAIN, code: ErrorApp.CODE_199_FAILURE, userInfo: dict)
                                            let message = ErrorAppManager.messageErrorApp(appError: error, json: nil)
                                            completionHandler(isReachability, false, message, 0)
                                        }
        })
    }
    
    /// Perform result requests.
    /// - parameters:
    ///     - imdbid: Imdbid.
    ///     - completionHandler: CompletionHandler.
    /// - throws:
    static func performResultRequests(_ imdbid: String,
                                      _ completionHandler: @escaping ResultRequestCompletionHandler) {
        LogApp.info(String(describing: ResultManager.self) + LoggingConstants.SEPARATOR + "Perform result requests...")
        let apiKey = ConfigurationManager.appUserDefaultsForSettingsEntity()
        let domain = ConfigurationManager.appUserDefaultsForSettingsServer()
        let service = ""
        let method = ""
        let headers = APIManager.apiHeadersAuthenticated()
        let parameters: [String : Any] = [
            APIParameters.IMDBID_QUERY: imdbid,
            APIParameters.API_KEY_QUERY: apiKey
        ]
        RestAPIManager.performRequest(httpMethod: RestAPIMethod.GET,
                                      domain: domain,
                                      service: service,
                                      method: method,
                                      headers: headers,
                                      parameters: parameters,
                                      completionHandler: { (isReachability, success, json, apiError, syncedAt) in
                                        if success {
                                            if apiError != nil {
                                                let appError = APIManager.errorApp(apiError: apiError)
                                                let message = ErrorAppManager.messageErrorApp(appError: appError, json: json)
                                                completionHandler(isReachability, false, message)
                                            } else {
                                                let response = json["Response"].boolValue
                                                if !response {
                                                    completionHandler(isReachability, false, json["Error"].string)
                                                } else {
                                                    do {
                                                        try FilmService.saveObject(dataJSON: json, syncedAt: syncedAt)
                                                        completionHandler(isReachability, true, nil)
                                                    } catch let coreDataError as NSError  {
                                                        LogApp.error(String(describing: ResultManager.self) + LoggingConstants.SEPARATOR + "Perform result requests: \(coreDataError) \(coreDataError.localizedDescription)")
                                                        var dict = [String: AnyObject]()
                                                        dict[NSLocalizedDescriptionKey] = NSLocalizedString("Processing data", comment: "") as AnyObject?
                                                        dict[NSLocalizedFailureReasonErrorKey] = coreDataError.localizedFailureReason as AnyObject?
                                                        dict[NSUnderlyingErrorKey] = coreDataError
                                                        let error = NSError(domain: ErrorApp.EXCEPTION_BUSINESS_DOMAIN, code: ErrorApp.CODE_198_PROCESSING_DATA, userInfo: dict)
                                                        let message = ErrorAppManager.messageErrorApp(appError: error, json: nil)
                                                        completionHandler(isReachability, false, message)
                                                    }
                                                }
                                            }
                                        } else {
                                            LogApp.error(String(describing: ResultManager.self) + LoggingConstants.SEPARATOR + "Perform result requests...")
                                            var dict = [String: AnyObject]()
                                            dict[NSLocalizedDescriptionKey] = NSLocalizedString("Try it later", comment: "") as AnyObject?
                                            dict[NSLocalizedFailureReasonErrorKey] = apiError?.localizedFailureReason as AnyObject?
                                            dict[NSUnderlyingErrorKey] = apiError! as NSError
                                            let error = NSError(domain: ErrorApp.EXCEPTION_BUSINESS_DOMAIN, code: ErrorApp.CODE_199_FAILURE, userInfo: dict)
                                            let message = ErrorAppManager.messageErrorApp(appError: error, json: nil)
                                            completionHandler(isReachability, false, message)
                                        }
        })
    }
    
    /// Perform clean results.
    /// - parameters:
    ///     - text: Text.
    ///     - completionHandler: CompletionHandler.
    /// - throws:
    static func performCleanResults() {
        LogApp.info(String(describing: ResultManager.self) + LoggingConstants.SEPARATOR + "Perform clean results...")
        do {
            try  FilmService.deleteAll()
        } catch let error as NSError {
            LogApp.error(String(describing: ResultManager.self) + LoggingConstants.SEPARATOR + "Perform clean results: \(error) \(error.localizedDescription)")
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////
    // MARK: Private methods
    ////////////////////////////////////////////////////////////////////////////////////////////
    
}

